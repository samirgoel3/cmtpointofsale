package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.tyre.TyreRimsActivity;
import com.spinno.cmtpointofsale.salesModle.addtyres.TyreRimsActivityFromSale;

import checkers.Toaster;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;

/**
 * Created by samir on 03/08/15.
 */
public class Adaprtergrid extends BaseAdapter {

    Context con ;
    LayoutInflater inflater;


    String [] colorsarr ;



    public Adaprtergrid(Context con,String [] colorsarr){
        this.con = con ;
        this.colorsarr = colorsarr ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return colorsarr.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view ;

        view = inflater.inflate(R.layout.item_grid, parent, false);

        FrameLayout backgropundcolour = (FrameLayout) view.findViewById(R.id.frame_in_item_of_grid);
        ImageView backgroundimage  = (ImageView) view.findViewById(R.id.backgroundimgae_in_item_grid);
        final TextView companyname = (TextView) view.findViewById(R.id.company_name_item_of_grid_view);

                  companyname.setText(colorsarr[position]);









        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConstantClass.whichActivity.equals("VehicleTypeActivityNewFromSale")) {
                    con.startActivity(new Intent(con, TyreRimsActivityFromSale.class));
                    PlaceOrderModuleConstants.companytype = companyname.getText().toString();
                } else if (ConstantClass.whichActivity.equals("FilterActivityNew")) {
                    if (position == 0) {

                        con.startActivity(new Intent(con, TyreRimsActivity.class));
                        PlaceOrderModuleConstants.companytype = companyname.getText().toString();
                    } else {
                        Toaster.generatemessage(con, "Under Development");
                    }
                }
            }
        });











        return view;
    }
}
