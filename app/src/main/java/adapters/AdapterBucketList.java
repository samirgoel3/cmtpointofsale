package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.InvoiceBucketTable;
import com.spinno.cmtpointofsale.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import databases.manager.InvoiceBucketManager;
import databases.manager.StockInManager;

/**
 * Created by spinnosolutions on 8/13/15.
 */
public class AdapterBucketList extends BaseAdapter{

    Context con ;
    LayoutInflater inflater ;
    InvoiceBucketManager ibmanager  ;
    StockInManager stinmanager ;
    List<InvoiceBucketTable>   invoiceBucketData ;

    public AdapterBucketList(Context context){
         this.con = context ;
         ibmanager = new InvoiceBucketManager(con);
         stinmanager = new StockInManager(con);
         invoiceBucketData = ibmanager.getalldata();
        inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    @Override
    public int getCount() {
        return ibmanager.getTotalTYpeOftyreInBucket();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_bucket_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.tyre_name.setText(""+invoiceBucketData.get(position).getTyreName());
        holder.tyre_specs.setText(""+invoiceBucketData.get(position).getTyreSpecs());
        holder.dealer_price.setText("Rs "+invoiceBucketData.get(position).getTyrePrice()+"/-");
        holder.no_of_unit.setText(""+invoiceBucketData.get(position).getTyreNoOfunits());



/////////////////plus button
        holder.plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int noOfUnits = Integer.parseInt(holder.no_of_unit.getText().toString());
                int tyreInStock = stinmanager.getTotalNoOftyreOfByTyreid(invoiceBucketData.get(position).getTyreId());
                if(noOfUnits<tyreInStock){
                    holder.no_of_unit.setText(""+(noOfUnits+1));
                    ibmanager.changeExsistingRowInInvoiceBucketTable(invoiceBucketData.get(position).getTyreId(), "" + holder.no_of_unit.getText().toString());
                }else {
                    Toaster.generatemessage(con , "You have only "+tyreInStock+" tyres in stock");
                }
            }
        });



//////////////////minus button
        holder.minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int noOfUnits = Integer.parseInt(holder.no_of_unit.getText().toString());
                int tyreInStock = stinmanager.getTotalNoOftyreOfByTyreid(invoiceBucketData.get(position).getTyreId());
                if(noOfUnits>1){
                    holder.no_of_unit.setText(""+(noOfUnits-1));
                    ibmanager.changeExsistingRowInInvoiceBucketTable(invoiceBucketData.get(position).getTyreId() ,""+holder.no_of_unit.getText().toString());
                }if(noOfUnits == 1) {
                    holder.no_of_unit.setText(""+(noOfUnits-1));
                    ibmanager.removeFromBucket(invoiceBucketData.get(position).getTyreId());
                    notifyDataSetChanged();
                }
            }
        });

        return view;
    }

    static class ViewHolder {
        @Bind(R.id.tyre_name_in_item_of_bucket_list)TextView tyre_name;
        @Bind(R.id.tyre_specs_in_item_of_bucket_list)TextView tyre_specs;
        @Bind(R.id.dealer_price_in_item_of_bucket_list)TextView dealer_price;
        @Bind(R.id.no_of_unit_in_item_of_bucket_list)TextView no_of_unit;
        @Bind(R.id.minus_button_in_item_of_bucket_list)ImageView minus_btn;
        @Bind(R.id.plus_button_in_item_of_bucket_list)ImageView plus_btn;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
