package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.PlaceOrderModuleConstants;

/**
 * Created by samir on 29/07/15.
 */
public class AdapterConfirmActivity extends BaseAdapter {
    Context con;
    LayoutInflater inflater;


    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();




    public AdapterConfirmActivity(Context con ,  ArrayList<String> tyre_id_arr , ArrayList<String> tyre_name_arr , ArrayList<String> tyre_specs_arr ,ArrayList<String> tyre_price_arr ,  ArrayList<String> tyre_number_of_unit_arr ) {
        this.con = con;
        this.tyre_id_arr= tyre_id_arr;
        this.tyre_name_arr  = tyre_name_arr ;
        this.tyre_specs_arr = tyre_specs_arr ;
        this.tyre_price_arr = tyre_price_arr ;
        this.tyre_number_of_unit_arr = tyre_number_of_unit_arr;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return tyre_id_arr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_confirm_proviosional_order, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.tyre_name_txt.setText(""+tyre_name_arr.get(position));
        holder.tyre_specs_txt.setText(""+tyre_specs_arr.get(position));
        holder.tyre_qty_txt.setText(""+tyre_number_of_unit_arr.get(position));
        holder.qty_bigger_txt.setText(""+tyre_number_of_unit_arr.get(position));



        if(PlaceOrderModuleConstants.ordertype.equals("order_without_price")){
            holder.pricing_btn.setVisibility(View.GONE);
            holder.qty_layout.setVisibility(View.VISIBLE);
            holder.qty_small_layout.setVisibility(View.GONE);
        }else if(PlaceOrderModuleConstants.ordertype.equals("order_with_price")){
            holder.pricing_btn.setVisibility(View.VISIBLE);
            holder.qty_layout.setVisibility(View.GONE);
            holder.qty_small_layout.setVisibility(View.VISIBLE);

        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toaster.generatemessage(con , "here dealer will enetr or edit price of a tyre");
            }
        });
        return view;
    }


    static class ViewHolder {
        @Bind(R.id.pricing_layout_in_confirm_Activity)TextView pricing_btn;
        @Bind(R.id.tyre_name_in_confirm_order_list_activity)TextView tyre_name_txt;
        @Bind(R.id.tyre_spec_in_confirm_order_list_activity)TextView tyre_specs_txt;
        @Bind(R.id.tyre_qty_in_confirm_order_list_activity)TextView tyre_qty_txt;
        @Bind(R.id.qty_layout_in_item_for_confirm_proviosional_orderslist)LinearLayout qty_layout;
        @Bind(R.id.qty_layout_small_in_confirm_provisionla_list_aactivity)LinearLayout qty_small_layout;
        @Bind(R.id.tyre_qty_bigger_in_confirm_order_list_activity)TextView qty_bigger_txt;





        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}