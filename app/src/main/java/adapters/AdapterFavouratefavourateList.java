package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import databases.manager.CartManager;

/**
 * Created by samir on 31/07/15.
 */
public class AdapterFavouratefavourateList  extends BaseAdapter {


    Context con ;
    CartManager dbCartManager ;
    LayoutInflater inflater ;

    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();





    public  AdapterFavouratefavourateList(Context con ,  ArrayList<String> tyre_id_arr  , ArrayList<String> tyre_name_arr , ArrayList<String> tyre_specs_arr , ArrayList<String> tyre_price_arr ,  ArrayList<String> tyre_number_of_unit_arr ){
        this.con = con ;
        this.tyre_id_arr = tyre_id_arr ;
        this.tyre_name_arr = tyre_name_arr;
        this.tyre_specs_arr = tyre_specs_arr;
        this.tyre_price_arr = tyre_price_arr;
        this.tyre_number_of_unit_arr = tyre_number_of_unit_arr ;

        dbCartManager = new CartManager(con);
        inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }


    @Override
    public int getCount() {
        return tyre_id_arr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_favourate_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.tyre_name_txt.setText(""+tyre_name_arr.get(position));
        holder.tyre_specs_txt.setText(""+tyre_specs_arr.get(position));
        holder.no_of_unit_txt.setText(""+tyre_number_of_unit_arr.get(position));



/////////// delete button
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbCartManager.removeFromCart(tyre_id_arr.get(position));
                   tyre_id_arr.remove(position);
                    notifyDataSetChanged();
                Toaster.generatemessage(con , "Tyre removed from favourates");
            }
        });

///////////// Minus button
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toaster.generatemessage(con , "subtract values in cart");
            }
        });

/////////// Plus Button
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toaster.generatemessage(con , "Add values in cart");

            }
        });


        return view;
    }


    static class ViewHolder {
        @Bind(R.id.plus_button_in_item_of_favourate_list)ImageView plus;
        @Bind(R.id.minus_button_in_item_of_favourate_list)ImageView minus;
        @Bind(R.id.delete_button_in_item_of_favourate_list)ImageView delete;

        @Bind(R.id.no_of_unit_in_item_of_favourate_list)TextView no_of_unit_txt;
        @Bind(R.id.tyre_name_in_item_of_favourate_list)TextView tyre_name_txt;
        @Bind(R.id.tyre_specs_in_item_of_favourate_list)TextView tyre_specs_txt;





        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
