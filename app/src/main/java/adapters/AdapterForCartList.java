package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import databases.manager.CartManager;

/**
 * Created by samir on 29/07/15.
 */
public class AdapterForCartList extends BaseAdapter {


    Context con ;
    LayoutInflater inflater ;

    CartManager dbcartmanager ;

    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();


    public  AdapterForCartList (Context con  ,ArrayList<String> tyre_id_arr ,ArrayList<String> tyre_name_arr  ,ArrayList<String> tyre_specs_arr ,ArrayList<String> tyre_price_arr , ArrayList<String> tyre_number_of_unit_arr ){
        this.con = con ;
        inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        this.tyre_id_arr = tyre_id_arr ;
        this.tyre_name_arr = tyre_name_arr;
        this.tyre_specs_arr = tyre_specs_arr;
        this.tyre_price_arr =tyre_price_arr ;
        dbcartmanager = new CartManager(con);
        this.tyre_number_of_unit_arr =tyre_number_of_unit_arr;
    }

    @Override
    public int getCount() {
        return tyre_id_arr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_cart_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.tyre_name_txt.setText(tyre_name_arr.get(position));
        holder.tyre_specs_text.setText(tyre_specs_arr.get(position));
        holder.no_of_units.setText(tyre_number_of_unit_arr.get(position));


        holder.plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int valueunit = Integer.parseInt(tyre_number_of_unit_arr.get(position));
                 tyre_number_of_unit_arr.set(position , ""+(valueunit+1));
                 holder.no_of_units.setText("" + (valueunit + 1));
                dbcartmanager.addtocart(tyre_id_arr.get(position), tyre_name_arr.get(position), tyre_specs_arr.get(position), tyre_price_arr.get(position), holder.no_of_units.getText().toString());
            }
        });


         holder.minus_btn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 int valueunit = Integer.parseInt(tyre_number_of_unit_arr.get(position));
                 if(valueunit>1){
                     tyre_number_of_unit_arr.set(position , ""+(valueunit-1));
                     holder.no_of_units.setText("" + (valueunit - 1));
                     dbcartmanager.addtocart(tyre_id_arr.get(position), tyre_name_arr.get(position), tyre_specs_arr.get(position), tyre_price_arr.get(position), holder.no_of_units.getText().toString());
                 } if(valueunit == 1) {
                     dbcartmanager.removeFromCart(tyre_id_arr.get(position));
                     tyre_id_arr.remove(position);
                     notifyDataSetChanged();
                 }

             }
         });

        return view;
    }


    static class ViewHolder {
        @Bind(R.id.tyre_name_in_item_for_cart_layout)TextView tyre_name_txt;
        @Bind(R.id.tyre_specs_in_item_Pfor_cart_layout)TextView tyre_specs_text;
        @Bind(R.id.no_of_unit_in_item_for_place_order_list)TextView no_of_units;
        @Bind(R.id.plus_button__in_item_for_place_order_list)ImageView plus_btn;
        @Bind(R.id.minus_button_in_item_for_place_order_list)ImageView minus_btn;




        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
