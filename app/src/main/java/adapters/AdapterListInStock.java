package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.StockTable;
import com.spinno.cmtpointofsale.salesModle.addtyres.TyreListInStock;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.PlaceOrderModuleConstants;
import databases.manager.InvoiceBucketManager;
import databases.manager.StockInManager;

/**
 * Created by spinnosolutions on 8/11/15.
 */
public class AdapterListInStock extends BaseAdapter {

    Context con ;
    LayoutInflater inflater ;
    StockInManager stinmanager ;
    InvoiceBucketManager ibmanager ;
    List<StockTable> datastocktable ;

    public AdapterListInStock(Context con ){
        this.con = con   ;
        stinmanager = new StockInManager(con);
        ibmanager = new InvoiceBucketManager(con);
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        datastocktable = stinmanager.getAllTyresByVehicleCompanyRimSizeTyresize(PlaceOrderModuleConstants.vehicletype , PlaceOrderModuleConstants.companytype , PlaceOrderModuleConstants.rimsize , PlaceOrderModuleConstants.tyresize);
    }

    @Override
    public int getCount() {
        return datastocktable.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_tyre_list_in_stock, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }



        holder.tyre_name_txt.setText(""+datastocktable.get(position).getTyreName());
        holder.tyre_spec_txt.setText("Size : " + datastocktable.get(position).getTyreSpecs());
        holder.in_stock_txt.setText(""+datastocktable.get(position).getTyreInStock());

//////////////setting visibility of favourite
            if(datastocktable.get(position).getTyreFavourite().equals("0")){
                holder.favourite_img.setVisibility(View.GONE);
            }else if(datastocktable.get(position).getTyreFavourite().equals("1")){
                holder.favourite_img.setVisibility(View.VISIBLE);
            }

////////////// setting visibility of offers  plus clickability on favourite one
               if(datastocktable.get(position).getTyreOffer().equals("0")){
                    holder.view_offers_txt.setVisibility(View.GONE);
               }else if(datastocktable.get(position).getTyreOffer().equals("0")){
                holder.view_offers_txt.setVisibility(View.VISIBLE);
                   view.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {
                           Toaster.generatemessage(con , "This will open new activity");
                       }
                   });
               }

//////////////// plus button working
          holder.plus_btn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                 if(con instanceof TyreListInStock){
                     int noOfUnit = Integer.parseInt(holder.no_of_unit_txt.getText().toString());
                     int instock  = Integer.parseInt(holder.in_stock_txt.getText().toString());

                         if(instock>0 && noOfUnit <instock){
                             holder.no_of_unit_txt.setText(""+(noOfUnit+1));

                             ibmanager.addToBucket(datastocktable.get(position).getTyreId()
                             ,datastocktable.get(position).getVehicleType()
                             ,datastocktable.get(position).getBrandName()
                             ,datastocktable.get(position).getTyreRimSize()
                             ,datastocktable.get(position).getTyreName()
                             ,datastocktable.get(position).getTyreSpecs()
                             ,datastocktable.get(position).getTyreType()
                             ,datastocktable.get(position).getTyreFavourite()
                             ,holder.no_of_unit_txt.getText().toString()
                             ,datastocktable.get(position).getTyreOffer()
                             ,datastocktable.get(position).getTyreOfferId()
                             ,datastocktable.get(position).getTyreOfferCompany()
                             ,datastocktable.get(position).getTyreOfferName()
                             ,datastocktable.get(position).getTyreOfferDescription()
                             ,datastocktable.get(position).getTyreOfferStart()
                             ,datastocktable.get(position).getTyreOfferEnd()
                             ,datastocktable.get(position).getTyreOfferUnit()
                             ,datastocktable.get(position).getTyrePrice());

                         }
                     ((TyreListInStock) con).invoiceBucketResolver();
                 }
              }
          });

/////////////// minus button working
        holder.minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int noOfUnit = Integer.parseInt(holder.no_of_unit_txt.getText().toString());
                int instock  = Integer.parseInt(holder.in_stock_txt.getText().toString());
                if(noOfUnit>1){
                    holder.no_of_unit_txt.setText(""+(noOfUnit-1));

                    ibmanager.addToBucket(datastocktable.get(position).getTyreId()
                            , datastocktable.get(position).getVehicleType()
                            , datastocktable.get(position).getBrandName()
                            , datastocktable.get(position).getTyreRimSize()
                            , datastocktable.get(position).getTyreName()
                            , datastocktable.get(position).getTyreSpecs()
                            , datastocktable.get(position).getTyreType()
                            , datastocktable.get(position).getTyreFavourite()
                            , holder.no_of_unit_txt.getText().toString()
                            , datastocktable.get(position).getTyreOffer()
                            , datastocktable.get(position).getTyreOfferId()
                            , datastocktable.get(position).getTyreOfferCompany()
                            , datastocktable.get(position).getTyreOfferName()
                            , datastocktable.get(position).getTyreOfferDescription()
                            , datastocktable.get(position).getTyreOfferStart()
                            , datastocktable.get(position).getTyreOfferEnd()
                            , datastocktable.get(position).getTyreOfferUnit()
                            , datastocktable.get(position).getTyrePrice());



                }if(noOfUnit == 1 ){
                    holder.no_of_unit_txt.setText(""+(noOfUnit-1));
                    ibmanager.removeFromBucket(datastocktable.get(position).getTyreId());
                }


                ((TyreListInStock) con).invoiceBucketResolver();
            }
        });
        return view;
    }



    static class ViewHolder {

        @Bind(R.id.tyre_name_in_item_of_tyreListInStock) TextView tyre_name_txt;
        @Bind(R.id.tyre_specs_in_item_of_tyreListInStock) TextView tyre_spec_txt;
        @Bind(R.id.favourate_image_in_item_of_tyreListInStock) ImageView favourite_img;
        @Bind(R.id.instock_values_in_item_of_tyreListInStock) TextView in_stock_txt;
        @Bind(R.id.offer_text_in_item_of_tyreListInStock) TextView view_offers_txt;
        @Bind(R.id.minus_button_in_item_of_tyreListInStock) ImageView minus_btn;
        @Bind(R.id.plus_button_in_item_of_tyreListInStock) ImageView plus_btn;
        @Bind(R.id.no_of_unit_in_item_of_tyreListInStock) TextView no_of_unit_txt;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
