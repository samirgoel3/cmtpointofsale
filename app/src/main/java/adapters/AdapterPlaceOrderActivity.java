package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by samir on 30/07/15.
 */
public class AdapterPlaceOrderActivity extends BaseAdapter {

    Context con ;
    LayoutInflater inflater ;
    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();


    public AdapterPlaceOrderActivity(Context con ,  ArrayList<String> tyre_id_arr , ArrayList<String> tyre_name_arr , ArrayList<String> tyre_specs_arr ,ArrayList<String> tyre_price_arr ,  ArrayList<String> tyre_number_of_unit_arr ){
        this.con = con ;
        this.tyre_id_arr= tyre_id_arr;
        this.tyre_name_arr  = tyre_name_arr ;
        this.tyre_specs_arr = tyre_specs_arr ;
        this.tyre_price_arr = tyre_price_arr ;
        this.tyre_number_of_unit_arr = tyre_number_of_unit_arr;
        inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

    }


    @Override
    public int getCount() {
        return tyre_id_arr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_thye_list_in_place_order_activity, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.tyre_name_txt.setText(""+tyre_name_arr.get(position));
        holder.tyre_specs_txt.setText(""+tyre_specs_arr.get(position));
        holder.tyre_qty_txt.setText(""+tyre_number_of_unit_arr.get(position));
        holder.tyre_price_txt.setText(""+tyre_price_arr.get(position));
        int unit = Integer.parseInt(holder.tyre_qty_txt.getText().toString());
        double price = Double.parseDouble(holder.tyre_price_txt.getText().toString());
        int multiplying = unit * (int)price;
        holder.tyre_per_gross_price_txt.setText(""+multiplying);
        return view;
    }


    static class ViewHolder {
         @Bind(R.id.tyre_name_in_place_order_activity)TextView tyre_name_txt;
        @Bind(R.id.tyre_specs_in_place_order_activity)TextView tyre_specs_txt;
        @Bind(R.id.tyre_qty_in_place_order_activity)TextView tyre_qty_txt;
        @Bind(R.id.tyre_price_in_place_order_activity)TextView tyre_price_txt;
        @Bind(R.id.tyre_per_gross_in_place_order_activity)TextView tyre_per_gross_price_txt;



        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
