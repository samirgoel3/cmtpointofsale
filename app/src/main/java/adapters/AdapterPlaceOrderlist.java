package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.tyre.PlaceOrderListActivity;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import databases.databseutils.CheckFavourate;
import databases.manager.CartManager;
import databases.manager.FavourateManager;

/**
 * Created by samir on 28/07/15.
 */
public class AdapterPlaceOrderlist  extends BaseAdapter {

    Context con ;
    LayoutInflater inflater ;

    CartManager dbcartmanager ;
    FavourateManager dbfavouratamanager ;
    CheckFavourate checkfav ;

    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();


    public AdapterPlaceOrderlist(Context con ,  ArrayList<String> tyre_id_arr ,  ArrayList<String> tyre_name_arr ,  ArrayList<String> tyre_specs_arr , ArrayList<String> tyre_price_arr , ArrayList<String> tyre_number_of_unit_arr ){
        this.con = con ;
        this.tyre_id_arr = tyre_id_arr;
        this.tyre_name_arr = tyre_name_arr;
        this.tyre_specs_arr = tyre_specs_arr ;
        this.tyre_price_arr = tyre_price_arr ;
        this.tyre_number_of_unit_arr = tyre_number_of_unit_arr ;
              checkfav = new CheckFavourate();
        dbcartmanager = new CartManager(con);
        dbfavouratamanager = new FavourateManager(con);
        inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }



    @Override
    public int getCount() {
        return tyre_id_arr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_place_order_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.tyre_name_txt.setText(tyre_name_arr.get(position));
        holder.tyre_spec_txt.setText(tyre_specs_arr.get(position));
        holder.no_of_units_txt.setText(tyre_number_of_unit_arr.get(position));







//////////// minus working button
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value_nof = Integer.parseInt(holder.no_of_units_txt.getText().toString()) ;
                if(value_nof > 1){
                    holder.no_of_units_txt.setText(""+(value_nof - 1));
                    tyre_number_of_unit_arr.set(position , ""+(value_nof-1));
                    dbcartmanager.addtocart(tyre_id_arr.get(position) ,tyre_name_arr.get(position) ,tyre_specs_arr.get(position) , tyre_price_arr.get(position),holder.no_of_units_txt.getText().toString());
                } if(value_nof == 1) {
                    holder.no_of_units_txt.setText("0");
                    dbcartmanager.removeFromCart(tyre_id_arr.get(position));
                   // tyre_id_arr.remove(position);
                   // notifyDataSetChanged();
                }

                PlaceOrderListActivity.setcartfromDB();

            }
        });



///////////// plus nutton working
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value_nof = Integer.parseInt(holder.no_of_units_txt.getText().toString()) ;
                holder.no_of_units_txt.setText(""+(value_nof + 1));
                tyre_number_of_unit_arr.set(position, "" + (value_nof + 1));
                dbcartmanager.addtocart(tyre_id_arr.get(position), tyre_name_arr.get(position), tyre_specs_arr.get(position), tyre_price_arr.get(position), holder.no_of_units_txt.getText().toString());
                PlaceOrderListActivity.setcartfromDB();

            }
        });

///////////// favourate button working

         if(checkfav.idalreadyExsistinFavourate(tyre_id_arr.get(position))){
             holder.favourate_img.setVisibility(View.VISIBLE);
             holder.unfavourate_img.setVisibility(View.GONE);
         }else if(!checkfav.idalreadyExsistinFavourate(tyre_id_arr.get(position))){
             holder.favourate_img.setVisibility(View.GONE);
             holder.unfavourate_img.setVisibility(View.VISIBLE);
         }
            holder.favourate_frame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                   boolean mark =  dbfavouratamanager.addtofavourate(tyre_id_arr.get(position) , tyre_name_arr.get(position) , tyre_specs_arr.get(position) , tyre_price_arr.get(position) , tyre_number_of_unit_arr.get(position));
                         if(mark == true){
                             holder.favourate_img.setVisibility(View.VISIBLE);
                             holder.unfavourate_img.setVisibility(View.GONE);
                         }else if (mark == false){
                             holder.favourate_img.setVisibility(View.GONE);
                             holder.unfavourate_img.setVisibility(View.VISIBLE);
                         }

                }
            });


        return view;
    }


    static class ViewHolder {
        @Bind(R.id.minus_button_in_item_for_place_order_list)ImageView minus;
        @Bind(R.id.plus_button__in_item_for_place_order_list)ImageView plus;

        @Bind(R.id.no_of_unit_in_item_for_place_order_list)TextView no_of_units_txt;
        @Bind(R.id.tyre_name_in_item_for_place_order_list)TextView tyre_name_txt;
        @Bind(R.id.tyre_specs_in_item_for_place_order_list)TextView tyre_spec_txt;
        @Bind(R.id.in_stock_in_item_for_place_order_list)TextView in_stock_txt;

        @Bind(R.id.favourate_pframe_laout_in_item_for_place_order_list)FrameLayout favourate_frame;
        @Bind(R.id.favourate_image_in_item_of_place_order_list)ImageView favourate_img;
        @Bind(R.id.unfavourate_image_in_item_of_place_order_list)ImageView unfavourate_img;




        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }




}
