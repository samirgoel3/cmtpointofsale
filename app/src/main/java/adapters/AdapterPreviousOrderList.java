package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.previousorders.SpecificPreviousOrder;

import butterknife.ButterKnife;

/**
 * Created by samir on 31/07/15.
 */
public class AdapterPreviousOrderList extends BaseAdapter {

    Context con ;
    LayoutInflater inflater ;
    public  AdapterPreviousOrderList(Context con){
        this.con = con ;
        inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }


    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_thye_list_in_place_order_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                con.startActivity(new Intent(con , SpecificPreviousOrder.class));
            }
        });

        return view;
    }


    static class ViewHolder {
        // @Bind(R.id.minus_button_in_place_order_list)ImageView minus;
        // @Bind(R.id.plus_button_in_place_order_list)ImageView plus;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
