package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.salesModle.addtyres.TyreSizeActivityFromSale;

import java.util.ArrayList;

import contants.PlaceOrderModuleConstants;

/**
 * Created by spinnosolutions on 8/10/15.
 */
public class AdapterRimSizeForSale extends BaseAdapter {
    ArrayList<String >rimsizes  = new ArrayList<>();


    Context con ;
    LayoutInflater inflater ;

    public AdapterRimSizeForSale(Context con ,ArrayList<String >rimsizes ){
        this.con = con ;
        this.rimsizes = rimsizes;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return rimsizes.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view ;

        view = inflater.inflate(R.layout.item_grid_for_tyre_rim, parent, false);

        FrameLayout backgropundcolour = (FrameLayout) view.findViewById(R.id.frame_in_item_of_grid);
        final TextView rimsize = (TextView) view.findViewById(R.id.company_name_item_of_grid_view);
        rimsize.setText(rimsizes.get(position)+" inches");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                con.startActivity(new Intent(con, TyreSizeActivityFromSale.class));
                PlaceOrderModuleConstants.rimsize = rimsizes.get(position);

            }
        });

        return view;
    }
}

