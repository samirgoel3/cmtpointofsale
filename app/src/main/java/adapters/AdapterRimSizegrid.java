package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.tyre.TyreSizeActivity;

import contants.PlaceOrderModuleConstants;

/**
 * Created by samir on 05/08/15.
 */
public class AdapterRimSizegrid extends BaseAdapter {

    String [] colorsarr = {"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,};
    String [] rimsizes ;


    Context con ;
    LayoutInflater inflater ;

    public AdapterRimSizegrid(Context con ,String [] rimsizes){
        this.con = con ;
        this.rimsizes = rimsizes;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return rimsizes.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view ;

        view = inflater.inflate(R.layout.item_grid_for_tyre_rim, parent, false);

        FrameLayout backgropundcolour = (FrameLayout) view.findViewById(R.id.frame_in_item_of_grid);
        final TextView rimsize = (TextView) view.findViewById(R.id.company_name_item_of_grid_view);
        rimsize.setText(rimsizes[position]+" inches");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    con.startActivity(new Intent(con, TyreSizeActivity.class));
                    PlaceOrderModuleConstants.rimsize = rimsizes[position];

            }
        });

        return view;
    }
}
