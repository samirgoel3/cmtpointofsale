package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by samir on 31/07/15.
 */
public class AdapterSpecificOrderList extends BaseAdapter {

    Context con ;
    LayoutInflater inflater ;

    ArrayList<String> chekbox = new ArrayList<>();



    public  AdapterSpecificOrderList(Context con , ArrayList<String> chekbox){
        this.con = con ;
        inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

         this.chekbox = chekbox ;
    }

    @Override
    public int getCount() {
        return chekbox.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_specific_order_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        if(chekbox.get(position).equals("0")){
            holder.checkbox_view.setChecked(false);
        }else if(chekbox.get(position).equals("1")){
            holder.checkbox_view.setChecked(true);
        }

        holder.checkbox_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chekbox.get(position).equals("0")){
                    holder.checkbox_view.setChecked(true);
                    chekbox.set(position , "1");
                }else if(chekbox.get(position).equals("1")){
                    holder.checkbox_view.setChecked(false);
                    chekbox.set(position , "0");
                }

            }
        });

        return view;
    }


    static class ViewHolder {
         @Bind(R.id.check_box_in_specific_order_list)CheckBox checkbox_view;
        // @Bind(R.id.plus_button_in_place_order_list)ImageView plus;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
