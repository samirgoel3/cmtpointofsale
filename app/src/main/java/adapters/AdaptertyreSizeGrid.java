package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.tyre.PlaceOrderListActivity;

import checkers.Toaster;
import contants.PlaceOrderModuleConstants;

/**
 * Created by samir on 05/08/15.
 */
public class AdaptertyreSizeGrid  extends BaseAdapter {


    String [] colorsarr = {"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,"D85567" ,"E77962"  ,"EF9F62" , "D8B059" , "7FA6A5" ,"7FB385" ,"9DC95A" , "BAC962" ,};
    Context con ;
    LayoutInflater inflater ;
    String [] tyreSize ;



    public AdaptertyreSizeGrid(Context con , String [] tyreSize){
        this.con = con ;
        this.tyreSize = tyreSize ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return tyreSize.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view ;

        view = inflater.inflate(R.layout.item_grid_tyre_size, parent, false);

        FrameLayout backgropundcolour = (FrameLayout) view.findViewById(R.id.frame_in_item_of_grid);
        final TextView tyresize = (TextView) view.findViewById(R.id.company_name_item_of_grid_view);


        tyresize.setText(tyreSize[position]);


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if (tyresize.getText().toString().equals("145/80 R12") || tyresize.getText().toString().equals("155/70 R13")) {
                        con.startActivity(new Intent(con, PlaceOrderListActivity.class));
                        PlaceOrderModuleConstants.tyresize = tyresize.getText().toString();
                    } else {
                        Toaster.generatemessage(con, "Under Development");
                    }
            }
        });

        return view;
    }
}
