package com.spinno.cmtpointofsale.AddProductModule;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.spinno.cmtpointofsale.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import databases.manager.OthersManager;
import logger.Logger;

public class AddProductActivity extends Activity {

    @Bind(R.id.category_name_actionbar_txt)TextView category_name_actionbar_txt ;

    @Bind(R.id.back_button_in_set_pricing_activity)LinearLayout back_btn ;
    @Bind(R.id.select_brand)LinearLayout brand_btn ;
    @Bind(R.id.product_name)LinearLayout product_name_btn ;
    @Bind(R.id.purchase_price)LinearLayout purchase_price_btn ;
    @Bind(R.id.product_description)LinearLayout product_description_btn ;
    @Bind(R.id.taxation)LinearLayout taxation_btn ;
    @Bind(R.id.save)LinearLayout save ;

    ///////////////////   Text views and edittext
    @Bind(R.id.select_brand_txt)TextView brand_txt ;
    @Bind(R.id.product_name_edt)EditText productname_edt ;
    @Bind(R.id.purchase_price_edt)EditText purchase_price_edt ;
    @Bind(R.id.description)EditText description_edt ;
    @Bind(R.id.taxation_edt)EditText taxation_edt ;

    ////////////////////  DATABSES VARIABLES  //////////////////
       OthersManager otm ;




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        ButterKnife.bind(this);
        otm = new OthersManager(AddProductActivity.this);


////////////   back button
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


////////////////////////   BRAND
        brand_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBrandDialog();
            }


        });




//////////////  save button
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toaster.generatemessage(AddProductActivity.this ,""+" brand "+ brand_txt.getText().toString()+productname_edt.getText().toString()+"  "+ purchase_price_edt.getText().toString()+"  "+productname_edt.getText().toString()+"  "+taxation_edt.getText().toString());
              if(brand_txt.getText().toString().equals("Select Brand")
                      || productname_edt.getText().toString().equals("")
                      || purchase_price_edt.getText().toString().equals("")
                      ||taxation_edt.getText().toString().equals("")
                      ||description_edt.getText().toString().equals("")){
                  Toaster.generatemessage(AddProductActivity.this ,"Please fill the details first");
              }else {
                  Logger.d("saving product with folowing category "+AddProducModuleConstants.Categroy);
                    otm.addToOtherTable(otm.generateIdofProduct(), AddProducModuleConstants.Categroy ,brand_txt.getText().toString(),productname_edt.getText().toString(),purchase_price_edt.getText().toString(),description_edt.getText().toString(),taxation_edt.getText().toString());
                    Toaster.generatemessage(AddProductActivity.this , "Product Saved Succesfully");
                  finish();
              }
            }
        });

    }

    private void showBrandDialog() {

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Enter Brand")
                .customView(R.layout.dialog_for_add_product, true)
                .show();

        View view = dialog.getCustomView();


        ListView lv = (ListView) view.findViewById(R.id.list_view_in_dialog);
            //  lv.setAdapter(new AdapterForDialogList(AddProductActivity.this));


        final EditText edt = (EditText) view.findViewById(R.id.edit_text_add_product_dialog);
        TextView ok_btn = (TextView) view.findViewById(R.id.okbutton_in_brand_dialog);



        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               brand_txt.setText(""+edt.getText().toString());
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        category_name_actionbar_txt.setText(""+AddProducModuleConstants.Categroy);
    }

}
