package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by spinnosolutions on 8/31/15.
 */
public class AlloyMiniBucket  extends SugarRecord<AlloyMiniBucket> {

    public String AlloyId ;
    public String AlloyBrand;
    public String AlloyModelNo ;
    public String AlloyColor ;
    public String AlloyDiameter ;
    public String AlloyWidth ;
    public String AlloyBolts;
    public String AlloyPcd ;
    public String AlloyCenterBore ;
    public String AlloyOffsetNegetive ;
    public String AlloyOffsetPositive ;
    public String AlloyImage ;
    public String AlloyDealerPrice ;
    public String NoOfUnits ;
    public String AlloySellingPrice ;




    public AlloyMiniBucket(){

    }


    public AlloyMiniBucket (  String AlloyId, String AlloyBrand, String AlloyModelNo, String AlloyColor, String AlloyDiameter, String AlloyWidth, String AlloyBolts, String AlloyPcd, String AlloyCenterBore, String AlloyOffsetNegetive, String AlloyOffsetPositive, String AlloyImage, String AlloyDealerPrice, String NoOfUnits, String AlloySellingPrice ){
        this.AlloyId  = AlloyId;
        this.AlloyBrand = AlloyBrand;
        this.AlloyModelNo  = AlloyModelNo;
        this.AlloyColor = AlloyColor ;
        this.AlloyDiameter = AlloyDiameter ;
        this.AlloyWidth  = AlloyWidth;
        this.AlloyBolts = AlloyBolts;
        this.AlloyPcd  = AlloyPcd;
        this.AlloyCenterBore  = AlloyCenterBore;
        this.AlloyOffsetNegetive  = AlloyOffsetNegetive;
        this.AlloyOffsetPositive  = AlloyOffsetPositive;
        this.AlloyImage  = AlloyImage;
        this.AlloyDealerPrice  = AlloyDealerPrice;
        this.NoOfUnits  = NoOfUnits;
        this.AlloySellingPrice =AlloySellingPrice ;
    }






    public String getAlloySellingPrice() {
        return AlloySellingPrice;
    }

    public void setAlloySellingPrice(String alloySellingPrice) {
        AlloySellingPrice = alloySellingPrice;
    }

    public String getNoOfUnits() {
        return NoOfUnits;
    }

    public void setNoOfUnits(String noOfUnits) {
        NoOfUnits = noOfUnits;
    }

    public String getAlloyDealerPrice() {
        return AlloyDealerPrice;
    }

    public void setAlloyDealerPrice(String alloyDealerPrice) {
        AlloyDealerPrice = alloyDealerPrice;
    }

    public String getAlloyImage() {
        return AlloyImage;
    }

    public void setAlloyImage(String alloyImage) {
        AlloyImage = alloyImage;
    }

    public String getAlloyOffsetPositive() {
        return AlloyOffsetPositive;
    }

    public void setAlloyOffsetPositive(String alloyOffsetPositive) {
        AlloyOffsetPositive = alloyOffsetPositive;
    }

    public String getAlloyOffsetNegetive() {
        return AlloyOffsetNegetive;
    }

    public void setAlloyOffsetNegetive(String alloyOffsetNegetive) {
        AlloyOffsetNegetive = alloyOffsetNegetive;
    }

    public String getAlloyCenterBore() {
        return AlloyCenterBore;
    }

    public void setAlloyCenterBore(String alloyCenterBore) {
        AlloyCenterBore = alloyCenterBore;
    }

    public String getAlloyPcd() {
        return AlloyPcd;
    }

    public void setAlloyPcd(String alloyPcd) {
        AlloyPcd = alloyPcd;
    }

    public String getAlloyBolts() {
        return AlloyBolts;
    }

    public void setAlloyBolts(String alloyBolts) {
        AlloyBolts = alloyBolts;
    }

    public String getAlloyWidth() {
        return AlloyWidth;
    }

    public void setAlloyWidth(String alloyWidth) {
        AlloyWidth = alloyWidth;
    }

    public String getAlloyDiameter() {
        return AlloyDiameter;
    }

    public void setAlloyDiameter(String alloyDiameter) {
        AlloyDiameter = alloyDiameter;
    }

    public String getAlloyColor() {
        return AlloyColor;
    }

    public void setAlloyColor(String alloyColor) {
        AlloyColor = alloyColor;
    }

    public String getAlloyModelNo() {
        return AlloyModelNo;
    }

    public void setAlloyModelNo(String alloyModelNo) {
        AlloyModelNo = alloyModelNo;
    }

    public String getAlloyBrand() {
        return AlloyBrand;
    }

    public void setAlloyBrand(String alloyBrand) {
        AlloyBrand = alloyBrand;
    }

    public String getAlloyId() {
        return AlloyId;
    }

    public void setAlloyId(String alloyId) {
        AlloyId = alloyId;
    }





}
