package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by spinnosolutions on 8/28/15.
 */
public class AlloysTable  extends SugarRecord<AlloysTable> {



    public String AlloyId ;
    public String AlloyBrand;
    public String AlloyModelNo ;
    public String AlloyColor ;
    public String AlloyDiameter ;
    public String AlloyWidth ;
    public String AlloyBolts;
    public String AlloyPcd ;
    public String AlloyCenterBore ;
    public String AlloyOffsetNegetive ;
    public String AlloyOffsetPositive ;
    public String AlloyImage ;
    public String AlloyDealerPrice ;



    public AlloysTable(){

    }


    public AlloysTable( String AlloyId ,String AlloyBrand,String AlloyModelNo ,String AlloyColor ,String AlloyDiameter ,String AlloyWidth ,String AlloyBolts, String AlloyPcd ,String AlloyCenterBore ,String AlloyOffsetNegetive ,String AlloyOffsetPositive ,String AlloyImage ,String AlloyDealerPrice ){
        this.AlloyId  = AlloyId ;
        this.AlloyBrand = AlloyBrand;
        this.AlloyModelNo = AlloyModelNo ;
        this.AlloyColor = AlloyColor ;
        this.AlloyDiameter = AlloyDiameter ;
        this.AlloyWidth = AlloyWidth ;
        this.AlloyBolts = AlloyBolts;
        this.AlloyPcd = AlloyPcd ;
        this.AlloyCenterBore = AlloyCenterBore ;
        this.AlloyOffsetNegetive = AlloyOffsetNegetive ;
        this.AlloyOffsetPositive  = AlloyOffsetPositive;
        this.AlloyImage = AlloyImage ;
        this.AlloyDealerPrice = AlloyDealerPrice ;

    }




    public String getAlloyId() {
        return AlloyId;
    }

    public void setAlloyId(String alloyId) {
        AlloyId = alloyId;
    }

    public String getAlloyBrand() {
        return AlloyBrand;
    }

    public void setAlloyBrand(String alloyBrand) {
        AlloyBrand = alloyBrand;
    }

    public String getAlloyModelNo() {
        return AlloyModelNo;
    }

    public void setAlloyModelNo(String alloyModelNo) {
        AlloyModelNo = alloyModelNo;
    }

    public String getAlloyColor() {
        return AlloyColor;
    }

    public void setAlloyColor(String alloyColor) {
        AlloyColor = alloyColor;
    }

    public String getAlloyDiameter() {
        return AlloyDiameter;
    }

    public void setAlloyDiameter(String alloyDiameter) {
        AlloyDiameter = alloyDiameter;
    }

    public String getAlloyWidth() {
        return AlloyWidth;
    }

    public void setAlloyWidth(String alloyWidth) {
        AlloyWidth = alloyWidth;
    }

    public String getAlloyBolts() {
        return AlloyBolts;
    }

    public void setAlloyBolts(String alloyBolts) {
        AlloyBolts = alloyBolts;
    }

    public String getAlloyPcd() {
        return AlloyPcd;
    }

    public void setAlloyPcd(String alloyPcd) {
        AlloyPcd = alloyPcd;
    }

    public String getAlloyCenterBore() {
        return AlloyCenterBore;
    }

    public void setAlloyCenterBore(String alloyCenterBore) {
        AlloyCenterBore = alloyCenterBore;
    }

    public String getAlloyOffsetNegetive() {
        return AlloyOffsetNegetive;
    }

    public void setAlloyOffsetNegetive(String alloyOffsetNegetive) {
        AlloyOffsetNegetive = alloyOffsetNegetive;
    }

    public String getAlloyOffsetPositive() {
        return AlloyOffsetPositive;
    }

    public void setAlloyOffsetPositive(String alloyOffsetPositive) {
        AlloyOffsetPositive = alloyOffsetPositive;
    }

    public String getAlloyImage() {
        return AlloyImage;
    }

    public void setAlloyImage(String alloyImage) {
        AlloyImage = alloyImage;
    }

    public String getAlloyDealerPrice() {
        return AlloyDealerPrice;
    }

    public void setAlloyDealerPrice(String alloyDealerPrice) {
        AlloyDealerPrice = alloyDealerPrice;
    }




}
