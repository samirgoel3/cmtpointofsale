package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by samir on 29/07/15.
 */
public class CartTable extends SugarRecord<CartTable> {

    public String TyreId ;
    public String TyreName ;
    public String TyreSpecs ;
    public String TyrePrice ;
    public  String TyreNoOfunits ;


    public CartTable(){
    }


    public CartTable(String TyreId, String TyreName, String TyreSpecs, String TyrePrice, String TyreNoOfunits) {
        this.TyreId = TyreId;
        this.TyreName = TyreName;
        this.TyreSpecs = TyreSpecs;
        this.TyrePrice = TyrePrice;
        this.TyreNoOfunits = TyreNoOfunits;
    }


    public String getTyreId() {
        return TyreId;
    }

    public void setTyreId(String tyreId) {
        TyreId = tyreId;
    }

    public String getTyrePrice() {
        return TyrePrice;
    }

    public void setTyrePrice(String tyrePrice) {
        TyrePrice = tyrePrice;
    }

    public String getTyreNoOfunits() {
        return TyreNoOfunits;
    }

    public void setTyreNoOfunits(String tyreNoOfunits) {
        TyreNoOfunits = tyreNoOfunits;
    }

    public String getTyreName() {
        return TyreName;
    }

    public void setTyreName(String tyreName) {
        TyreName = tyreName;
    }

    public String getTyreSpecs() {
        return TyreSpecs;
    }

    public void setTyreSpecs(String tyreSpecs) {
        TyreSpecs = tyreSpecs;
    }
}
