package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by spinnosolutions on 8/13/15.
 */
public class InvoiceBucketTable extends SugarRecord<InvoiceBucketTable> {

    public String TyreId ;
    public String VehicleType ;
    public String BrandName ;
    public String TyreRimSize ;
    public String TyreName ;
    public String TyreSpecs ;
    public String TyreType ;
    public String TyreFavourite ;
    public String TyreNoOfunits ;
    public String TyreOffer ;
    public String TyreOfferId ;
    public String TyreOfferCompany ;
    public String TyreOfferName ;
    public String TyreOfferDescription ;
    public String TyreOfferStart ;
    public String TyreOfferEnd ;
    public String TyreOfferUnit ;





    public InvoiceBucketTable(String TyreId,
                              String VehicleType,
                              String BrandName,
                              String TyreRimSize,
                              String TyreName,
                              String TyreSpecs,
                              String TyreType,
                              String TyreFavourite,
                              String TyreNoOfunits,
                              String TyreOffer,
                              String TyreOfferId,
                              String TyreOfferCompany,
                              String TyreOfferName,
                              String TyreOfferDescription,
                              String TyreOfferStart,
                              String TyreOfferEnd,
                              String TyreOfferUnit,
                              String TyrePrice) {



        this.TyreId  = TyreId;
        this.VehicleType  = VehicleType;
        this.BrandName  =BrandName;
        this.TyreRimSize=  TyreRimSize ;
        this.TyreName = TyreName ;
        this.TyreSpecs = TyreSpecs ;
        this.TyreType = TyreType ;
        this.TyreFavourite = TyreFavourite ;
        this.TyreNoOfunits = TyreNoOfunits ;
        this.TyreOffer = TyreOffer ;
        this.TyreOfferId = TyreOfferId ;
        this.TyreOfferCompany = TyreOfferCompany ;
        this.TyreOfferName = TyreOfferName ;
        this.TyreOfferDescription = TyreOfferDescription ;
        this.TyreOfferStart = TyreOfferStart ;
        this.TyreOfferEnd = TyreOfferEnd ;
        this.TyreOfferUnit = TyreOfferUnit ;
        this.TyrePrice = TyrePrice ;

    }











    public String getTyrePrice() {
        return TyrePrice;
    }

    public void setTyrePrice(String tyrePrice) {
        TyrePrice = tyrePrice;
    }

    public String getTyreOfferUnit() {
        return TyreOfferUnit;
    }

    public void setTyreOfferUnit(String tyreOfferUnit) {
        TyreOfferUnit = tyreOfferUnit;
    }

    public String getTyreOfferEnd() {
        return TyreOfferEnd;
    }

    public void setTyreOfferEnd(String tyreOfferEnd) {
        TyreOfferEnd = tyreOfferEnd;
    }

    public String getTyreOfferStart() {
        return TyreOfferStart;
    }

    public void setTyreOfferStart(String tyreOfferStart) {
        TyreOfferStart = tyreOfferStart;
    }

    public String getTyreOfferDescription() {
        return TyreOfferDescription;
    }

    public void setTyreOfferDescription(String tyreOfferDescription) {
        TyreOfferDescription = tyreOfferDescription;
    }

    public String getTyreOfferName() {
        return TyreOfferName;
    }

    public void setTyreOfferName(String tyreOfferName) {
        TyreOfferName = tyreOfferName;
    }

    public String getTyreOfferCompany() {
        return TyreOfferCompany;
    }

    public void setTyreOfferCompany(String tyreOfferCompany) {
        TyreOfferCompany = tyreOfferCompany;
    }

    public String getTyreOfferId() {
        return TyreOfferId;
    }

    public void setTyreOfferId(String tyreOfferId) {
        TyreOfferId = tyreOfferId;
    }

    public String getTyreOffer() {
        return TyreOffer;
    }

    public void setTyreOffer(String tyreOffer) {
        TyreOffer = tyreOffer;
    }

    public String getTyreNoOfunits() {
        return TyreNoOfunits;
    }

    public void setTyreNoOfunits(String tyreInStock) {
        TyreNoOfunits = tyreInStock;
    }

    public String getTyreFavourite() {
        return TyreFavourite;
    }

    public void setTyreFavourite(String tyreFavourite) {
        TyreFavourite = tyreFavourite;
    }

    public String getTyreType() {
        return TyreType;
    }

    public void setTyreType(String tyreType) {
        TyreType = tyreType;
    }

    public String getTyreSpecs() {
        return TyreSpecs;
    }

    public void setTyreSpecs(String tyreSpecs) {
        TyreSpecs = tyreSpecs;
    }

    public String getTyreName() {
        return TyreName;
    }

    public void setTyreName(String tyreName) {
        TyreName = tyreName;
    }

    public String getTyreRimSize() {
        return TyreRimSize;
    }

    public void setTyreRimSize(String tyreRimSize) {
        TyreRimSize = tyreRimSize;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String getVehicleType() {
        return VehicleType;
    }

    public void setVehicleType(String vehicleType) {
        VehicleType = vehicleType;
    }

    public String getTyreId() {
        return TyreId;
    }

    public void setTyreId(String tyreId) {
        TyreId = tyreId;
    }

    public String TyrePrice ;

    public InvoiceBucketTable(){

    }
}
