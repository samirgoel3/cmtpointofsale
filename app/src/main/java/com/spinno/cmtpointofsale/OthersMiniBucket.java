package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by spinnosolutions on 8/26/15.
 */
public class OthersMiniBucket extends SugarRecord<OthersMiniBucket> {


    public  String OtherId ;
    public  String OtherCategory ;
    public  String OtherBrand ;
    public  String OtherProductName ;
    public  String OtherDealerPrice ;
    public  String OtherProductDescription ;
    public  String OtherTaxation ;
    public  String NoOfUnits  ;
    public  String OthersSellingPrice  ;




    public OthersMiniBucket(  String OtherId ,String OtherCategory ,String OtherBrand ,String OtherProductName ,String OtherDealerPrice ,String OtherProductDescription ,String OtherTaxation ,String NoOfUnits  ,String OthersSellingPrice ){
        this.OtherId    =  OtherId;
        this.OtherCategory = OtherCategory ;
        this.OtherBrand = OtherBrand ;
        this.OtherProductName = OtherProductName ;
        this.OtherDealerPrice =OtherDealerPrice ;
        this.OtherProductDescription = OtherProductDescription ;
        this.OtherTaxation = OtherTaxation ;
        this.NoOfUnits = NoOfUnits  ;
        this.OthersSellingPrice = OthersSellingPrice  ;
    }


    public String getOthersSellingPrice() {
        return OthersSellingPrice;
    }

    public void setOthersSellingPrice(String othersSellingPrice) {
        OthersSellingPrice = othersSellingPrice;
    }

    public String getNoOfUnits() {
        return NoOfUnits;
    }

    public void setNoOfUnits(String noOfUnits) {
        NoOfUnits = noOfUnits;
    }

    public String getOtherTaxation() {
        return OtherTaxation;
    }

    public void setOtherTaxation(String otherTaxation) {
        OtherTaxation = otherTaxation;
    }

    public String getOtherProductDescription() {
        return OtherProductDescription;
    }

    public void setOtherProductDescription(String otherProductDescription) {
        OtherProductDescription = otherProductDescription;
    }

    public String getOtherDealerPrice() {
        return OtherDealerPrice;
    }

    public void setOtherDealerPrice(String otherDealerPrice) {
        OtherDealerPrice = otherDealerPrice;
    }

    public String getOtherProductName() {
        return OtherProductName;
    }

    public void setOtherProductName(String otherProductName) {
        OtherProductName = otherProductName;
    }

    public String getOtherBrand() {
        return OtherBrand;
    }

    public void setOtherBrand(String otherBrand) {
        OtherBrand = otherBrand;
    }

    public String getOtherCategory() {
        return OtherCategory;
    }

    public void setOtherCategory(String otherCategory) {
        OtherCategory = otherCategory;
    }

    public String getOtherId() {
        return OtherId;
    }

    public void setOtherId(String otherId) {
        OtherId = otherId;
    }


    public OthersMiniBucket(){

    }

}
