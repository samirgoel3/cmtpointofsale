package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by spinnosolutions on 8/26/15.
 */
public class OthersTable extends SugarRecord<OthersTable> {

    public  String OtherId ;
    public  String OtherCategory ;
    public  String OtherBrand ;
    public  String OtherProductName ;
    public  String OtherDealerPrice ;
    public  String OtherProductDescription ;
    public  String OtherTaxation ;




    public OthersTable(  String Otherid ,String OtherCategory ,String OtherBrand ,String OtherProductName ,String OtherDealerPrice ,String OtherProductDescription ,String OtherTaxation ){
       // Logger.d("Value getin=g for saving in table constant   " +  Otherid + OtherCategory + OtherBrand + OtherProductName + OtherDealerPrice + OtherProductDescription + OtherTaxation );
        this.OtherId = Otherid ;
        this.OtherCategory = OtherCategory ;
        this.OtherBrand = OtherBrand ;
        this.OtherProductName = OtherProductName ;
        this.OtherDealerPrice = OtherDealerPrice ;
        this.OtherProductDescription = OtherProductDescription ;
        this.OtherTaxation = OtherTaxation ;
    }



    public String getOtherTaxation() {
        return OtherTaxation;
    }

    public void setOtherTaxation(String otherTaxation) {
        OtherTaxation = otherTaxation;
    }

    public String getOtherId() {
        return OtherId;
    }

    public void setOtherId(String otherId) {
        OtherId = otherId;
    }

    public String getOtherCategory() {
        return OtherCategory;
    }

    public void setOtherCategory(String otherCategory) {
        OtherCategory = otherCategory;
    }

    public String getOtherBrand() {
        return OtherBrand;
    }

    public void setOtherBrand(String otherBrand) {
        OtherBrand = otherBrand;
    }

    public String getOtherProductName() {
        return OtherProductName;
    }

    public void setOtherProductName(String otherProductName) {
        OtherProductName = otherProductName;
    }

    public String getOtherDealerPrice() {
        return OtherDealerPrice;
    }

    public void setOtherDealerPrice(String otherDealerPrice) {
        OtherDealerPrice = otherDealerPrice;
    }

    public String getOtherProductDescription() {
        return OtherProductDescription;
    }

    public void setOtherProductDescription(String otherProductDescription) {
        OtherProductDescription = otherProductDescription;
    }



    public OthersTable(){

    }








}
