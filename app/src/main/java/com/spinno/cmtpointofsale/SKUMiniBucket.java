package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by spinnosolutions on 8/25/15.
 */
public class SKUMiniBucket extends SugarRecord<SKUMiniBucket> {

    public String TyreId ;
    public String TyreVehicleType ;
    public String TyreBrandName ;
    public String TyreRimSize ;
    public String TyreSize ;
    public String TyrePattern ;
    public String TyreSpeedRating ;
    public String TyreType ;
    public String TyreDealerPrice ;
    public String NoOfUnits ;
    public String SellingPrice ;


    public SKUMiniBucket(String TyreId, String TyreVehicleType, String TyreBrandName, String TyreRimSize, String TyreSize, String TyrePattern, String TyreSpeedRating, String TyreType, String TyreDealerPrice ,String NoOfUnits , String SellingPrice){

        this.TyreId = TyreId ;
        this.TyreVehicleType = TyreVehicleType ;
        this.TyreBrandName = TyreBrandName ;
        this.TyreRimSize = TyreRimSize ;
        this.TyreSize = TyreSize ;
        this.TyrePattern = TyrePattern ;
        this.TyreSpeedRating = TyreSpeedRating ;
        this.TyreType = TyreType ;
        this.TyreDealerPrice = TyreDealerPrice ;
        this.NoOfUnits  = NoOfUnits;
        this.SellingPrice  = SellingPrice ;
    }


    public String getTyreId() {
        return TyreId;
    }

    public void setTyreId(String tyreId) {
        TyreId = tyreId;
    }

    public String getTyreVehicleType() {
        return TyreVehicleType;
    }

    public void setTyreVehicleType(String tyreVehicleType) {
        TyreVehicleType = tyreVehicleType;
    }

    public String getTyreBrandName() {
        return TyreBrandName;
    }

    public void setTyreBrandName(String tyreBrandName) {
        TyreBrandName = tyreBrandName;
    }

    public String getTyreRimSize() {
        return TyreRimSize;
    }

    public void setTyreRimSize(String tyreRimSize) {
        TyreRimSize = tyreRimSize;
    }

    public String getTyreSize() {
        return TyreSize;
    }

    public void setTyreSize(String tyreSize) {
        TyreSize = tyreSize;
    }

    public String getTyrePattern() {
        return TyrePattern;
    }

    public void setTyrePattern(String tyrePattern) {
        TyrePattern = tyrePattern;
    }

    public String getTyreSpeedRating() {
        return TyreSpeedRating;
    }

    public void setTyreSpeedRating(String tyreSpeedRating) {
        TyreSpeedRating = tyreSpeedRating;
    }

    public String getTyreType() {
        return TyreType;
    }

    public void setTyreType(String tyreType) {
        TyreType = tyreType;
    }

    public String getTyreDealerPrice() {
        return TyreDealerPrice;
    }

    public void setTyreDealerPrice(String tyreDealerPrice) {
        TyreDealerPrice = tyreDealerPrice;
    }

    public String getNoOfUnits() {
        return NoOfUnits;
    }

    public void setNoOfUnits(String noOfUnits) {
        NoOfUnits = noOfUnits;
    }

    public String getSellingPrice() {
        return SellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        SellingPrice = sellingPrice;
    }



    public SKUMiniBucket(){

    }




}
