package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by spinnosolutions on 8/25/15.
 */
public class TubeMiniBucket  extends SugarRecord<TubeMiniBucket>{

    public String TubeId ;
    public String TubeVehicleType ;
    public String TubeBrand ;
    public String TubeRimSize ;
    public String TubeSize ;
    public String TubePrice ;
    public String NoofUnits ;
    public String SellingPrice ;





    public TubeMiniBucket(String TubeId ,String TubeVehicleType ,String TubeBrand  , String TubeRimSize ,String TubeSize  , String TubePrice  , String NoofUnits , String SellingPrice){

        this.TubeId = TubeId ;
        this.TubeVehicleType = TubeVehicleType ;
        this.TubeBrand = TubeBrand ;
        this.TubeRimSize =  TubeRimSize ;
        this.TubeSize = TubeSize ;
        this.TubePrice = TubePrice ;
        this.NoofUnits = NoofUnits ;
        this.SellingPrice = NoofUnits ;

    }








    public String getSellingPrice() {
        return SellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        SellingPrice = sellingPrice;
    }

    public String getNoofUnits() {
        return NoofUnits;
    }

    public void setNoofUnits(String noofUnits) {
        NoofUnits = noofUnits;
    }

    public String getTubePrice() {
        return TubePrice;
    }

    public void setTubePrice(String tubePrice) {
        TubePrice = tubePrice;
    }

    public String getTubeSize() {
        return TubeSize;
    }

    public void setTubeSize(String tubeSize) {
        TubeSize = tubeSize;
    }

    public String getTubeRimSize() {
        return TubeRimSize;
    }

    public void setTubeRimSize(String tubeRimSize) {
        TubeRimSize = tubeRimSize;
    }

    public String getTubeBrand() {
        return TubeBrand;
    }

    public void setTubeBrand(String tubeBrand) {
        TubeBrand = tubeBrand;
    }

    public String getTubeVehicleType() {
        return TubeVehicleType;
    }

    public void setTubeVehicleType(String tubeVehicleType) {
        TubeVehicleType = tubeVehicleType;
    }

    public String getTubeId() {
        return TubeId;
    }

    public void setTubeId(String tubeId) {
        TubeId = tubeId;
    }




    public TubeMiniBucket(){

    }



}
