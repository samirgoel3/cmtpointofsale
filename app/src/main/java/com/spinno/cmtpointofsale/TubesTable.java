package com.spinno.cmtpointofsale;

import com.orm.SugarRecord;

/**
 * Created by spinnosolutions on 8/24/15.
 */
public class TubesTable extends SugarRecord<TubesTable> {

    public String TubeId ;
    public String TubeVehicleType ;
    public String TubeBrand ;
    public String TubeRimSize ;
    public String TubeSize ;
    public String TubePrice ;





    public TubesTable(String TubeId ,String TubeVehicleType ,String TubeBrand  , String TubeRimSize ,String TubeSize  , String TubePrice ){

        this.TubeId = TubeId ;
        this.TubeVehicleType = TubeVehicleType ;
        this.TubeBrand = TubeBrand ;
        this.TubeRimSize =  TubeRimSize ;
        this.TubeSize = TubeSize ;
        this.TubePrice = TubePrice ;

    }

    public String getTubeId() {
        return TubeId;
    }

    public void setTubeId(String tubeId) {
        TubeId = tubeId;
    }

    public String getTubeVehicleType() {
        return TubeVehicleType;
    }

    public void setTubeVehicleType(String tubeVehicleType) {
        TubeVehicleType = tubeVehicleType;
    }

    public String getTubeBrand() {
        return TubeBrand;
    }

    public void setTubeBrand(String tubeBrand) {
        TubeBrand = tubeBrand;
    }

    public String getTubeSize() {
        return TubeSize;
    }

    public void setTubeSize(String tubeSize) {
        TubeSize = tubeSize;
    }


    public String getTubePrice() {
        return TubePrice;
    }

    public void setTubePrice(String tubePrice) {
        TubePrice = tubePrice;
    }



    public String getTubeRimSize() {
        return TubeRimSize;
    }

    public void setTubeRimSize(String tubeRimSize) {
        TubeRimSize = tubeRimSize;
    }




    public TubesTable(){

    }
}
