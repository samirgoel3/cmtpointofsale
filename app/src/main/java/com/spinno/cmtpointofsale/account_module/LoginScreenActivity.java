package com.spinno.cmtpointofsale.account_module;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.startupscreens.SKULoadingActivity;

import butterknife.Bind;
import butterknife.ButterKnife;


public class LoginScreenActivity extends Activity {


    @Bind(R.id.splashlogo)ImageView splashlogo;
    @Bind(R.id.splash_banner)ImageView splasbanner;
    @Bind(R.id.sign_in_view_in_splash_activity)View signinview;
    @Bind(R.id.newtosignupline)View newtosignuptext;
    @Bind(R.id.sign_in_button_in_splash_Activity)Button signinbtn;
    @Bind(R.id.sign_up_textbtn_in_splash_activity)TextView signupbtn;
    @Bind(R.id.phone_in_splash_screen)EditText phoneedt;
    @Bind(R.id.password_in_splash_screen)EditText passwordedt;


   public static  Activity loginscreenactivity ;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen_activity);
        ButterKnife.bind(this);
        loginscreenactivity = this ;

        signinview.setVisibility(View.INVISIBLE);
        newtosignuptext.setVisibility(View.INVISIBLE);

        splasbanner.setImageResource(R.drawable.splash_banner);



        Animation animTranslate  = AnimationUtils.loadAnimation(LoginScreenActivity.this, R.anim.anim);



        animTranslate.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                signinview.setVisibility(View.VISIBLE);
                newtosignuptext.setVisibility(View.VISIBLE);
                Animation animFade = AnimationUtils.loadAnimation(LoginScreenActivity.this, R.anim.fade);
                signinview.startAnimation(animFade);
            }
        });


        splashlogo.setAnimation(animTranslate);




        signinbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (phoneedt.getText().toString().equals("") || passwordedt.getText().toString().equals("")) {
                    Toast.makeText(LoginScreenActivity.this, "Required field is empty", Toast.LENGTH_SHORT).show();

                } else {
                    startActivity(new Intent(LoginScreenActivity.this, SKULoadingActivity.class));
                    finish();
                }


            }
        });


        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginScreenActivity.this, SignUpActivity.class));
            }
        });


    }


}
