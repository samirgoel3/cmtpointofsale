package com.spinno.cmtpointofsale.account_module;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.startupscreens.MainActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.EmailChecker;
import checkers.Toaster;


public class SignUpActivity extends Activity {

    @Bind(R.id.back_login_btn_in_sign_up_activity)TextView backbtn;
    @Bind(R.id.first_name_in_sign_up_activity)EditText firstnameedt;
    @Bind(R.id.last_name_in_sign_up_activity)EditText lastnameedt;
    @Bind(R.id.email_in_sign_up_activity)EditText emailedt;
    @Bind(R.id.mobile_in_sign_up_activity)EditText mobileedt;
    @Bind(R.id.buisness_name_in_sign_up_activity)EditText buisnessnameedt;
    @Bind(R.id.buisness_address_in_sign_up_activity)EditText buisnessaddressedt;
    @Bind(R.id.city_in_sign_up_activity)EditText cityedt;
    @Bind(R.id.pin_in_sign_up_activity)EditText pinedt;
    @Bind(R.id.password_in_sign_up_activity)EditText passwordedt;
    @Bind(R.id.confirm_password_in_sign_up_activity)EditText confirmpasswordedt;
    @Bind(R.id.sign_button_in_sign_up_activity)Button signupbtn;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });





        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(EmailChecker.isEmailIsCorrect(emailedt.getText().toString())){
                    if(checkpassword()){
                        registerUser(firstnameedt.getText().toString(), lastnameedt.getText().toString(), emailedt.getText().toString(), mobileedt.getText().toString(), buisnessnameedt.getText().toString(), buisnessaddressedt.getText().toString(), cityedt.getText().toString(), pinedt.getText().toString(), passwordedt.getText().toString(), confirmpasswordedt.getText().toString());

                    }else {
                        Toaster.generatemessage(SignUpActivity.this, "Password does not match");
                    }
                }else {
                    Toaster.generatemessage(SignUpActivity.this , "Please enter a valid email address");
                }
            }
        });
    }




    public boolean checkpassword(){
        boolean check ;
        if(passwordedt.getText().toString().equals(confirmpasswordedt.getText().toString())){
            check = true ;
        }else {
            check = false;
        }

        return check;
    }



    private void registerUser(String firstnamestr, String latnamestr, String emailstr, String mobilestr, String buisnessnamestr, String buisnessaddressstr, String scitystr6, String pinstr, String spasswordstr, String confirmpasswordstr) {
        startActivity(new Intent(SignUpActivity.this , MainActivity.class));
        LoginScreenActivity.loginscreenactivity.finish();
        finish();
    }


}
