package com.spinno.cmtpointofsale.placeordermodule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.spinno.cmtpointofsale.CartTable;
import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.tyre.ConfirmOrderActivity;

import java.util.ArrayList;
import java.util.List;

import adapters.AdapterForCartList;
import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;
import databases.manager.CartManager;

public class CartActivity extends Activity {
    @Bind(R.id.list_view_in_cart_Activity)ListView list_in_cart_activity;
    @Bind(R.id.back_button_in_cart_activty)LinearLayout back_button;
    @Bind(R.id.place_order_button_in_cart_activity)LinearLayout place_order_button;
    @Bind(R.id.when_there_is_data_in_cart_layout)View whenthere_is_data_in_cart_layout;
    @Bind(R.id.when_there_is_no_data_in_cart_layout)View whenthere_is_no_data_in_cart_layout;
    public static TextView total_item_on_action_bar;



    public static CartManager dbcartmanager ;
    List<CartTable> carttable  ;
    CartManager adbcartmanager ;
    public static  final  String classname = "CartActivity";

    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        total_item_on_action_bar = (TextView) findViewById(R.id.total_item_in_cart_Activity);
        dbcartmanager = new CartManager(CartActivity.this) ;
        ConstantClass.whichActivity = classname ;
        whenthere_is_data_in_cart_layout.setVisibility(View.GONE);
        whenthere_is_no_data_in_cart_layout.setVisibility(View.GONE);
 //        total_item_on_action_bar.setText("" + dbcartmanager.countTotalNumbersOfTyre());


        dbcartmanager = new CartManager(CartActivity.this);

        carttable = dbcartmanager.getalldata();
        Toaster.generatemessage(CartActivity.this , "Total type of tyre in cart "+carttable.size());

        if(carttable.size()>0){
            whenthere_is_data_in_cart_layout.setVisibility(View.VISIBLE);
            whenthere_is_no_data_in_cart_layout.setVisibility(View.GONE);

            for(int i = 0 ; i<carttable.size() ; i++){
                tyre_id_arr.add(carttable.get(i).getTyreId());
                tyre_name_arr.add(carttable.get(i).getTyreName());
                tyre_specs_arr.add(carttable.get(i).getTyreSpecs());
                tyre_price_arr.add(carttable.get(i).getTyrePrice());
                tyre_number_of_unit_arr.add(carttable.get(i).getTyreNoOfunits());

            }
            list_in_cart_activity.setAdapter(new AdapterForCartList(CartActivity.this ,tyre_id_arr , tyre_name_arr , tyre_specs_arr , tyre_price_arr ,tyre_number_of_unit_arr));
        }else {
            whenthere_is_no_data_in_cart_layout.setVisibility(View.VISIBLE);
            whenthere_is_data_in_cart_layout.setVisibility(View.GONE);

        }


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });










        place_order_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final MaterialDialog dialog = new MaterialDialog.Builder(CartActivity.this)
                        .title("Select your order type")
                        .customView(R.layout.custom_dialog_layout_aftyer_place_order, true)


                        .callback(new MaterialDialog.ButtonCallback() {
                                      @Override
                                      public void onPositive(MaterialDialog dialog) {
                                      }

                                  }


                        ).build();
                LinearLayout confirmorder = (LinearLayout) dialog.getCustomView().findViewById(R.id.confirm_order_in_cart_cart_activity);

                confirmorder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(CartActivity.this, ConfirmOrderActivity.class));
                        PlaceOrderModuleConstants.ordertype = "order_with_price";
                        dialog.dismiss();
                    }
                });



                LinearLayout provisionalmorder = (LinearLayout) dialog.getCustomView().findViewById(R.id.provisional_order_in_cart_activity);


                provisionalmorder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(CartActivity.this, ConfirmOrderActivity.class));
                        PlaceOrderModuleConstants.ordertype = "order_without_price";

                        dialog.dismiss();
                    }
                });


                dialog.show();
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        showTextonActionBar();

    }

    public static void showTextonActionBar() {
        total_item_on_action_bar.setText(""+dbcartmanager.countTotalNumbersOfTyre());
    }

}
