package com.spinno.cmtpointofsale.placeordermodule.favourate;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.spinno.cmtpointofsale.FavourateTable;
import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;
import java.util.List;

import adapters.AdapterFavouratefavourateList;
import butterknife.Bind;
import butterknife.ButterKnife;
import databases.manager.FavourateManager;

public class FavourateListActivity extends Activity {

    @Bind(R.id.back_layout_in_favourate_list_activity)LinearLayout back_btn;
    @Bind(R.id.when_there_is_favourate_layout)View favourate_layout;
    @Bind(R.id.when_there_is_no_favourate_layout)View no_favourate_layout;

    @Bind(R.id.list_in_favourate_list_activity)ListView list_favourate_list;

    FavourateManager dbFavourateManager ;


    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourate_list);
        ButterKnife.bind(this);
        dbFavourateManager = new FavourateManager(FavourateListActivity.this);

        setvisibilityOfLayout();



        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });







    }

    private void setvisibilityOfLayout() {
        if(dbFavourateManager.countTotalTypesOfTyre()==0){
            favourate_layout.setVisibility(View.GONE);
            no_favourate_layout.setVisibility(View.VISIBLE);
        }else if(dbFavourateManager.countTotalTypesOfTyre()>0){
            favourate_layout.setVisibility(View.VISIBLE);
            no_favourate_layout.setVisibility(View.GONE);
            loadDataintothelist();
        }
    }

    private void loadDataintothelist() {
        List<FavourateTable> data = dbFavourateManager.getalldata() ;
        for(int i = 0 ; i< data.size();i++){
            tyre_id_arr.add(data.get(i).getTyreId());
            tyre_name_arr.add(data.get(i).getTyreName());
            tyre_specs_arr.add(data.get(i).getTyreSpecs());
            tyre_price_arr.add(data.get(i).getTyrePrice());
            tyre_number_of_unit_arr.add(data.get(i).getTyreNoOfunits());
        }
        list_favourate_list.setAdapter(new AdapterFavouratefavourateList(FavourateListActivity.this , tyre_id_arr ,tyre_name_arr , tyre_specs_arr ,tyre_price_arr , tyre_number_of_unit_arr));

    }
}
