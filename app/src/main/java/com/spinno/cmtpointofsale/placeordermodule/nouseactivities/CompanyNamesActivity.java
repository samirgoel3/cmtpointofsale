package com.spinno.cmtpointofsale.placeordermodule.nouseactivities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import checkers.Toaster;
import views.dragabblegridview.DraggableGridView;

public class CompanyNamesActivity extends Activity {

    String [] colorsarr = {"e74c3c" ,"2ecc71" , "e67e22" ,"9b59b6" , "f1c40f" ,"3498db" , "f39c12" , "34495e" , "e74c3c" ,"2ecc71" , "e67e22" ,"9b59b6" , "f1c40f" ,"3498db" , "f39c12" , "34495e" , "e74c3c" ,"2ecc71" , "e67e22" ,"9b59b6" , "f1c40f" ,"3498db" , "f39c12" , "34495e"};
    String [] companyarr = {"Apollo" ,"bridgestone" ,"CEAT" , "Continental" , "Cooper" , "Falken" , "GoodYear" , "Hankook" , "Ironman" , "JK Tyre" , "kenda" , "kumho" ,"Leao" , "Maxxis" , "Metro" , "Michelin" , "MRF" , "Nexan" ,"Pirelli" , "Yokohama"};

    DraggableGridView dgv;
    ArrayList<String> poem = new ArrayList<String>();

    LinearLayout pulldoen_btn ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_names);
        pulldoen_btn = (LinearLayout) findViewById(R.id.pull_down_button_in_company_names_activity);

        dgv = ((DraggableGridView)findViewById(R.id.vgv));


        setListeners();
    }
    private void setListeners()
    {



        for(int i = 0 ; i<companyarr.length ; i++){
            String word = ""+companyarr[i];
            ImageView view = new ImageView(CompanyNamesActivity.this);
            view.setImageBitmap(getThumb(word , i));
            dgv.addView(view);
            poem.add(word);
        }



        dgv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toaster.generatemessage(CompanyNamesActivity.this, "" + companyarr[position]);
                  finish();
            }
        });


        pulldoen_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private Bitmap getThumb(String s , int i)
    {
        Bitmap bmp = Bitmap.createBitmap(150, 150, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bmp);
        Paint paint = new Paint();

        //paint.setColor(Color.rgb(random.nextInt(128), random.nextInt(128), random.nextInt(128)));

        paint.setColor(Color.parseColor("#" + colorsarr[i]));
        paint.setTextSize(20);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        canvas.drawRect(new Rect(0, 0, 150, 150), paint);
        paint.setColor(Color.WHITE);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(s, 75, 75, paint);

        return bmp;
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.abc_slide_out_bottom);
    }

}
