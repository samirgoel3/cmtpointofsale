package com.spinno.cmtpointofsale.placeordermodule.nouseactivities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.favourate.FavourateListActivity;
import com.spinno.cmtpointofsale.placeordermodule.tyre.PlaceOrderListActivity;
import com.spinno.cmtpointofsale.placeordermodule.previousorders.PreviousOrderActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import views.togglebutton.ToggleButton;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class FilterActivity extends Activity {


    @Bind(R.id.select_for_previous_layout_in_filter_activity)LinearLayout selectfrom_previous_btn;
    @Bind(R.id.select_from_favourate_layout_in_filter_activity)LinearLayout selectfrom_favourates_btn;
    @Bind(R.id.back_layout_in_filter_activity)LinearLayout back_btn;



    @Bind(R.id.select_manufacture_in_filter_activyt)TextView selectmanufacture_btn;
    @Bind(R.id.select_vehicle_type_in_filter_activyt)TextView selectvehicletype_btn;
    @Bind(R.id.tyre_type_in_filter_activyt)TextView tyre_type_btn;
    @Bind(R.id.select_tyre_size_in_filter_activyt)TextView tyre_size_btn;

    @Bind(R.id.rim_size_in_filter_activiy)TextView rim_size_btn_txt;

    @Bind(R.id.toggle_button_in_filter_activity)ToggleButton toggle_btn;


    @Bind(R.id.tube_tyre_image)ImageView tube_tyre_img;
    @Bind(R.id.tube_less_tyre_image)ImageView tube_less_tyre_img;

    @Bind(R.id.search_button_in_filter_activity)Button search_btn;

    public static String  MANUFACTURE_KEY= "manufacturekey";
    public static String  VEHICLE_TYPE_KEY= "vehiclekey";
    public static String  TYRE_SIZE_KEY= "tyre_sozekey";
    public static String  TYRE_TYPE_KEY= "tyre_type_key";
    public static  final  String classname = "FilterActivity";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);

        setViewAnimation();

        ConstantClass.whichActivity = classname ;

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        selectfrom_previous_btn.setOnClickListener(selectFrompreviousListener);
        selectfrom_favourates_btn.setOnClickListener(selectFromFavouratelistener);
        selectmanufacture_btn.setOnClickListener(selectManufactureListener);
        selectvehicletype_btn.setOnClickListener(selectvehicletypeListener);
        tyre_type_btn.setOnClickListener(tyretypeListener);
        rim_size_btn_txt.setOnClickListener(rimSizeListener);
        tyre_size_btn.setOnClickListener(tyreSizelistener);
        search_btn.setOnClickListener(searchListener);






/////////////// toggle functionality
        tube_tyre_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle_btn.setToggleOff();
                tyre_type_btn.setText("Tube");
            }
        });


        tube_less_tyre_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle_btn.setToggleOn();
                tyre_type_btn.setText("Tube less");
            }
        });


        toggle_btn.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on == true) {
                    tyre_type_btn.setText("Tubeless");
                } else if (on == false) {
                    tyre_type_btn.setText("Tube");
                }
            }
        });







    }


    private void setViewAnimation() {
        YoYo.with(Techniques.BounceInLeft).duration(1000).playOn(findViewById(R.id.bill_icon_for_animation));
        YoYo.with(Techniques.BounceInRight).duration(1000).playOn(findViewById(R.id.star_icon_for_animation));

    }


    ////listeners/////
    View.OnClickListener selectFrompreviousListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(FilterActivity.this  , PreviousOrderActivity.class));
        }
    };



    View.OnClickListener selectFromFavouratelistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(FilterActivity.this, FavourateListActivity.class));
        }
    };



    View.OnClickListener selectManufactureListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i2 = new Intent(FilterActivity.this, CompanyNamesActivity.class);
            startActivity(i2);
            overridePendingTransition(R.anim.slide_in_up, R.anim.no_sliding);
          /*  String [] companyarr = {"Apollo" ,"bridgestone" ,"CEAT" , "Continental" , "Cooper" , "Falken" , "GoodYear" , "Hankook" , "Ironman" , "JK Tyre" , "kenda" , "kumho" ,"Leao" , "Maxxis" , "Metro Continental" , "Michelin" , "MRF" , "Nexan" ,"Pirelli" , "Yokohama"};
            new MaterialDialog.Builder(FilterActivity.this)
                    .title("Select Manufacturer")
                    .items(companyarr)
                    .negativeText("Cancel")
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            selectmanufacture_btn.setText(""+text);
                        }
                    })
                    .show();   */

        }
    };




    View.OnClickListener selectvehicletypeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final MaterialDialog dialog = new MaterialDialog.Builder(FilterActivity.this)
                    .title("vehicle Type")
                    .customView(R.layout.tyre_type_layout, true)

                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(final MaterialDialog dialog) {
                            dialog.findViewById(R.id.car_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    selectvehicletype_btn.setText("Car");
                                    dialog.dismiss();
                                }
                            });

                            dialog.findViewById(R.id.two_wheeler_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    selectmanufacture_btn.setText("Two Wheeler");
                                    dialog.dismiss();
                                }
                            });

                            dialog.findViewById(R.id.hcv_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    selectvehicletype_btn.setText("HCV Type");
                                    dialog.dismiss();
                                }
                            });

                            dialog.findViewById(R.id.lcv_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    selectvehicletype_btn.setText("LCV Type");
                                    dialog.dismiss();
                                }
                            });

                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                        }
                    }).build();
            dialog.show();




            dialog.findViewById(R.id.car_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectvehicletype_btn.setText("Car");
                    dialog.dismiss();
                }
            });

            dialog.findViewById(R.id.two_wheeler_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectmanufacture_btn.setText("Two Wheeler");
                    dialog.dismiss();
                }
            });

            dialog.findViewById(R.id.hcv_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectvehicletype_btn.setText("HCV Type");
                    dialog.dismiss();
                }
            });

            dialog.findViewById(R.id.lcv_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectvehicletype_btn.setText("LCV Type");
                    dialog.dismiss();
                }
            });

        }
    };




    View.OnClickListener tyretypeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String [] companyarr = {"Tube" ,"Tubeless" };

            new MaterialDialog.Builder(FilterActivity.this)
                    .title("Select Vehicle type")
                    .items(companyarr)
                    .negativeText("Cancel")
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            tyre_type_btn.setText("" + text);
                        }
                    })
                    .show();
        }
    };




    View.OnClickListener rimSizeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                String [] companyarr = {"R15" ,"R16" ,"R17" ,"R18" ,"R19" ,"R20"};
            new MaterialDialog.Builder(FilterActivity.this)
                    .title("Select Rim Size")
                    .items(companyarr)
                    .negativeText("Cancel")
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            rim_size_btn_txt.setText("" + text);
                        }
                    })
                    .show();
        }
    };


    View.OnClickListener tyreSizelistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

           /* if(rim_size_btn_txt.getText().toString().equals("Rim Size")){
                Toaster.generatemessage(FilterActivity.this , "please select rim size first");
            }else {
                MaterialDialog dialog = new MaterialDialog.Builder(FilterActivity.this)
                        .title("Tyre Specification")
                        .customView(R.layout.custom_dialog_layout_for_tyre_spec_filter, true)
                        .positiveText("Select")
                        .negativeText(android.R.string.cancel)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                EditText specs = (EditText) dialog.getCustomView().findViewById(R.id.edittext_rim_size_in_filter_layout);
                                tyre_size_btn.setText(""+specs.getText());
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                            }
                        }).build();
                dialog.show();  */

            String [] companyarr = {"750.20" ,"825.20" ,"900-20" ,"1100.20" ,"750-16" ,"825.16" ,"750.20" ,"825.20" ,"900-20" ,"1100.20" ,"750-16" ,"825.16"};
            new MaterialDialog.Builder(FilterActivity.this)
                    .title("Size according to rim size")
                    .items(companyarr)
                    .negativeText("Cancel")
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            tyre_size_btn.setText("" + text);
                        }
                    })
                    .show();

            }




        };




    View.OnClickListener searchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent in = new Intent(FilterActivity.this , PlaceOrderListActivity.class );
                  in.putExtra(MANUFACTURE_KEY ,""+selectmanufacture_btn.getText().toString() );
                  in.putExtra(VEHICLE_TYPE_KEY , ""+selectvehicletype_btn.getText().toString());
                  in.putExtra(TYRE_SIZE_KEY , ""+tyre_size_btn.getText().toString());
                  in.putExtra(TYRE_TYPE_KEY, "" + tyre_type_btn.getText().toString());
                   startActivity(in);
        }
    };






    private void setupWindowAnimations() {
        Explode explode = new Explode();
        explode.setDuration(2000);
        getWindow().setExitTransition(explode);

        Fade fade = new Fade();
        getWindow().setReenterTransition(fade);
    }










}
