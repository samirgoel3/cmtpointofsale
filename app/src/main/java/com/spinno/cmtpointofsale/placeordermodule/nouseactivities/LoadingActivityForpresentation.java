package com.spinno.cmtpointofsale.placeordermodule.nouseactivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.tyre.TyreRimsActivity;

public class LoadingActivityForpresentation extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_activity_forpresentation);

        findViewById(R.id.continue_button_in_loading_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoadingActivityForpresentation.this , TyreRimsActivity.class));
            }
        });
    }


}
