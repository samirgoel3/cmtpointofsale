package com.spinno.cmtpointofsale.placeordermodule.previousorders;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.spinno.cmtpointofsale.R;

import adapters.AdapterPreviousOrderList;
import butterknife.Bind;
import butterknife.ButterKnife;

public class PreviousOrderActivity extends Activity {
    @Bind(R.id.back_layout_in_previous_order_activity)LinearLayout back_btn;
    @Bind(R.id.list_in_previous_order_Activiy)ListView list_previous_orders;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_order);
        ButterKnife.bind(this);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        list_previous_orders.setAdapter(new AdapterPreviousOrderList(PreviousOrderActivity.this));

    }

}
