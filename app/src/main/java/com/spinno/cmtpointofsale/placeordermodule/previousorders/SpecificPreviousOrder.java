package com.spinno.cmtpointofsale.placeordermodule.previousorders;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import adapters.AdapterSpecificOrderList;
import butterknife.Bind;
import butterknife.ButterKnife;

public class SpecificPreviousOrder extends Activity {

    @Bind(R.id.list_in_specific_order_activity)ListView list_specific_order;
    @Bind(R.id.back_layout_in_specififc_order_activity)LinearLayout back_btn;



    ArrayList<String> chekbox = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_previous_order);
        ButterKnife.bind(this);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        for(int i = 0 ; i<16 ; i++){
            chekbox.add("0");
        }

        list_specific_order.setAdapter(new AdapterSpecificOrderList(SpecificPreviousOrder.this , chekbox));
    }
}
