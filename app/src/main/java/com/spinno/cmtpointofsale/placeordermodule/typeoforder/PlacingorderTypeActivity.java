package com.spinno.cmtpointofsale.placeordermodule.typeoforder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.tyre.FilterActivityNew;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;

public class PlacingorderTypeActivity extends Activity {

    @Bind(R.id.back_layout_in_placing_order_type_activity)LinearLayout back_btn;
    @Bind(R.id.tyre_layout_in_placing_order_type_activity)FrameLayout tyre_btn ;
    @Bind(R.id.tube_layout_in_placing_order_type_activitye)FrameLayout tube_btn ;
    @Bind(R.id.alloy_in_placing_order_type_activity)FrameLayout alloy_btn ;
    @Bind(R.id.miscleneousin_placing_order_type_activity)FrameLayout misceleneous_btn ;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placingorder_type);
        ButterKnife.bind(this);

             back_btn.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     finish();
                 }
             });




        tyre_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 startActivity(new Intent(PlacingorderTypeActivity.this , FilterActivityNew.class));
            }
        });
        tube_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toaster.generatemessage(PlacingorderTypeActivity.this , "Under Development");
            }
        });


        alloy_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toaster.generatemessage(PlacingorderTypeActivity.this , "Under Development");
            }
        });

        misceleneous_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toaster.generatemessage(PlacingorderTypeActivity.this , "Under Development");
            }
        });




    }

}
