package com.spinno.cmtpointofsale.placeordermodule.tyre;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import adapters.Adaprtergrid;
import butterknife.Bind;
import butterknife.ButterKnife;
import contants.PlaceOrderModuleConstants;

public class CompanyNamesActivityNew extends Activity {

    @Bind(R.id.gridview_in_company_names_activity)GridView grid_view;
    @Bind(R.id.back_laupit_in_company_name_activity_new)LinearLayout back_btn;
    @Bind(R.id.vehicle_type_image_in_companyname_activity)ImageView vehicletype;

    String [] companyarr = {"Apollo" ,"Bridgestone" ,"Ceat" , "Continental" , "Cooper" , "Falken" , "GoodYear" , "Hankook" , "Ironman" , "JK Tyre" , "Kenda" , "Kumho" ,"Leao" , "Maxxis" , "Metro" , "Michelin" , "MRF" , "Nexan" ,"Pirelli" , "Yokohama"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_names_activity_new);
        ButterKnife.bind(this);

        setviewsaccordingtopreviouselection();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

           grid_view.setAdapter(new Adaprtergrid(CompanyNamesActivityNew.this ,companyarr));



    }

    private void setviewsaccordingtopreviouselection() {
        if(PlaceOrderModuleConstants.vehicletype.equals("car_type")){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (PlaceOrderModuleConstants.vehicletype.equals("two_wheeler_type")){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("HCV_type")){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("LCV_type")){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
    }
}
