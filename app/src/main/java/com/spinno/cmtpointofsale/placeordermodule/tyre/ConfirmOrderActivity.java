package com.spinno.cmtpointofsale.placeordermodule.tyre;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.CartTable;
import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;
import java.util.List;

import adapters.AdapterConfirmActivity;
import butterknife.Bind;
import butterknife.ButterKnife;
import databases.manager.CartManager;

public class ConfirmOrderActivity extends Activity {

    @Bind(R.id.list_view_in_confirm_order_activity)ListView list_in_confirm_order;
    @Bind(R.id.check_out_in_confirm_order_activity)LinearLayout check_out_confirm_order;
    @Bind(R.id.back_button_in_confirm_order_activty)LinearLayout back_btn;
    @Bind(R.id.total_no_of_tyres_in_confirm_order_Activity)TextView total_no_tyres_txt;


   CartManager dbcartmanager ;
    List<CartTable> carttable  ;


    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        ButterKnife.bind(this);
        dbcartmanager = new CartManager(ConfirmOrderActivity.this);
        carttable = dbcartmanager.getalldata();


        total_no_tyres_txt.setText(""+dbcartmanager.countTotalNumbersOfTyre());

        check_out_confirm_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ConfirmOrderActivity.this, VendersAddressActivity.class));
            }
        });




        loadListFromDatabse();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });





    }

    private void loadListFromDatabse() {

            for(int i = 0 ; i<carttable.size() ; i++){
                tyre_id_arr.add(carttable.get(i).getTyreId());
                tyre_name_arr.add(carttable.get(i).getTyreName());
                tyre_specs_arr.add(carttable.get(i).getTyreSpecs());
                tyre_price_arr.add(carttable.get(i).getTyrePrice());
                tyre_number_of_unit_arr.add(carttable.get(i).getTyreNoOfunits());

            }
            list_in_confirm_order.setAdapter(new AdapterConfirmActivity(ConfirmOrderActivity.this ,tyre_id_arr ,tyre_name_arr ,tyre_specs_arr ,tyre_price_arr ,tyre_number_of_unit_arr));


    }
}
