package com.spinno.cmtpointofsale.placeordermodule.tyre;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;

public class FilterActivityNew extends Activity {


    @Bind(R.id.back_layout_in_filter_activity)LinearLayout back_btn;
    @Bind(R.id.action_bar_image)ImageView actionbarImage ;


    public static  final  String classname = "FilterActivityNew";
    Intent intent ;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_activity_new);
        ButterKnife.bind(this);
        ConstantClass.whichActivity = classname ;


        intent  = new Intent(FilterActivityNew.this , CompanyNamesActivityNew.class);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        findViewById(R.id.car_type_layout_in_place_order_moldule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaceOrderModuleConstants.vehicletype = "car_type";
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });

        findViewById(R.id.scooter_type_layout_in_place_order_moldule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaceOrderModuleConstants.vehicletype = "two_wheeler_type";
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });

        findViewById(R.id.hcv_type_layout_in_place_order_moldule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaceOrderModuleConstants.vehicletype = "HCV_type";
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });

        findViewById(R.id.lcv_type_layout_in_place_order_moldule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaceOrderModuleConstants.vehicletype = "LCV_type";
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });




      /*  neworder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog dialog = new MaterialDialog.Builder(FilterActivityNew.this)
                        .title("vehicle Type")
                        .customView(R.layout.tyre_type_layout, true)

                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(final MaterialDialog dialog) {
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                            }
                        }).build();
                dialog.show();


                dialog.findViewById(R.id.car_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PlaceOrderModuleConstants.vehicletype = "car_type";
                        dialog.dismiss();
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    }
                });

                dialog.findViewById(R.id.two_wheeler_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PlaceOrderModuleConstants.vehicletype = "two_wheeler_type";
                        dialog.dismiss();
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    }
                });

                dialog.findViewById(R.id.hcv_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PlaceOrderModuleConstants.vehicletype = "HCV_type";
                        dialog.dismiss();
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    }
                });

                dialog.findViewById(R.id.lcv_type_layout_in_tyre_type_activity).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PlaceOrderModuleConstants.vehicletype = "LCV_type";
                        dialog.dismiss();
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    }
                });
            }
        });    */
    }




}
