package com.spinno.cmtpointofsale.placeordermodule.tyre;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.spinno.cmtpointofsale.CartTable;
import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;
import java.util.List;

import adapters.AdapterPlaceOrderActivity;
import butterknife.Bind;
import butterknife.ButterKnife;
import databases.manager.CartManager;

public class PlaceOrderActivity extends Activity {
    @Bind(R.id.back_button_in_place_order_activty)LinearLayout back_btn;
    @Bind(R.id.list_view_in_place_order_Activity)ListView list_order_details;

    CartManager dbCartManager ;
    List<CartTable> carttable  ;

    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);
        ButterKnife.bind(this);
        dbCartManager = new CartManager(PlaceOrderActivity.this);
        carttable = dbCartManager.getalldata();


        list_order_details.setFocusable(false);
        loadListFromDatabse();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        findViewById(R.id.place_order_button_place_order_actiovity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/plain");
                startActivity(emailIntent);
            }
        });

    }



    private void loadListFromDatabse() {

        for(int i = 0 ; i<carttable.size() ; i++){
            tyre_id_arr.add(carttable.get(i).getTyreId());
            tyre_name_arr.add(carttable.get(i).getTyreName());
            tyre_specs_arr.add(carttable.get(i).getTyreSpecs());
            tyre_price_arr.add(carttable.get(i).getTyrePrice());
            tyre_number_of_unit_arr.add(carttable.get(i).getTyreNoOfunits());
        }
        list_order_details.setAdapter(new AdapterPlaceOrderActivity(PlaceOrderActivity.this,tyre_id_arr ,tyre_name_arr ,tyre_specs_arr ,tyre_price_arr ,tyre_number_of_unit_arr));
        setListViewHeightBasedOnChildren(list_order_details);
    }

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }



}
