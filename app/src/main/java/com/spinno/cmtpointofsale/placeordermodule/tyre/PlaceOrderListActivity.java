package com.spinno.cmtpointofsale.placeordermodule.tyre;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.CartActivity;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import java.util.ArrayList;

import adapters.AdapterPlaceOrderlist;
import adapters.AdapterRimSizegrid;
import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;
import databases.manager.CartManager;
import databases.manager.FavourateManager;
import loader.LoaderView;

public class PlaceOrderListActivity extends Activity  {


    private MenuDrawer mDrawer;

    @Bind(R.id.filter_button_in_place_order_list)LinearLayout filterbtn;
    @Bind(R.id.filter_opner_layout_in_place_order_activity)LinearLayout filteropenlayoutbtn;

    @Bind(R.id.listview_in_place_order_list)ListView list;
    @Bind(R.id.loaderspinningwheel)LoaderView loaderwheel;
    public static View carrlayout;
    public static TextView totalo_no_of_tyre_in_cart;




    @Bind(R.id.company_name_on_action_bar_of_place_order)TextView company_name_on_actiopbar;
    @Bind(R.id.rim_size_on_action_bar_of_place_order)TextView rim_size_on_action_bar;
    @Bind(R.id.tyre_size_on_action_bar_of_place_order)TextView tyre_size_on_action_bar;
    @Bind(R.id.vehicle_image_on_action_bar_of_place_order)ImageView vehicle_type_img_on_action_bar;



    @Bind(R.id.grid_view_in_tyre_rim_size_drawer_layout)GridView girdview_in_filter_drawer;





    public static CartManager dbCartManager ;
    public static FavourateManager dbFavourateManager;
    public static  final  String classname = "PlaceOrderListActivity";



    ArrayList<String> tyre_id_arr = new ArrayList<String>();
    ArrayList<String> tyre_name_arr = new ArrayList<String>();
    ArrayList<String> tyre_specs_arr = new ArrayList<String>();
    ArrayList<String> tyre_price_arr = new ArrayList<String>();
    ArrayList<String> tyre_number_of_unit_arr = new ArrayList<String>();
    ArrayList<String> favourate_arr = new ArrayList<String>();


    String [] rimsizes = {"R12" ,"R13" ,"R14" , "R15","R16" ,"R17" ,"R18" ,"R19" };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawer = MenuDrawer.attach(this , Position.RIGHT);
        mDrawer.setContentView(R.layout.activity_place_order_list);
        mDrawer.setMenuView(R.layout.rim_tyre_size_drawer_layout);
        mDrawer.setDropShadowEnabled(false);


        ButterKnife.bind(this);

        carrlayout = findViewById(R.id.cart_layout_in_place_order_list);
        totalo_no_of_tyre_in_cart = (TextView) findViewById(R.id.total_no_of_tyres_in_cart_in_place_order_list_activity);
        Toaster.generatemessage(PlaceOrderListActivity.this  ,""+dbCartManager.countTotalNumbersOfTyre());


        ConstantClass.whichActivity =  classname;



        dbCartManager = new CartManager(PlaceOrderListActivity.this);
        dbFavourateManager = new FavourateManager(PlaceOrderListActivity.this);
        dbCartManager.clearCartTable();

        setviewsaccordingtopreviouselection();

        filterbtn.setOnClickListener(filterbuttonlistener);
        filteropenlayoutbtn.setOnClickListener(filterbuttonlistener);


        girdview_in_filter_drawer.setAdapter(new AdapterRimSizegrid(PlaceOrderListActivity.this  ,rimsizes));


         if(PlaceOrderModuleConstants.tyresize.equals("145/80 R12")){
             loadlistrtwelve();
         }if(PlaceOrderModuleConstants.tyresize.equals("155/70 R13")){
            loadlistrthrteen();
        }


        carrlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PlaceOrderListActivity.this , CartActivity.class));
            }
        });

    }



    private void setviewsaccordingtopreviouselection() {
        company_name_on_actiopbar.setText(PlaceOrderModuleConstants.companytype);
        rim_size_on_action_bar.setText(PlaceOrderModuleConstants.rimsize);
        tyre_size_on_action_bar.setText(PlaceOrderModuleConstants.tyresize);
        if(PlaceOrderModuleConstants.vehicletype.equals("car_type")){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.car_white_icon);
        }else  if (PlaceOrderModuleConstants.vehicletype.equals("two_wheeler_type")){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("HCV_type")){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("LCV_type")){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.truck_icon_white);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setcartfromDB();
    }

    private void loadlistrtwelve() {


        for (int i = 1 ; i<6 ; i++){
            tyre_id_arr.add(""+i);
            tyre_number_of_unit_arr.add("0");
        }

        tyre_name_arr.add("Apollo AMAZER XL TT");
        tyre_name_arr.add("ApolloAMAZER 3G TL");
        tyre_name_arr.add("Apolo AMAZER 4G TL");
        tyre_name_arr.add("Apollo AMAZER XL TL");
        tyre_name_arr.add("Apolo AMAZER 4G TT");


        tyre_specs_arr.add("Size: 145/80 R12 74T");
        tyre_specs_arr.add("Size: 145/80 R12 74T");
        tyre_specs_arr.add("Size: 145/80 R12 65T");
        tyre_specs_arr.add("Size: 145/80 R12 69T");
        tyre_specs_arr.add("Size: 145/80 R12 74T");



        tyre_price_arr.add("2051.45");
        tyre_price_arr.add("1890.88");
        tyre_price_arr.add("2101");
        tyre_price_arr.add("1951");
        tyre_price_arr.add("2300");



        loaderwheel.setVisibility(View.GONE);
        list.setAdapter(new AdapterPlaceOrderlist(PlaceOrderListActivity.this , tyre_id_arr , tyre_name_arr , tyre_specs_arr , tyre_price_arr , tyre_number_of_unit_arr));
    }

    private void loadlistrthrteen() {


        for (int i = 7 ; i<11 ; i++){
            tyre_id_arr.add(""+i);
            tyre_number_of_unit_arr.add("0");
        }

        tyre_name_arr.add("Apollo ACELERE TL");
        tyre_name_arr.add("Apollo AMAZER 3G TT");
        tyre_name_arr.add("Apollo AMAZER 4G TL");
        tyre_name_arr.add("Apollo AMAZER XL TT");


        tyre_specs_arr.add("Size: 155/70 R13 75T");
        tyre_specs_arr.add("Size: 155/70 R13 75T");
        tyre_specs_arr.add("Size: 155/70 R13 75H");
        tyre_specs_arr.add("Size: 155/70 R13 75H");



        tyre_price_arr.add("2388.90");
        tyre_price_arr.add("2289");
        tyre_price_arr.add("2551");
        tyre_price_arr.add("2508");



        loaderwheel.setVisibility(View.GONE);
        list.setAdapter(new AdapterPlaceOrderlist(PlaceOrderListActivity.this , tyre_id_arr , tyre_name_arr , tyre_specs_arr , tyre_price_arr , tyre_number_of_unit_arr));
    }


    View.OnClickListener filterbuttonlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawer.toggleMenu();
        }
    };



    public static void setcartfromDB(){
        if(dbCartManager.countTotalNumbersOfTyre()>0){
            carrlayout.setVisibility(View.VISIBLE);
            totalo_no_of_tyre_in_cart.setText(""+dbCartManager.countTotalNumbersOfTyre());
        }else {
            carrlayout.setVisibility(View.GONE);
        }
    }





}
