package com.spinno.cmtpointofsale.placeordermodule.tyre;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import adapters.AdapterRimSizegrid;
import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;

public class TyreRimsActivity extends Activity {

    @Bind(R.id.gridview_in_rim_size_activity)GridView  gdview  ;
    @Bind(R.id.vehicle_type_image_in_tyre_rim_activity)ImageView vehicletype;
    @Bind(R.id.company_name_tyre_rim_activity)TextView companyname_txt;

    String [] rimsizes = {"R12" ,"R13" ,"R14" , "R15","R16" ,"R17" ,"R18" ,"R19" };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_rims);
        ButterKnife.bind(this);
        ConstantClass.whichActivity = "TyreRimsActivity";
        setviewsaccordingtopreviouselection();

        findViewById(R.id.back_laupit_in_rim_size_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        gdview.setAdapter(new AdapterRimSizegrid(TyreRimsActivity.this ,rimsizes));

    }




    private void setviewsaccordingtopreviouselection() {
        if(PlaceOrderModuleConstants.vehicletype.equals("car_type")){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (PlaceOrderModuleConstants.vehicletype.equals("two_wheeler_type")){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("HCV_type")){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("LCV_type")){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }


        companyname_txt.setText(PlaceOrderModuleConstants.companytype);
    }
}
