package com.spinno.cmtpointofsale.placeordermodule.tyre;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import adapters.AdaptertyreSizeGrid;
import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;

public class TyreSizeActivity extends Activity {

    @Bind(R.id.gridview_in_tyre_size_activity)GridView gdview;
    @Bind(R.id.back_laupit_in_tyre_size_activity)LinearLayout back_btn;
    @Bind(R.id.vehicle_type_image_in_tyre_size_activity)ImageView vehicletype;
    @Bind(R.id.company_name_in_tyre_size_activity)TextView companyname;
    @Bind(R.id.rim_size_in_tyre_size_activity)TextView rimsize_txt;


    String []R12 = {"145/80 R12" ,"145/70 R12" ,"135/70 R12" , "155/65 R12" } ;
    String []R13 = {"145/70 R13" ,"145/80 R13" ,"155/65 R13" ,"155/70 R13" ,"155/80 R13" ,"165/65 R13" ,"165/70 R13" ,"165/80 R13" ,"175/60 R13" ,"175/70 R13" ,"185/60 R13" ,"185/65 R13" ,"185/70 R13" ,"205/60 R13" } ;
    String []R14 = {"155/65 R14" ,"155/70 R14" ,"165/65 R14" ,"165/80 R14" ,"175/64 R14" ,"175/70 R14" ,"185/60 R14" ,"185/65 R14" ,"185/70 R14" ,"195/60 R14" ,"195/70 R14" } ;
    String []R15 = {"165/80 R15" ,"185/60 R15" ,"185/65 R15" ,"195/55 R15" ,"195/60 R15" ,"195/65 R15" ,"195/70 R15" ,"195/80 R15","205/55 R15" ,"205/60 R15" ,"205/65 R15" ,"215/75 R15" ,"225/60 R15" } ;
    String []R16 = {"195/55 R16" ,"205/55 R16" ,"215/55 R16"} ;
    String []R17 = {"205/40 R17" ,"215/40 R17" ,"215 45 R17" ,"225/45 R17" ,"225/50 R17" ,"235/40 R17"} ;
    String []R18 = {"215/35 R18" ,"235/40 R18" ,"245/40 R18"} ;
    String []R19 = {"235/35 R19"  } ;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_size);
        ButterKnife.bind(this);
        ConstantClass.whichActivity = "TyreSizeActivity";



        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if(PlaceOrderModuleConstants.rimsize.equals("R12")){
            gdview.setAdapter(new AdaptertyreSizeGrid(TyreSizeActivity.this ,R12 ));
        }if(PlaceOrderModuleConstants.rimsize.equals("R13")){
            gdview.setAdapter(new AdaptertyreSizeGrid(TyreSizeActivity.this ,R13 ));
        }if(PlaceOrderModuleConstants.rimsize.equals("R14")){
            gdview.setAdapter(new AdaptertyreSizeGrid(TyreSizeActivity.this ,R14 ));
        }if(PlaceOrderModuleConstants.rimsize.equals("R15")){
            gdview.setAdapter(new AdaptertyreSizeGrid(TyreSizeActivity.this ,R15 ));
        }if(PlaceOrderModuleConstants.rimsize.equals("R16")){
            gdview.setAdapter(new AdaptertyreSizeGrid(TyreSizeActivity.this ,R16 ));
        }if(PlaceOrderModuleConstants.rimsize.equals("R17")){
            gdview.setAdapter(new AdaptertyreSizeGrid(TyreSizeActivity.this ,R17 ));
        }if(PlaceOrderModuleConstants.rimsize.equals("R18")){
            gdview.setAdapter(new AdaptertyreSizeGrid(TyreSizeActivity.this ,R18 ));
        }if(PlaceOrderModuleConstants.rimsize.equals("R19")){
            gdview.setAdapter(new AdaptertyreSizeGrid(TyreSizeActivity.this ,R19 ));
        }


        setviewsaccordingtopreviouselection();

    }




    private void setviewsaccordingtopreviouselection() {
        companyname.setText(PlaceOrderModuleConstants.companytype);
        rimsize_txt.setText(PlaceOrderModuleConstants.rimsize);
        if(PlaceOrderModuleConstants.vehicletype.equals("car_type")){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (PlaceOrderModuleConstants.vehicletype.equals("two_wheeler_type")){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("HCV_type")){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("LCV_type")){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
    }

}
