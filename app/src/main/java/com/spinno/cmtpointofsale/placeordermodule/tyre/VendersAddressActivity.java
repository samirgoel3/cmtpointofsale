package com.spinno.cmtpointofsale.placeordermodule.tyre;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VendersAddressActivity extends Activity {
    @Bind(R.id.back_button_in_select_vendor_activty)LinearLayout back_btn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venders_address);
        ButterKnife.bind(this);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.appolo_address).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VendersAddressActivity.this , PlaceOrderActivity.class));
            }
        });
    }


}
