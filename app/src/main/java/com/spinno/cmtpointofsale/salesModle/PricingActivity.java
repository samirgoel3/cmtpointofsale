package com.spinno.cmtpointofsale.salesModle;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.spinno.cmtpointofsale.InvoiceBucketTable;
import com.spinno.cmtpointofsale.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import databases.manager.InvoiceBucketManager;

public class PricingActivity extends Activity {

    @Bind(R.id.back_button_in_set_pricing_activity)LinearLayout back_btn ;
    @Bind(R.id.linealayout_container_in_pricing_activity)LinearLayout container ;

    @Bind(R.id.freight_in_pricing_activity)LinearLayout freight_btn ;
    @Bind(R.id.packing_in_pricing_Activity)LinearLayout packing_btn ;
    @Bind(R.id.miscleneous_in_pricing_Activity)LinearLayout misceleneous_btn ;

    @Bind(R.id.freight_text_in_pricing_activity)TextView freight_txt ;
    @Bind(R.id.packing_text_in_pricing_activity)TextView packing_txt ;
    @Bind(R.id.misceleneous_text_in_pricing_activity)TextView misceleneous_txt ;

////////////Databse Managers
          InvoiceBucketManager ibmanager ;
          List <InvoiceBucketTable>  bucketTable ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pricing);
        ButterKnife.bind(this);
        ibmanager = new InvoiceBucketManager(PricingActivity.this);
        bucketTable = ibmanager.getalldata() ;

        loadDataFromInvoiceBuckeTable();


        freight_btn.setOnClickListener(FreightButtonListener);
        packing_btn.setOnClickListener(PackingButtonListener);
        misceleneous_btn.setOnClickListener(MisceleneousButtonListener);



        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.no_sliding, R.anim.push_out_right);
            }
        });
    }

    private void loadDataFromInvoiceBuckeTable() {
        for(int i = 0 ; i< ibmanager.getTotalTYpeOftyreInBucket() ; i++){
          container.addView(viewFromInvoiceDatabase(bucketTable.get(i).getTyreName(), bucketTable.get(i).getTyreSpecs(), bucketTable.get(i).getTyreNoOfunits()));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_sliding, R.anim.push_out_right);
    }


    private View viewFromInvoiceDatabase(String tyrename , String tyrespecs , String tyrenoOfUnit){
        LayoutInflater li = (LayoutInflater)PricingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addview  = li.inflate(R.layout.item_for_tyres_for_container_one , null);
        TextView tyrename_txt  = (TextView) addview.findViewById(R.id.tyre_name_in_container_of_pricing_Activity);
        TextView tyrespecs_txt  = (TextView) addview.findViewById(R.id.tyre_specs_in_container_of_pricing_Activity);
        TextView tyre_noOfUnit  = (TextView) addview.findViewById(R.id.tyre_noOfUnit_in_container_of_pricing_Activity);

           tyrename_txt.setText(""+tyrename);
           tyrespecs_txt.setText(""+tyrespecs);
           tyre_noOfUnit.setText(""+tyrenoOfUnit);


        addview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toaster.generatemessage(PricingActivity.this , "This will enter a dialog window that is under development");
            }
        });

        return addview;

    }




    View.OnClickListener FreightButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new MaterialDialog.Builder(PricingActivity.this)
                    .titleColorRes(R.color.client_action_bar_colour)
                    .positiveColorRes(R.color.client_action_bar_colour)
                    .title("Enter Amount")
                    .customView(R.layout.dialog_edit_text, true)
                    .positiveText("Ok")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            EditText edt = (EditText) dialog.findViewById(R.id.edit_text_in_dialog);
                            freight_txt.setText("Rs."+edt.getText().toString()+"/-");
                        }
                    })
                    .show();

        }
    };



    View.OnClickListener PackingButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new MaterialDialog.Builder(PricingActivity.this)
                    .titleColorRes(R.color.client_action_bar_colour)
                    .positiveColorRes(R.color.client_action_bar_colour)
                    .title("Enter Amount")
                    .customView(R.layout.dialog_edit_text, true)
                    .positiveText("Ok")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            EditText edt = (EditText) dialog.findViewById(R.id.edit_text_in_dialog);
                            packing_txt.setText("Rs." + edt.getText().toString()+"/-");
                        }
                    })
                    .show();

        }
    };


    View.OnClickListener MisceleneousButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new MaterialDialog.Builder(PricingActivity.this)
                    .titleColorRes(R.color.client_action_bar_colour)
                    .positiveColorRes(R.color.client_action_bar_colour)
                    .title("Enter Amount")
                    .customView(R.layout.dialog_edit_text, true)
                    .positiveText("Ok")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            EditText edt = (EditText) dialog.findViewById(R.id.edit_text_in_dialog);
                            misceleneous_txt.setText("Rs."+edt.getText().toString()+"/-");
                        }
                    })
                    .show();

        }
    };






}
