package com.spinno.cmtpointofsale.salesModle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.orm.SugarRecord;
import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.StockTable;
import com.spinno.cmtpointofsale.salesModle.addtube.VehicletypeActivityForTube;
import com.spinno.cmtpointofsale.salesModle.addtyres.VehicleTypeActivityNewFromSale;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import databases.manager.StockInManager;

public class SaleActivityFirst extends Activity {

    @Bind(R.id.tyres_in_sales_activity_first)LinearLayout tyres_btn ;
    @Bind(R.id.tubes_in_sales_activity_first)LinearLayout tubes_btn ;
    @Bind(R.id.alloys_in_sales_activity_first)LinearLayout alloys_btn ;
    @Bind(R.id.others_in_sales_activity_first)LinearLayout others_btn ;
    @Bind(R.id.back_button_in_sale_activity_first)LinearLayout  back_button ;


        StockInManager stinmanager ;




    ///////// arrary lisy for databse ///////
    String [] tyreidarr ={"155","542","34545","4544","535435","634","3357","854","9453","1340","151","13452","1334","13454","1345435","34534516","34417","44318","565519","33320","554422","3243","433424"};
    String [] vehicletypearr = {"Car","Car","Car","Car","Car","Car","Car","Car","Car","Car","Car","Car","Car","Car","Car","Car","Two Wheeler","Two Wheeler","Two Wheeler","Two Wheeler","Two Wheeler","Car","Car","Car"};
    String [] brandnamearr = {"Apollo","Apollo","Apollo","Apollo","Apollo","Apollo","Falken","Falken","Falken","Falken","Falken","Falken","Falken","Falken","Falken","Falken","Ceat","Ceat","Ceat","Ceat","Ceat","Ceat","Ceat","Ceat",};
    String [] rimsizearr = {"12" ,"12" ,"13","13","13","13","13","15","15","15","15","15","15","15","15","15","17","17","17","17","17","14","14","14"};
    String [] tyrenamearr= {"AMAZER XL TT"
            ,"Apollo AMAZER 3G TL"
            ,"Apollo AMAZER XL TT"
            ,"Apollo ACELERE TL"
            ,"Apollo AMAZER XL TT"
            ,"Apollo AMAZER XL TT"
            ,"Apollo ACELERE TL"
            ,"Falken AZENIS PT722"
            ,"Falken SINCERA SN835"
            ,"Falken SINCERA SN835"
            ,"Falken AZENIS PT722"
            ,"Falken AZENIS PT722"
            ,"Falken SINCERA SN835"
            ,"Falken SINCERA SN835"
            ,"Falken SINCERA SN835"
            ,"Falken AZENIS PT722"
            ,"CEAT SEC  ZOOM F"
            , "CEAT SECURA F 85"
            , "CEAT ZOOM F"
            , "CEAT SECURA F85+"
            , "CEAT SEC  M72"
            ,"CEAT MILAZE TL"
            , "CEAT MILAZE TL"
            , "CEAT MILAZE TL"};


    String [] tyrespecsarr = {"145/80 R12"
            ,"145/80 R12"
            ,"165/65 R13"
            ,"155/70 R13"
            ,"145/70 R13"
            ,"155/70 R13"
            ,"175/70 R13"
            ,"195/55 R15"
            ,"185/60 R15"
            ,"195/60 R15"
            ,"205/60 R15"
            ,"225/60 R15"
            ,"175/65 R15"
            ,"185/65 R15"
            ,"195/65 R15"
            ,"205/65 R15"
            ,"275 - 17"
            ,"275 - 17"
            ,"2.75 - 17"
            ,"2.75 - 17"
            ,"275 - 17"
            ,"165/80  R 14"
            ,"175/65  R 14"
            ,"185/70  R 14"};



    String [] tyretypearr = {"TT"
            ,"TL"
            ,"TT"
            ,"TL"
            ,"TT"
            ,"TT"
            ,"TL"
            ,"TL"
            ,"TL"
            ,"TL"
            ,"TL"
            ,"TL"
            ,"TL"
            ,"TL"
            ,"TL"
            ,"TL"
            ,"TT"
            ,"TT"
            ,"TT"
            ,"TT"
            ,"TT"
            ,"TL"
            ,"TL"
            ,"TL"};


    String [] isfavouritearr = {"1"
            ,"0"
            ,"1"
            ,"1"
            ,"1"
            ,"0"
            ,"1"
            ,"1"
            ,"1"
            ,"1"
            ,"0"
            ,"0"
            ,"0"
            ,"0"
            ,"0"
            ,"0"
            ,"1"
            ,"0"
            ,"0"
            ,"1"
            ,"0"
            ,"1"
            ,"1"
            ,"1"};


    String [] instockarr = {"12"
            ,"6"
            ,"11"
            ,"12"
            ,"6"
            ,"3"
            ,"15"
            ,"12"
            ,"9"
            ,"5"
            ,"12"
            ,"30"
            ,"21"
            ,"45"
            ,"22"
            ,"17"
            ,"10"
            ,"10"
            ,"13"
            ,"16"
            ,"18"
            ,"15"
            ,"11"
            ,"14"};

    String [] offers = {"0"
            ,"0"
            ,"0"
            ,"1"
            ,"0"
            ,"1"
            ,"0"
            ,"1"
            ,"1"
            ,"0"
            ,"0"
            ,"0"
            ,"0"
            ,"0"
            ,"0"
            ,"0"
            ,"1"
            ,"0"
            ,"0"
            ,"0"
            ,"1"
            ,"0"
            ,"0"
            ,"0"};


    String [] offerid = {"N/A"
            ,"N/A"
            ," N/A"
            ,"1"
            ,"N/A"
            ,"2"
            ,"N/A"
            ,"3"
            ,"4"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"5"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"};

    String [] offercompanyarr = {"N/A"
            ,"N/A"
            ,"N/A"
            ,"Titan"
            ,"N/A"
            ,"Apollo"
            ,"N/A"
            ,"Falken"
            ,"Falken"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"Ceat"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"};

    String [] offername = {"N/A"
            ,"N/A"
            ,"N/A"
            ,"Wristy"
            ,"N/A"
            ,"Monsoon Sale"
            ,"N/A"
            ,"Buy 3 Get 1 Free"
            ,"Sunny Offer"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"Change both tyres and save"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"};

    String [] offerdescriptionarr = {"N/A"
            ,"N/A"
            ,"N/A"
            ,"Get Titan Watch free on buying 4 Apollo Acelere tyres"
            ,"N/A"
            ,"Get 15 % discount on Tyre MRP"
            ,"N/A"
            ,"Buy 3 tyres and get one tyre FOC"
            ,"Get Window shades free with every Tyre"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"On buying second tyre with tyre get 10 % off"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"};

    String [] offerstartarr = {"N/A"
            ,"N/A"
            ,"N/A"
            ,"8/1/2015"
            ,"N/A"
            ,"8/1/2015"
            ,"N/A"
            ,"8/1/2015"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"};

    String [] offerendarr = {"N/A"
            ,"N/A"
            ,"N/A"
            ,"8/31/2015"
            ,"N/A"
            ,"8/31/2015"
            ,"N/A"
            ,"9/30/2015"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"};


    String [] offerunitarr = {"N/A"
            ,"N/A"
            ,"N/A"
            ,"3"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"3"
            ,"9"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"
            ,"N/A"};

    String [] tyrepricesarr = {"1900","1950","2200","2150","2050",
            "2700","3800","3300","3850","4700",
            "6800","3400","3300","3700","3750",
            "1400","1350","1281","1172","1350",
            "1950","20800","3300","3600"};





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_activity_first);
        ButterKnife.bind(this);
            stinmanager = new StockInManager(SaleActivityFirst.this);

        List<StockTable> templist = StockTable.listAll(StockTable.class);
        Toaster.generatemessage(SaleActivityFirst.this, "" + templist.size());

      //  SugarRecord.deleteAll(StockTable.class);

        createDatabaseForPresentation();


        tyres_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SaleActivityFirst.this, VehicleTypeActivityNewFromSale.class));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        tubes_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    startActivity(new Intent(SaleActivityFirst.this, VehicletypeActivityForTube.class));
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });

        alloys_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toaster.generatemessage(SaleActivityFirst.this, "Under Development");
            }
        });



        others_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toaster.generatemessage(SaleActivityFirst.this, "Under Development");
            }
        });




    }

    private void createDatabaseForPresentation() {

            SugarRecord.deleteAll(StockTable.class);

     for(int i = 0 ; i<offerid.length-1 ; i++){
          new StockTable(tyreidarr[i],
                  vehicletypearr[i],
                  brandnamearr[i],
                  rimsizearr[i],
                  tyrenamearr[i],
                  tyrespecsarr[i],
                  tyretypearr[i],
                  isfavouritearr[i],
                  instockarr[i],
                  offers[i],
                  offerid[i],
                  offercompanyarr[i],
                  offername[i],
                  offerdescriptionarr[i],
                  offerstartarr[i],
                  offerendarr[i],
                  offerunitarr[i] ,
                  tyrepricesarr[i]).save();
      }

        List<StockTable> templist = StockTable.listAll(StockTable.class);
        Toaster.generatemessage(SaleActivityFirst.this , "size  "+templist.size());

    }

}
