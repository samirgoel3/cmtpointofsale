package com.spinno.cmtpointofsale.salesModle;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.InvoiceBucketTable;
import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.salesModle.addtube.VehicletypeActivityForTube;

import java.util.List;

import adapters.AdapterBucketList;
import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.InvoiceBucketManager;

public class TyreBucketActivity extends Activity {

    @Bind(R.id.back_button_in_bucket_activiy)LinearLayout back_btn;
    @Bind(R.id.list_view_in_bucket_activity)ListView list_tyre;

    ///for typeface
    @Bind(R.id.addmore_txt)TextView addmore;
    @Bind(R.id.ad_tube_txt)TextView addeube;
    @Bind(R.id.add_allow_txt)TextView addalloy;
    @Bind(R.id.add_others_txt)TextView addothers;
    @Bind(R.id.next)TextView next;

    ////////Databse variables
    InvoiceBucketManager ibmanager  ;
    List<InvoiceBucketTable> invoiceBucketData ;

    ///////// for for down buttons
    @Bind(R.id.add_more_button_in_tyre_bucket_activity) LinearLayout add_tyre_btn ;
    @Bind(R.id.add_tube_layout) LinearLayout add_tube ;
    @Bind(R.id.next_button_in_tyre_bucket_activity) LinearLayout next_btn ;

    public static int pressedonbAddMoreButton = 0 ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_bucket);
        ButterKnife.bind(this);
        ConstantClass.isBucketActivyopen = true ;
        ConstantClass.whichActivity = "TyreBucketActivity";
        pressedonbAddMoreButton = 0 ;
        ibmanager = new InvoiceBucketManager(TyreBucketActivity.this);
        invoiceBucketData = ibmanager.getalldata() ;
        loadtyresInList();

        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/AllerDisplay.ttf");
             addmore.setTypeface(tf, Typeface.BOLD);
             addeube.setTypeface(tf, Typeface.BOLD);
             addalloy.setTypeface(tf, Typeface.BOLD);
             addothers.setTypeface(tf, Typeface.BOLD);
             next.setTypeface(tf, Typeface.BOLD);




        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                ConstantClass.isBucketActivyopen = false ;
                overridePendingTransition(R.anim.no_sliding, R.anim.push_out_right);
            }
        });


        add_tyre_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pressedonbAddMoreButton = 1;
                finish();
                ConstantClass.isBucketActivyopen = false ;
                overridePendingTransition(R.anim.no_sliding, R.anim.push_out_right);
            }
        });


        add_tube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(TyreBucketActivity.this , VehicletypeActivityForTube.class));
                overridePendingTransition(R.anim.slide_in_up, R.anim.no_sliding);

            }
        });


        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TyreBucketActivity.this , PricingActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });
    }

    private void loadtyresInList() {

        list_tyre.setAdapter(new AdapterBucketList(TyreBucketActivity.this));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ConstantClass.isBucketActivyopen = false ;
       overridePendingTransition( R.anim.no_sliding ,R.anim.push_out_right);
    }
}
