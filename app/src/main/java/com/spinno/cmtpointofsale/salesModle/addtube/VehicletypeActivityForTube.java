package com.spinno.cmtpointofsale.salesModle.addtube;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;

public class VehicletypeActivityForTube extends Activity {

    @Bind(R.id.back_layout_in_vehicle_type_activity) LinearLayout back_btn ;
    @Bind(R.id.car_type_layout_in_tyre_type_activity) LinearLayout car_btn ;
    @Bind(R.id.two_wheeler_type_layout_in_tyre_type_activity) LinearLayout two_wheeler_btn ;
    @Bind(R.id.lcv_type_layout_in_tyre_type_activity) LinearLayout lcv_btn ;
    @Bind(R.id.hcv_type_layout_in_tyre_type_activity) LinearLayout hcv_btn ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicletype_activity_for_tube);
        ButterKnife.bind(this);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                if (ConstantClass.whichActivity.equals("TyreBucketActivity")) {
                    overridePendingTransition(R.anim.no_sliding, R.anim.slide_out_bottom);
                }
            }
        });


        car_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VehicletypeActivityForTube.this , TyreFilterparameterSecond.class));
                overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
            }
        });

        two_wheeler_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        lcv_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });



        hcv_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(ConstantClass.whichActivity.equals("TyreBucketActivity")){
            overridePendingTransition(R.anim.no_sliding ,R.anim.slide_out_bottom);
        }

    }
}
