package com.spinno.cmtpointofsale.salesModle.addtyres;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

/**
 * Created by spinnosolutions on 8/11/15.
 */
public class AdapterDrawerFilterCompanyStockinList extends BaseAdapter {

    Context con ;
    LayoutInflater inflater;

    ArrayList<String> colorsarr   = new ArrayList<>() ;




    public AdapterDrawerFilterCompanyStockinList(Context con, ArrayList<String> colorsarr){
        this.con = con ;
        this.colorsarr = colorsarr ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return colorsarr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view ;

        view = inflater.inflate(R.layout.item_grid, parent, false);

        final TextView companyname = (TextView) view.findViewById(R.id.company_name_item_of_grid_view);

        companyname.setText(colorsarr.get(position));

        return view;
    }


}
