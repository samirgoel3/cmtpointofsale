package com.spinno.cmtpointofsale.salesModle.addtyres;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

/**
 * Created by spinnosolutions on 8/11/15.
 */
public class AdapterDrawerFilterRimsizeStockInList extends BaseAdapter {
    ArrayList<String >rimsizes  = new ArrayList<>();


    Context con ;
    LayoutInflater inflater ;

    public AdapterDrawerFilterRimsizeStockInList(Context con ,ArrayList<String >rimsizes ){
        this.con = con ;
        this.rimsizes = rimsizes;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return rimsizes.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view ;

        view = inflater.inflate(R.layout.item_grid_for_tyre_rim, parent, false);

        final TextView rimsize = (TextView) view.findViewById(R.id.company_name_item_of_grid_view);
        rimsize.setText(rimsizes.get(position)+" inches");

        return view;
    }
}

