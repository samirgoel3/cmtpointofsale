package com.spinno.cmtpointofsale.salesModle.addtyres;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import databases.manager.StockInManager;

/**
 * Created by spinnosolutions on 8/13/15.
 */
public class AdapterTyreFilterTyreSizeStockInList extends BaseAdapter {

    Context con ;
    LayoutInflater inflater ;
    ArrayList<String> tyreSize ;
    StockInManager stinmanager ;



    public AdapterTyreFilterTyreSizeStockInList(Context con , ArrayList<String> tyreSize){
        this.con = con ;
        this.tyreSize = tyreSize ;
        stinmanager = new StockInManager(con);
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return tyreSize.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view ;
        view = inflater.inflate(R.layout.item_grid_tyre_size_for_stockin, parent, false);
        final TextView tyresize = (TextView) view.findViewById(R.id.company_name_item_of_grid_view);
        tyresize.setText(tyreSize.get(position));

        return view;
    }
}
