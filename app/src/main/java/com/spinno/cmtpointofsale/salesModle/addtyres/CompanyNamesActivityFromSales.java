package com.spinno.cmtpointofsale.salesModle.addtyres;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import adapters.AdapterCompanyNamesForSale;
import butterknife.Bind;
import butterknife.ButterKnife;
import contants.PlaceOrderModuleConstants;
import databases.manager.StockInManager;

public class CompanyNamesActivityFromSales extends Activity {
    @Bind(R.id.gridview_in_company_name_activity_from_sales)GridView grid_view;
    @Bind(R.id.back_layout_in_company_name_activity_from_sales)LinearLayout back_btn;
    @Bind(R.id.vehicle_type_image_in_company_name_activity_from_sales)ImageView vehicletype;

    ArrayList<String>   companies_arr_list  = new ArrayList<>();
    StockInManager stockInManager ;
    public static Activity activity ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_names_activity_from_sales);
        ButterKnife.bind(this);
        activity = this ;
        ////initializations
        stockInManager = new StockInManager(CompanyNamesActivityFromSales.this);
        setviewsaccordingtopreviouselection();





        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


     /*   if(PlaceOrderModuleConstants.vehicletype.equals("car_type")){
            List<StockTable> data_in_list;
            data_in_list = StockTable.find(StockTable.class, "Vehicle_Type = ?", "Car");
            Toaster.generatemessage(CompanyNamesActivityFromSales.this , "Total no SKU having CAR type  "+data_in_list.size());
            List<String> companynames = new ArrayList<>();
            for(int i = 0 ; i <data_in_list.size() ; i++){
                     companynames.add(data_in_list.get(i).getBrandName());
            }



            Set<String> hs = new HashSet<>();
            hs.addAll(companynames);
            data_in_list.clear();
            companynames.addAll(hs);

            Toaster.generatemessage(CompanyNamesActivityFromSales.this , "Companies in stock "+companynames.size());

        }else if(PlaceOrderModuleConstants.vehicletype.equals("two_wheeler_type")){
            List<StockTable> data_in_list;
            data_in_list = StockTable.find(StockTable.class, "Vehicle_Type = ?", "Two Wheeler");
            Toaster.generatemessage(CompanyNamesActivityFromSales.this , "Total no SKU having Tow wheeler type  "+data_in_list.size());

        }    */



        grid_view.setAdapter(new AdapterCompanyNamesForSale(CompanyNamesActivityFromSales.this ,stockInManager.getAllTypeOfCompaniesRelatedToVehicle(PlaceOrderModuleConstants.vehicletype)));

    }



    private void setviewsaccordingtopreviouselection() {
        if(PlaceOrderModuleConstants.vehicletype.equals("Car")){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (PlaceOrderModuleConstants.vehicletype.equals("Two Wheeler")){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("HCV")){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("LCV")){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
    }


}
