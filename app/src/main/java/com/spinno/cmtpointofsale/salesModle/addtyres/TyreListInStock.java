package com.spinno.cmtpointofsale.salesModle.addtyres;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;
import com.spinno.cmtpointofsale.InvoiceBucketTable;
import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.salesModle.TyreBucketActivity;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import adapters.AdapterListInStock;
import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;
import databases.manager.InvoiceBucketManager;
import databases.manager.StockInManager;

public class TyreListInStock extends Activity {

    @Bind(R.id.company_name_on_action_bar_of_tyre_list_in_stock)TextView company_name_on_actiopbar;
    @Bind(R.id.rim_size_on_action_bar_of_tyre_list_in_stock)TextView rim_size_on_action_bar;
    @Bind(R.id.tyre_size_on_action_bar_of_tyre_list_in_stock)TextView tyre_size_on_action_bar;
    @Bind(R.id.vehicle_image_on_action_bar_of_tyre_list_in_stock)ImageView vehicle_type_img_on_action_bar;
    @Bind(R.id.openfilter_in_tyreListInStock)LinearLayout openfilter_btn;

    @Bind(R.id.listview_in_tyre_list_in_stockt)ListView listview_stock_in_list;


    @Bind(R.id.loaderspinningwheeltyre_list_in_stock)LinearLayout loaderview;

    ////////// Drawer
    @Bind(R.id.gridview_in_drawer_of_tyreListInStock)GridView companies_grid ;
    @Bind(R.id.rimsizegridview_in_drawer_of_tyreListInStock)GridView rimsize_grid ;
    @Bind(R.id.brand_name_in_drawer_layout_tyre_stockin)TextView brand_name_txt ;
    @Bind(R.id.rim_Size_in_drawer_layout_tyre_stockin)TextView rimsize_txt ;


    /////////tyre bucket
    @Bind(R.id.cart_layout_tyreList_in_stock)View tyre_bucket ;
    @Bind(R.id.total_no_of_tyres_tyre_list_in_stocky)TextView total_no_of_tyre_in_bucket_txt ;
    InvoiceBucketManager ibmanager ;

    ///////// Databse
    StockInManager stinmanager ;


    private MenuDrawer mDrawer;
    public static int idsMenuOpen = 0 ;

    ///////// constants
    public static String temp_brand = "";
    public static String temp_rimSize = "";
    public static String  temp_tyreSize = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawer = MenuDrawer.attach(this , Position.RIGHT);
        mDrawer.setContentView(R.layout.activity_tyre_list_in_stock);
        mDrawer.setMenuView(R.layout.drawer_layout_tyrelistinstock);
        ButterKnife.bind(this);
        finishPreviousActivity();
///////check weather it from bucket activity after pressing add more buttons

        SugarRecord.deleteAll(InvoiceBucketTable.class);
        stinmanager = new StockInManager(TyreListInStock.this);
        ibmanager = new InvoiceBucketManager(TyreListInStock.this);
        tyre_bucket.setVisibility(View.GONE);


        loadlist();

        setViewsAccordingToPreciousSelection();

        openfilter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.toggleMenu();

            }
        });


///////////////companies gris listeer
        companies_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            //    Toaster.generatemessage(TyreListInStock.this, "" + stinmanager.getAllTypeOfCompaniesRelatedToVehicle(PlaceOrderModuleConstants.vehicletype).get(i));
                temp_brand = stinmanager.getAllTypeOfCompaniesRelatedToVehicle(PlaceOrderModuleConstants.vehicletype).get(i);
                brand_name_txt.setText("" + temp_brand);
                loadrimSizeInFilter(temp_brand);
            }
        });

///////////////Rim size grid listener
        rimsize_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(temp_brand.equals("")){
                    rimsize_txt.setText(stinmanager.getAllRimSizeByVehicleAndCompany(PlaceOrderModuleConstants.vehicletype , PlaceOrderModuleConstants.companytype).get(i)+" inches");
                    openDialogForTyreSizes(PlaceOrderModuleConstants.vehicletype , PlaceOrderModuleConstants.companytype ,stinmanager.getAllRimSizeByVehicleAndCompany(PlaceOrderModuleConstants.vehicletype , PlaceOrderModuleConstants.companytype).get(i));
                }else {
                    rimsize_txt.setText(stinmanager.getAllRimSizeByVehicleAndCompany(PlaceOrderModuleConstants.vehicletype , temp_brand).get(i));
                         openDialogForTyreSizes(PlaceOrderModuleConstants.vehicletype ,temp_brand , stinmanager.getAllRimSizeByVehicleAndCompany(PlaceOrderModuleConstants.vehicletype , temp_brand).get(i));
                }
            }
        });


///////////////lisener on item of listview
        listview_stock_in_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });




        mDrawer.setOnDrawerStateChangeListener(new MenuDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (oldState == 0) {
                    idsMenuOpen = 1 ;
                    companies_grid.setAdapter(new AdapterDrawerFilterCompanyStockinList(TyreListInStock.this, stinmanager.getAllTypeOfCompaniesRelatedToVehicle(PlaceOrderModuleConstants.vehicletype)));
                    brand_name_txt.setText(PlaceOrderModuleConstants.companytype);
                    rimsize_txt.setText(PlaceOrderModuleConstants.rimsize + " inches");
                    loadrimSizeInFilter(PlaceOrderModuleConstants.companytype);
                } else if (oldState == 8) {
                        idsMenuOpen = 0 ;
                    TyreBucketActivity.pressedonbAddMoreButton = 0 ;
                }
            }
        });

        tyre_bucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ConstantClass.isBucketActivyopen == true){
                          finish();
                    overridePendingTransition(R.anim.no_sliding, R.anim.slide_out_bottom);
                }else if(ConstantClass.isBucketActivyopen == false){
                    Intent in = new Intent(TyreListInStock.this , TyreBucketActivity.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
                }else {
                    Toaster.generatemessage(TyreListInStock.this , "Check TyreListInStock Activity Samir");
                }

            }
        });
    }

    private void finishPreviousActivity() {
        VehicleTypeActivityNewFromSale.activity.finish();
        CompanyNamesActivityFromSales.activity.finish();
        TyreRimsActivityFromSale.activity.finish();
        TyreSizeActivityFromSale.activity.finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        invoiceBucketResolver();
        if(TyreBucketActivity.pressedonbAddMoreButton == 1){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDrawer.openMenu();
                }
            }, 600);
        }
    }

    private void loadlist() {
        loaderview.setVisibility(View.VISIBLE);
        listview_stock_in_list.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loaderview.setVisibility(View.GONE);
                listview_stock_in_list.setVisibility(View.VISIBLE);
                listview_stock_in_list.setAdapter(new AdapterListInStock(TyreListInStock.this));


            }
        }, 2000);
    }

    private void openDialogForTyreSizes(final String vehicle , final String brand , final String rimsize) {
        final MaterialDialog dialog = new MaterialDialog.Builder(TyreListInStock.this)
                .title("Tyre Size")
                .customView(R.layout.tyre_size_custom_layout, true)
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(final MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                    }
                }).build();

        GridView dgview = (GridView) dialog.findViewById(R.id.grid_in_tyre_size_custom_layout);
                dgview.setAdapter(new AdapterTyreFilterTyreSizeStockInList(TyreListInStock.this, stinmanager.getAllTyreSizeByVehicleAndCompanyAndRimSize(vehicle, brand, rimsize)));

        dgview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dialog.dismiss();
                // Toaster.generatemessage(TyreListInStock.this  , ""+stinmanager.getAllTyreSizeByVehicleAndCompanyAndRimSize(vehicle, brand ,rimsize).get(i));
                PlaceOrderModuleConstants.companytype = brand_name_txt.getText().toString();
                PlaceOrderModuleConstants.rimsize = rimsize_txt.getText().toString();
                PlaceOrderModuleConstants.tyresize = stinmanager.getAllTyreSizeByVehicleAndCompanyAndRimSize(vehicle, brand, rimsize).get(i);
                loadlist();
                mDrawer.closeMenu();
            }
        });
        dialog.show();

    }

    public void loadrimSizeInFilter(String company) {
            rimsize_grid.setAdapter(new AdapterDrawerFilterRimsizeStockInList(TyreListInStock.this, stinmanager.getAllRimSizeByVehicleAndCompany(PlaceOrderModuleConstants.vehicletype, company)));
    }

    private void setViewsAccordingToPreciousSelection() {
        company_name_on_actiopbar.setText(PlaceOrderModuleConstants.companytype);
        rim_size_on_action_bar.setText(PlaceOrderModuleConstants.rimsize + " inches");
        tyre_size_on_action_bar.setText(PlaceOrderModuleConstants.tyresize);
        setupDrawerLayoutViewsAccordingly();
        if(PlaceOrderModuleConstants.vehicletype.equals("Car")){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.car_white_icon);
        }else  if (PlaceOrderModuleConstants.vehicletype.equals("Two Wheeler")){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("HCV")){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("LCV")){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.truck_icon_white);
        }
    }

    private void setupDrawerLayoutViewsAccordingly() {
        brand_name_txt.setText(""+PlaceOrderModuleConstants.companytype);
        rimsize_txt.setText("" + PlaceOrderModuleConstants.rimsize + " inches");
    }

    public void invoiceBucketResolver(){
        if(ibmanager.getTotalnooftyreInBucket()>0){
            tyre_bucket.setVisibility(View.VISIBLE);
            total_no_of_tyre_in_bucket_txt.setText(""+ibmanager.getTotalnooftyreInBucket());
        }else {
            tyre_bucket.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed() {
        if(idsMenuOpen == 0){
            super.onBackPressed();
        }else if(idsMenuOpen == 1){
                 mDrawer.closeMenu();
        }
    }
}
