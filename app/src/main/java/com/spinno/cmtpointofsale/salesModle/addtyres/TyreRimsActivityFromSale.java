package com.spinno.cmtpointofsale.salesModle.addtyres;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import adapters.AdapterRimSizeForSale;
import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;
import databases.manager.StockInManager;

public class TyreRimsActivityFromSale extends Activity {

    @Bind(R.id.gridview_in_rim_size_activity_from_sale)GridView gdview  ;
    @Bind(R.id.vehicle_type_image_in_tyre_rim_activity_from_sale)ImageView vehicletype;
    @Bind(R.id.company_name_tyre_rim_activity_from_sale)TextView companyname_txt;

    public static Activity activity ;

StockInManager stinmanager ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_rims_activity_from_sale);
        ButterKnife.bind(this);
        activity = this ;
        stinmanager = new StockInManager(TyreRimsActivityFromSale.this);
        ConstantClass.whichActivity = "TyreRimsActivityFromSale";
        setviewsaccordingtopreviouselection();
        findViewById(R.id.back_laupit_in_rim_size_activity_from_sale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        gdview.setAdapter(new AdapterRimSizeForSale(TyreRimsActivityFromSale.this,stinmanager.getAllRimSizeByVehicleAndCompany(PlaceOrderModuleConstants.vehicletype , PlaceOrderModuleConstants.companytype) ));
    }


    private void setviewsaccordingtopreviouselection() {
        if(PlaceOrderModuleConstants.vehicletype.equals("Car")){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (PlaceOrderModuleConstants.vehicletype.equals("Two Wheeler")){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("HCV")){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("LCV")){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }

        companyname_txt.setText(PlaceOrderModuleConstants.companytype);
    }


}
