package com.spinno.cmtpointofsale.salesModle.addtyres;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import adapters.AdapterTyreSizeForSale;
import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;
import databases.manager.StockInManager;

public class TyreSizeActivityFromSale extends Activity {
    @Bind(R.id.gridview_in_tyre_size_activity_from_sale)GridView gdview;
    @Bind(R.id.back_laupit_in_tyre_size_activity_from_sale)LinearLayout back_btn;
    @Bind(R.id.vehicle_type_image_in_tyre_size_activity_from_sale)ImageView vehicletype;
    @Bind(R.id.company_name_in_tyre_size_activity_from_sale)TextView companyname;
    @Bind(R.id.rim_size_in_tyre_size_activity_from_sale)TextView rimsize_txt;

    StockInManager stinmanager ;


    public static Activity activity ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_size_activity_from_sale);
        ConstantClass.whichActivity = "TyreSizeActivityFromSale";
        ButterKnife.bind(this);
        activity = this ;
        stinmanager = new StockInManager(TyreSizeActivityFromSale.this);
        setviewsaccordingtopreviouselection();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        gdview.setAdapter(new AdapterTyreSizeForSale(TyreSizeActivityFromSale.this, stinmanager.getAllTyreSizeByVehicleAndCompanyAndRimSize(PlaceOrderModuleConstants.vehicletype , PlaceOrderModuleConstants.companytype , PlaceOrderModuleConstants.rimsize)));

    }


    private void setviewsaccordingtopreviouselection() {
        companyname.setText(PlaceOrderModuleConstants.companytype);
        rimsize_txt.setText(PlaceOrderModuleConstants.rimsize);
        if(PlaceOrderModuleConstants.vehicletype.equals("Car")){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (PlaceOrderModuleConstants.vehicletype.equals("Two Wheeler")){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("HCV")){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (PlaceOrderModuleConstants.vehicletype.equals("LCV")){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
    }

}
