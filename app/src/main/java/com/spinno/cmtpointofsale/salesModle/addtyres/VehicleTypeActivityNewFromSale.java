package com.spinno.cmtpointofsale.salesModle.addtyres;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.ConstantClass;
import contants.PlaceOrderModuleConstants;
import databases.manager.StockInManager;

public class VehicleTypeActivityNewFromSale extends Activity {

    @Bind(R.id.back_layout_in_vehicle_type_activity)LinearLayout back_btn;
    @Bind(R.id.car_type_layout_in_tyre_type_activity)LinearLayout car_type_layout;
    @Bind(R.id.two_wheeler_type_layout_in_tyre_type_activity)LinearLayout twowheer_layout;
    @Bind(R.id.hcv_type_layout_in_tyre_type_activity)LinearLayout hcv_type_layout;
    @Bind(R.id.lcv_type_layout_in_tyre_type_activity)LinearLayout lcv_type_layout;


    Intent intent ;
    StockInManager  stinmanager;
    ArrayList<String> Companies  =  new ArrayList<>();

    public static Activity activity ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_type_activity_new);
        ButterKnife.bind(this);
        ConstantClass.whichActivity = "VehicleTypeActivityNewFromSale";
        activity = this;





        stinmanager = new StockInManager(VehicleTypeActivityNewFromSale.this);

        intent = new Intent(VehicleTypeActivityNewFromSale.this , CompanyNamesActivityFromSales.class);


        Companies = stinmanager.getAllTypeOfVehicleRelatedToTyre();

        if(Companies.contains("Car")){
            car_type_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaceOrderModuleConstants.vehicletype = "Car";
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

                }
            });
        }if(Companies.contains("Two Wheeler")){
            twowheer_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaceOrderModuleConstants.vehicletype = "Two Wheeler";
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }if(Companies.contains("LCV")){
            lcv_type_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toaster.generatemessage(VehicleTypeActivityNewFromSale.this, "LCV");
                }
            });
        }if(Companies.contains("HCV")){
            hcv_type_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toaster.generatemessage(VehicleTypeActivityNewFromSale.this, "HCV");
                }
            });
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });










    }
}
