package com.spinno.cmtpointofsale.startupscreens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.AddProductModule.AddProductCategoryActivity;
import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.placeordermodule.CartActivity;
import com.spinno.cmtpointofsale.placeordermodule.typeoforder.PlacingorderTypeActivity;
import com.spinno.cmtpointofsale.salesModle.SaleActivityFirst;
import com.spinno.cmtpointofsale.stockinModule.StartUpScreensForStockinModule.StockinSccreenFirst;

import net.simonvt.menudrawer.MenuDrawer;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends Activity {
    private MenuDrawer mDrawer;
    @Bind(R.id.menu_opener_layout_in_main_activity)LinearLayout menubtn;
    @Bind(R.id.purchase_order_in_activity_main)LinearLayout purchaseorderbtn;
    @Bind(R.id.stock_in__in_main_activity)LinearLayout stockinmenubtn;
    @Bind(R.id.cart_in_menu)LinearLayout cartinmenubtn;
    @Bind(R.id.sale_button_in_main_Activity)LinearLayout sales_btn ;
    @Bind(R.id.add_new_product)LinearLayout addNewProduct_btn  ;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawer = MenuDrawer.attach(this);
        mDrawer.setContentView(R.layout.activity_main);
        mDrawer.setMenuView(R.layout.menu_layout);
        mDrawer.setMenuSize(350);
        ButterKnife.bind(this);

        menubtn.setOnClickListener(menubtnlistener);
        purchaseorderbtn.setOnClickListener(placeorderbtnlistener);
        stockinmenubtn.setOnClickListener(stockinbtnlistener);
        addNewProduct_btn.setOnClickListener(addNewProductListener);


        cartinmenubtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this , CartActivity.class));
            }
        });


        sales_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this , SaleActivityFirst.class));
            }
        });
    }


    View.OnClickListener menubtnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                 //mDrawer.toggleMenu();
        }
    };


    View.OnClickListener placeorderbtnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainActivity.this , PlacingorderTypeActivity.class));
        }
    };



    View.OnClickListener stockinbtnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          startActivity(new Intent(MainActivity.this , StockinSccreenFirst.class));
        }
    };



    View.OnClickListener addNewProductListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainActivity.this, AddProductCategoryActivity.class));
        }
    };








}
