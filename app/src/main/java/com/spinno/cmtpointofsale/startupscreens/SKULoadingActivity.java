package com.spinno.cmtpointofsale.startupscreens;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import com.orm.SugarRecord;
import com.spinno.cmtpointofsale.AlloysTable;
import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.SKUTable;
import com.spinno.cmtpointofsale.TubesTable;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import checkers.Toaster;
import contants.CSVFile;
import databases.manager.AlloyManager;
import databases.manager.SKUManager;
import databases.manager.TubeManager;

public class SKULoadingActivity extends Activity {


    //////////////array list for Tyre Database ///////////////////
    public static ArrayList<String> Tyreid_arr = new ArrayList<>();
    public static ArrayList<String>  TyreVehicleType_arr = new ArrayList<>();
    public static ArrayList<String> TyreBrand_arr = new ArrayList<>();
    public static ArrayList<String> TyreRimSize_arr = new ArrayList<>();
    public static ArrayList<String> TyreSize_arr = new ArrayList<>();
    public static ArrayList<String> TyrePattern_arr = new ArrayList<>();
    public static ArrayList<String> TyreSpeedRating_arr = new ArrayList<>();
    public static ArrayList<String> TyreType_arr = new ArrayList<>();
    public static ArrayList<String> TyreDealerPrice_arr = new ArrayList<>();



    //////////////array list for Tube Database ///////////////////
    public static ArrayList<String> TUbeid_arr = new ArrayList<>();
    public static ArrayList<String>  TubeVehicleType_arr = new ArrayList<>();
    public static ArrayList<String> TubeBrand_arr = new ArrayList<>();
    public static ArrayList<String> TubeRimSize = new ArrayList<>();
    public static ArrayList<String> TubeSize_arr = new ArrayList<>();
    public static ArrayList<String> TubePrice_arr = new ArrayList<>();




    //////////////array list for Alloys Database ///////////////////
    public static ArrayList<String> AlloysID_arr = new ArrayList<>();
    public static ArrayList<String>  AlloyBrand_arr = new ArrayList<>();
    public static ArrayList<String> AlloysModel_arr = new ArrayList<>();
    public static ArrayList<String> Alloy_Colours_arr = new ArrayList<>();
    public static ArrayList<String> Alloydiameter_arr = new ArrayList<>();
    public static ArrayList<String> AlloyWidth_arr = new ArrayList<>();
    public static ArrayList<String> AlloyBolts_arr = new ArrayList<>();
    public static ArrayList<String> AlloyPcD_arr = new ArrayList<>();
    public static ArrayList<String> AloyCenterBore_arr = new ArrayList<>();
    public static ArrayList<String> offset_negetive_arr = new ArrayList<>();
    public static ArrayList<String> offset_positive_arr = new ArrayList<>();
    public static ArrayList<String> AlloyImge_arr = new ArrayList<>();
    public static ArrayList<String> AllDealerPrice_arr = new ArrayList<>();


    LoadTubedatabse loadtube ;




    SKUManager skum  ;
    TubeManager tum ;
    AlloyManager alm ;


    TextView loadtype_txt ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skuloading);

        loadtype_txt = (TextView) findViewById(R.id.loadtype_txt);

        skum = new SKUManager(SKULoadingActivity.this);
        tum = new TubeManager(SKULoadingActivity.this);
        alm = new AlloyManager(SKULoadingActivity.this);

        loadtube =   new LoadTubedatabse() ;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Tyreid_arr.clear();
                new loadTyredatabse().execute();
            }
        }, 2000);
    }

    private class loadTyredatabse extends AsyncTask<Void , Void , Integer>{


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadtype_txt.setText("Tyres");
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            SugarRecord.deleteAll(SKUTable.class);


            InputStream inputStream = getResources().openRawResource(R.raw.sku_data);
            CSVFile csvFile = new CSVFile(inputStream);
            List<String[]> scoreList = csvFile.read();
            String[] tyre ;
            for(int i = 1 ; i< scoreList.size() ; i++){
                tyre = scoreList.get(i);
                Tyreid_arr.add(tyre[0]);
                TyreVehicleType_arr.add(tyre[1]);
                TyreBrand_arr.add(tyre[2]);
                TyreRimSize_arr.add(tyre[3]);
                TyreSize_arr.add(tyre[4]);
                TyrePattern_arr.add(tyre[5]);
                TyreSpeedRating_arr.add(tyre[6]);
                TyreType_arr.add(tyre[7]);
                TyreDealerPrice_arr.add(tyre[8]);
            }
            for(int i = 0 ; i<Tyreid_arr.size() ; i++){
             skum.addToSKUTable(Tyreid_arr.get(i) , TyreVehicleType_arr.get(i) , TyreBrand_arr.get(i) , TyreRimSize_arr.get(i) ,TyreSize_arr.get(i) , TyrePattern_arr.get(i) , TyreSpeedRating_arr.get(i) , TyreType_arr.get(i) , TyreDealerPrice_arr.get(i));
            }
            return Tyreid_arr.size();
        }

        @Override
        protected void onPostExecute(Integer s) {
            super.onPostExecute(s);
            Toaster.generatemessage(SKULoadingActivity.this, "Total Tyres  " + s);
            loadtube.execute();
        }
    }




    private class LoadTubedatabse extends AsyncTask<Void , Void , Integer>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadtype_txt.setText("Tubes");
        }
        @Override
        protected Integer doInBackground(Void... voids) {
            SugarRecord.deleteAll(TubesTable.class);

            InputStream inputStream = getResources().openRawResource(R.raw.tubes_data);
            CSVFile csvFile = new CSVFile(inputStream);
            List<String[]> scoreList = csvFile.read();
            String[] tyre ;
            for(int i = 1 ; i< scoreList.size() ; i++){
                tyre = scoreList.get(i);

                TUbeid_arr.add(tyre[0]);
                TubeVehicleType_arr.add(tyre[1]);
                TubeBrand_arr.add(tyre[2]);
                TubeRimSize.add(tyre[3]);
                TubeSize_arr.add(tyre[4]);
                TubePrice_arr.add(tyre[5]);
            }

            for(int i = 0 ; i<TUbeid_arr.size() ; i++){
                Log.d("isme ja " , "tube id "+TUbeid_arr.get(i));
            tum.addToTubeTable(TUbeid_arr.get(i), TubeVehicleType_arr.get(i), TubeBrand_arr.get(i), TubeRimSize.get(i), TubeSize_arr.get(i), TubePrice_arr.get(i));
            }
            return TUbeid_arr.size();
        }

        @Override
        protected void onPostExecute(Integer s) {
            super.onPostExecute(s);
            Toaster.generatemessage(SKULoadingActivity.this, "Total tubes  " + s);
            new LoadAlloydatabse().execute();

        }
    }




    private class LoadAlloydatabse extends AsyncTask<Void , Void , Integer>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadtype_txt.setText("Alloys");
        }
        @Override
        protected Integer doInBackground(Void... voids) {
            SugarRecord.deleteAll(AlloysTable.class);

            InputStream inputStream = getResources().openRawResource(R.raw.alloys_data);
            CSVFile csvFile = new CSVFile(inputStream);
            List<String[]> scoreList = csvFile.read();
            String[] alloy ;
            for(int i = 1 ; i< scoreList.size() ; i++){
                alloy = scoreList.get(i);



                AlloysID_arr.add(alloy[0]);
                AlloyBrand_arr.add(alloy[1]);
                AlloysModel_arr.add(alloy[2]);
                Alloy_Colours_arr.add(alloy[3]);
                Alloydiameter_arr.add(alloy[4]);
                AlloyWidth_arr.add(alloy[5]);
                AlloyBolts_arr.add(alloy[6]);
                AlloyPcD_arr.add(alloy[7]);
                AloyCenterBore_arr.add(alloy[8]);
                offset_negetive_arr.add(alloy[9]);
                offset_positive_arr.add(alloy[10]);
                AlloyImge_arr.add(alloy[11]);
                AllDealerPrice_arr.add(alloy[12]);

            }

            for(int i = 0 ; i<AlloysID_arr.size() ; i++){
                alm.addToAlloysTable(AlloysID_arr.get(i),
                        AlloyBrand_arr.get(i),
                        AlloysModel_arr.get(i),
                        Alloy_Colours_arr.get(i),
                        Alloydiameter_arr.get(i),
                        AlloyWidth_arr.get(i),
                        AlloyBolts_arr.get(i),
                        AlloyPcD_arr.get(i),
                        AloyCenterBore_arr.get(i),
                        offset_negetive_arr.get(i),
                        offset_positive_arr.get(i),
                        AlloyImge_arr.get(i),
                        AllDealerPrice_arr.get(i));
            }
            return AlloysID_arr.size();
        }

        @Override
        protected void onPostExecute(Integer s) {
            super.onPostExecute(s);
            Toaster.generatemessage(SKULoadingActivity.this , "Total Alloys  "+s);
            finish();
            startActivity(new Intent(SKULoadingActivity.this, MainActivity.class));

        }
    }
}
