package com.spinno.cmtpointofsale.stockinModule.AlloysModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.AlloyManager;

public class AlloyBoltsActivity extends Activity {

    @Bind(R.id.gridview)GridView gdview ;

    ///////////////// Table Explorer
    AlloyManager alm ;
    ArrayList<String> bolts_arr = new ArrayList<>();

    public static Activity activity ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alloy_bolts);
        ButterKnife.bind(this);
        activity = this ;
        alm = new AlloyManager(AlloyBoltsActivity.this);






////////////////////////////////////////////////////////////////   grid listeners
        gdview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(ConstantClass.whichActivity.equals("AlloyListStockIn")){
                    /////// fill into the temp variable
                    if(AlloyListStockIn.temp_Alloy_brand.equals("")){
                        AlloyListStockIn.temp_Alloy_brand = AlloysConstants.AlloyBrand;
                    }if(AlloyListStockIn.temp__Alloy_diameter.equals("")){
                        AlloyListStockIn.temp__Alloy_diameter = AlloysConstants.AlloyDiameter;
                    }if(AlloyListStockIn.temp__Alloy_width.equals("")){
                        AlloyListStockIn.temp__Alloy_width = AlloysConstants.AlloyWidth ;
                    }
                    AlloyListStockIn.temp__Alloy_bolts = bolts_arr.get(i);
                    finish();
                }else {
                    ////////  fill into the main  variables
                    AlloysConstants.AlloyBolts = bolts_arr.get(i);
                    startActivity(new Intent(AlloyBoltsActivity.this, AlloyListStockIn.class));
                }
            }
        });

    }











    @Override
    protected void onResume() {
        super.onResume();
        loadGrid();
    }








    private void loadGrid() {
        if(!AlloyListStockIn.temp__Alloy_width.equals("")){
            //  load it from temp variables
            bolts_arr = alm.getAllBoltsByBrandDiameterWidth(AlloyListStockIn.temp_Alloy_brand ,AlloyListStockIn.temp__Alloy_diameter , AlloyListStockIn.temp__Alloy_width);
            gdview.setAdapter(new AdapterForGrid(AlloyBoltsActivity.this , bolts_arr));
        }else {
         //   load it from main variables
            bolts_arr = alm.getAllBoltsByBrandDiameterWidth(AlloysConstants.AlloyBrand ,AlloysConstants.AlloyDiameter , AlloysConstants.AlloyWidth);
            gdview.setAdapter(new AdapterForGrid(AlloyBoltsActivity.this , bolts_arr));
        }

    }
}
