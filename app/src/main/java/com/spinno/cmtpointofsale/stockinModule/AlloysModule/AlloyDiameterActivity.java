package com.spinno.cmtpointofsale.stockinModule.AlloysModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.AlloyManager;

public class AlloyDiameterActivity extends Activity {

    @Bind(R.id.gridview)GridView gdview ;

    ///////////////// Table Explorer
    AlloyManager alm ;
    ArrayList<String> diameter_arr = new ArrayList<>();

    public static Activity activity ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alloy_diameter);
        ButterKnife.bind(this);
        activity = this ;
        alm = new AlloyManager(AlloyDiameterActivity.this);




////////////////////   grid listeners
         gdview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             @Override
             public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                 if(ConstantClass.whichActivity.equals("AlloyListStockIn")){
                     if(AlloyListStockIn.temp_Alloy_brand.equals("")){
                         AlloyListStockIn.temp_Alloy_brand = AlloysConstants.AlloyBrand  ;
                     }
                     AlloyListStockIn.temp__Alloy_diameter = diameter_arr.get(i);
                     AlloyListStockIn.temp__Alloy_width = "";
                     AlloyListStockIn.temp__Alloy_bolts = "";
                     finish();

                 }else {
                     AlloysConstants.AlloyDiameter = diameter_arr.get(i);
                     startActivity(new Intent(AlloyDiameterActivity.this, AlloyWidthActivity.class));
                 }
             }
         });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!AlloyListStockIn.temp_Alloy_brand.equals("")){
            // load view accoding to the temp variable
            diameter_arr = alm.getAllDiameterByBrand(AlloyListStockIn.temp_Alloy_brand);
            gdview.setAdapter(new AdapterForGrid(AlloyDiameterActivity.this, diameter_arr));
        }else {
            //load view according to main variables
            diameter_arr = alm.getAllDiameterByBrand(AlloysConstants.AlloyBrand);
            gdview.setAdapter(new AdapterForGrid(AlloyDiameterActivity.this, diameter_arr));
        }

    }
}
