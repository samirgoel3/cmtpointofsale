package com.spinno.cmtpointofsale.stockinModule.AlloysModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.StockInEvents.TotalItemsInMiniBucketsEvent;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterAlloyActivity;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.ConstantClass;
import databases.manager.AlloyManager;
import databases.manager.StockInMiniBucketManager;
import de.greenrobot.event.EventBus;

public class AlloyListStockIn extends Activity {



//////////////////////////////////////////////////////////////////// Action Bar
    @Bind(R.id.brand_actionbar_txt) TextView brand_actionbar_txt ;
    @Bind(R.id.diameter_actionbartxt) TextView diameter_actionbartxt ;
    @Bind(R.id.rim_actionbar_txt) TextView rim_actionbar_txt ;
    @Bind(R.id.bolts_actionbar_txt) TextView bolts_actionbar_txt ;
    @Bind(R.id.openfilter_in_tyreListInStock)LinearLayout openfilter_in_tyreListInStock ;


/////////////////////////////////////////////////////////////  list and loader
    @Bind(R.id.listview) ListView listview ;
    @Bind(R.id.loaderspinningwheeltyre_list_in_stock)LinearLayout loaderview ;


//////////////////////////////////////////////////////////////////////////Bucket attributes
    @Bind(R.id.cart_layout_tyreList_in_stock)View  Bucket;
    @Bind(R.id.tyres_bucket)TextView tyre_Bucket;
    @Bind(R.id.tubes_bucket)TextView  tube_Bucket;
    @Bind(R.id.alloys_bucket)TextView  alloy_Bucket;
    @Bind(R.id.others_bucket)TextView  others_Bucket;
    @Bind(R.id.next)LinearLayout  next_Bucket;


//////////////////////////////////////////////////////////////////////////  FILTER ATTRIBUTES
    @Bind(R.id.brand_filter_btn)LinearLayout brand_filter_btn ;
    @Bind(R.id.diameter_filter_btn)LinearLayout diameter_filter_btn ;
    @Bind(R.id.width_filter_btn)LinearLayout width_filter_btn ;
    @Bind(R.id.bolts_filter_btn)LinearLayout bolts_filter_btn ;
    @Bind(R.id.apply_filter)LinearLayout apply_filter ;
    @Bind(R.id.brandname_filter_txt) TextView brandname_filter_txt ;
    @Bind(R.id.diameter_filter_txt) TextView diameter_filter_txt ;
    @Bind(R.id.width_filter_txt) TextView width_filter_txt ;
    @Bind(R.id.bolts_filter_txt) TextView bolts_filter_txt ;





    ///////////////// Table Explorer
    AlloyManager alm ;

    private EventBus bus = EventBus.getDefault();

    private MenuDrawer mDrawer;

    public static String temp_Alloy_brand = "";
    public static String temp__Alloy_diameter = "";
    public static String temp__Alloy_width = "";
    public static String temp__Alloy_bolts = "";


    StockInMiniBucketManager simbum ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDrawer = MenuDrawer.attach(this , Position.RIGHT);
        mDrawer.setContentView(R.layout.activity_alloy_list_stock_in);
        mDrawer.setMenuView(R.layout.drawer_layout_for_alloystockin);
        mDrawer.setDropShadowColor(R.color.client_colour_grey_dark);

        ButterKnife.bind(this);
        simbum = new StockInMiniBucketManager(AlloyListStockIn.this);
        ConstantClass.whichActivity = "AlloyListStockIn";
        alm =  new AlloyManager(AlloyListStockIn.this);
        bus.register(this);
        finishPreviousactivities();



        openfilter_in_tyreListInStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.toggleMenu();
            }
        });




        brand_filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AlloyListStockIn.this, AlloysBrandsActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });


        diameter_filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AlloyListStockIn.this, AlloyDiameterActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });


        width_filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AlloyListStockIn.this, AlloyWidthActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });


        bolts_filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AlloyListStockIn.this, AlloyBoltsActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });








        apply_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.closeMenu();

                if(!temp_Alloy_brand.equals("") && !temp__Alloy_diameter.equals("")  && !temp__Alloy_width.equals("")  && !temp__Alloy_bolts.equals("")){
                    AlloysConstants.AlloyBrand = temp_Alloy_brand ;
                    AlloysConstants.AlloyDiameter = temp__Alloy_diameter ;
                    AlloysConstants.AlloyWidth = temp__Alloy_width ;
                    AlloysConstants.AlloyBolts = temp__Alloy_bolts ;

                    loadlist();
                }else {
                    Toaster.generatemessage(AlloyListStockIn.this , "Please create a write filter first ");
                }

            }
        });






        mDrawer.setOnDrawerStateChangeListener(new MenuDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (oldState == 0 && newState == 1) {
                    //////    when ever the menu is open load data from main constants only
                    setFilterAttributeAccordingly();
                    setActionBarAccordingly();
                   emptyAllTempVariables();
                } else if (oldState == 8 && newState == 4) {
                    // runs when close
                    setFilterAttributeAccordingly();
                    setActionBarAccordingly();
                }
            }
        });









    }

    private void finishPreviousactivities() {
        AlloysBrandsActivity.activity.finish();
        AlloyDiameterActivity.activity.finish();
        AlloyWidthActivity.activity.finish();
        AlloyBoltsActivity.activity.finish();
    }




    public void onEvent(TotalItemsInMiniBucketsEvent event){
        tyre_Bucket.setText(event.getDatatyre());
        tube_Bucket.setText(event.getDatatube());
        others_Bucket.setText(event.getDataOthers());
        alloy_Bucket.setText(event.getDataAlloys());
    }

    @Override
    protected void onResume() {
        super.onResume();
        simbum.showDataOverBucket();
        loadlist();
    }


    @Override
    protected void onDestroy() {
        // Unregister
        bus.unregister(this);
        super.onDestroy();
    }


    private void loadlist() {

        setActionBarAccordingly();
        setFilterAttributeAccordingly();

        loaderview.setVisibility(View.VISIBLE);
        listview.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loaderview.setVisibility(View.GONE);
                listview.setVisibility(View.VISIBLE);
                listview.setAdapter(new AdapterAlloyActivity(AlloyListStockIn.this, alm.getAllAloyByBrandDiameterWidthBolts(AlloysConstants.AlloyBrand, AlloysConstants.AlloyDiameter, AlloysConstants.AlloyWidth, AlloysConstants.AlloyBolts)));
            }
        }, 1500);
    }

    private void setFilterAttributeAccordingly() {
        brandname_filter_txt.setText(""+AlloysConstants.AlloyBrand);
        diameter_filter_txt.setText(""+AlloysConstants.AlloyDiameter+" inches");
        width_filter_txt.setText(""+AlloysConstants.AlloyWidth+" inches");
        bolts_filter_txt.setText(""+AlloysConstants.AlloyBolts+" bolts");

        if(!temp_Alloy_brand.equals("")){
            brandname_filter_txt.setText(""+temp_Alloy_brand);
            diameter_filter_txt.setText("Click to select");
            width_filter_txt.setText("Click to select");
            bolts_filter_txt.setText("Click to select");

            if(!temp__Alloy_diameter.equals("")){
                diameter_filter_txt.setText(temp__Alloy_diameter);
                width_filter_txt.setText("Click to select");
                bolts_filter_txt.setText("Click to select");

                if(!temp__Alloy_width.equals("")){
                     width_filter_txt.setText(temp__Alloy_width);
                     bolts_filter_txt.setText("Click to select");

                    if(!temp__Alloy_bolts.equals("")){
                        bolts_filter_txt.setText(temp__Alloy_bolts);
                    }
                }
            }
        }
    }

    private void setActionBarAccordingly() {
        brand_actionbar_txt.setText(""+AlloysConstants.AlloyBrand);
        diameter_actionbartxt.setText(""+AlloysConstants.AlloyDiameter);
        rim_actionbar_txt.setText(""+AlloysConstants.AlloyWidth);
        bolts_actionbar_txt.setText(""+AlloysConstants.AlloyBolts);
    }




    public static  void emptyAllTempVariables(){
        temp_Alloy_brand = "";
        temp__Alloy_diameter = "";
        temp__Alloy_width = "";
        temp__Alloy_bolts = "";
    }
}
