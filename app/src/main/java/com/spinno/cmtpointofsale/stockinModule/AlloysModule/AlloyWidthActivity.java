package com.spinno.cmtpointofsale.stockinModule.AlloysModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.AlloyManager;

public class AlloyWidthActivity extends Activity {

    @Bind(R.id.gridview) GridView gdview ;

    ///////////////// Table Explorer
    AlloyManager alm ;
    ArrayList<String> width_arr = new ArrayList<>();


    public static Activity activity ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alloy_width);
        ButterKnife.bind(this);
        activity = this ;
        alm = new AlloyManager(AlloyWidthActivity.this);

////////////////////   grid listeners
        gdview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(ConstantClass.whichActivity.equals("AlloyListStockIn")){
                     // fill into temp variables
                    if(AlloyListStockIn.temp_Alloy_brand.equals("")){
                      AlloyListStockIn.temp_Alloy_brand = AlloysConstants.AlloyBrand;
                    }if(AlloyListStockIn.temp__Alloy_diameter.equals("")){
                        AlloyListStockIn.temp__Alloy_diameter = AlloysConstants.AlloyDiameter;
                    }
                    AlloyListStockIn.temp__Alloy_width = width_arr.get(i);
                    AlloyListStockIn.temp__Alloy_bolts = "";
                    finish();
                }else {
                    // fill into main variables
                    AlloysConstants.AlloyWidth = width_arr.get(i);
                    startActivity(new Intent(AlloyWidthActivity.this, AlloyBoltsActivity.class));
                }
            }
        });
    }






    @Override
    protected void onResume() {
        super.onResume();
        loadGrid();
    }

    private void loadGrid() {
        if(!AlloyListStockIn.temp__Alloy_diameter.equals("")){
           ///   load it form temp variables
            width_arr =  alm.getAllWidthByBrandsDiameter(AlloyListStockIn.temp_Alloy_brand , AlloyListStockIn.temp__Alloy_diameter);
            gdview.setAdapter(new AdapterForGrid(AlloyWidthActivity.this , width_arr));

        }else {
          //  load it from main variables
            width_arr =  alm.getAllWidthByBrandsDiameter(AlloysConstants.AlloyBrand , AlloysConstants.AlloyDiameter);
            gdview.setAdapter(new AdapterForGrid(AlloyWidthActivity.this , width_arr));
        }

    }
}
