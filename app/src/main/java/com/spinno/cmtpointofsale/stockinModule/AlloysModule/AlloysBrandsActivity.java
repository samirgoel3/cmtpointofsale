package com.spinno.cmtpointofsale.stockinModule.AlloysModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.AlloyManager;

public class AlloysBrandsActivity extends Activity {


    @Bind(R.id.gridview)GridView  gdview ;


    ///////////////// Table Explorer
    AlloyManager alm ;
    ArrayList<String> brands_arr = new ArrayList<>();

    public static Activity activity ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alloys_brands);
        ButterKnife.bind(this);
        activity = this;


///////////////// working of grid  view
        gdview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                if (ConstantClass.whichActivity.equals("AlloyListStockIn")) {
                    ///////////// fill in the temp variables
                    AlloyListStockIn.temp_Alloy_brand = brands_arr.get(i);
                    AlloyListStockIn.temp__Alloy_diameter = "";
                    AlloyListStockIn.temp__Alloy_width = "";
                    AlloyListStockIn.temp__Alloy_bolts = "";
                    finish();
                }else {
                    AlloysConstants.AlloyBrand = brands_arr.get(i);
                    startActivity(new Intent(AlloysBrandsActivity.this, AlloyDiameterActivity.class));
                }

            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        loadGrid();
    }

    private void loadGrid() {

        brands_arr = alm.getAllBrand();
        gdview.setAdapter(new AdapterForGrid(AlloysBrandsActivity.this ,brands_arr ));
    }
}
