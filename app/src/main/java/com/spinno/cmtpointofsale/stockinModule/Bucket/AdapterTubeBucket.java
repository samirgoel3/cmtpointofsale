package com.spinno.cmtpointofsale.stockinModule.Bucket;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.TubeMiniBucket;

import java.util.List;

import butterknife.ButterKnife;
import checkers.Toaster;

/**
 * Created by spinnosolutions on 9/1/15.
 */
public class AdapterTubeBucket extends BaseAdapter {

    Context con ;
    List<TubeMiniBucket> data ;
    LayoutInflater inflater ;


    public AdapterTubeBucket(Context con , List<TubeMiniBucket>  data  ){
        this.con = con ;
        this.data = data ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_tube_bucket, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toaster.generatemessage(con , "i am Tube ");
            }
        });

        return view;
    }

    static class ViewHolder {
        //  @Bind(R.id.title)TextView name;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
