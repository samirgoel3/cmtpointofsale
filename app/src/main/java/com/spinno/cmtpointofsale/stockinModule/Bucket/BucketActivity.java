package com.spinno.cmtpointofsale.stockinModule.Bucket;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.spinno.cmtpointofsale.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import databases.manager.StockInMiniBucketManager;

public class BucketActivity extends Activity {

    @Bind(R.id.back_button_in_set_pricing_activity)LinearLayout back_btn ;
    @Bind(R.id.tyre_list)ListView tyre_list ;
    @Bind(R.id.tube_list)ListView tube_list ;
    @Bind(R.id.alloy_list)ListView alloy_list ;
    @Bind(R.id.others_list)ListView others_list ;



    //////////////////// DATABASE VARIABLES
   StockInMiniBucketManager simbum ;
   Context con ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bucket);
        ButterKnife.bind(this);
         con = BucketActivity.this ;
        simbum  = new StockInMiniBucketManager(con);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.no_sliding, R.anim.push_out_right);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        loadDataFromMiniBuckets();
    }

    private void loadDataFromMiniBuckets() {
        tyre_list.setAdapter(new AdapterTyreBucket(con, simbum.getAllTyreFromMiniBucket()));
        setListViewHeightBasedOnChildren(tyre_list);
        tube_list.setAdapter(new AdapterTubeBucket(con, simbum.getAllTubeFromMiniBucket()));
        setListViewHeightBasedOnChildren(tube_list);
        alloy_list.setAdapter(new AdapterAlloyBucket(con, simbum.getAllAlloyMiniBucket()));
        setListViewHeightBasedOnChildren(alloy_list);
        others_list.setAdapter(new AdapterOtherBucket(con, simbum.getAllTubeOthersMiniBucket()));
        setListViewHeightBasedOnChildren(others_list);
    }


    /****Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
