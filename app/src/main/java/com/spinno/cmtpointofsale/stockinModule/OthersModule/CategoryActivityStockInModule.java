package com.spinno.cmtpointofsale.stockinModule.OthersModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CategoryActivityStockInModule extends Activity {

    @Bind(R.id.back_button_in_set_pricing_activity)LinearLayout back_btn ;
    @Bind(R.id.car_accesories_btn)LinearLayout carAccessories_btn ;
    @Bind(R.id.bike_accesories_btn)LinearLayout bike_accesories_btn ;
    @Bind(R.id.car_audio_btn)LinearLayout car_audio_btn ;
    @Bind(R.id.car_care_btn)LinearLayout car_care_btn ;
    @Bind(R.id.sphere_parts_btn)LinearLayout sphere_parts_btn ;



   public static  Activity activity ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_activity_stock_in_module);
        ButterKnife.bind(this);
        initializingVariables();

         back_btn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 finish();
             }
         });


        carAccessories_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OthersConstants.Categroy = OthersConstants.CAR_ACESSORIES_KEY;
                changeActivity();
            }
        });

        bike_accesories_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OthersConstants.Categroy = OthersConstants.BIKE_ACESSORIES_KEY;
                changeActivity();
            }
        });

        car_audio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OthersConstants.Categroy = OthersConstants.CAR_AUDIO__KEY;
                changeActivity();
            }
        });


        car_care_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OthersConstants.Categroy = OthersConstants.CAR_CARE_KEY ;
                changeActivity();
            }
        });


        sphere_parts_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OthersConstants.Categroy = OthersConstants.SPHERE_PARTS_KEY ;
                changeActivity();
            }
        });
    }

    private void initializingVariables() {
        activity = this ;
    }


    public void changeActivity(){
        startActivity(new Intent(CategoryActivityStockInModule.this , OthersListStockIn.class));
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }


}
