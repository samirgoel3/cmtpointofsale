package com.spinno.cmtpointofsale.stockinModule.OthersModule;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.StockInEvents.TotalItemsInMiniBucketsEvent;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterOtherList;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import databases.manager.OthersManager;
import databases.manager.StockInMiniBucketManager;
import de.greenrobot.event.EventBus;

public class OthersListStockIn extends Activity {




    ///////////// Action bar
    @Bind(R.id.back_button_in_bucket_activiy)LinearLayout back_btn ;
    @Bind(R.id.category_name_Actionbar_txt)TextView category_name_Actionbar_txt;



    //////////////  ListView and loader
    @Bind(R.id.listview)ListView other_list ;
    @Bind(R.id.loaderspinningwheeltyre_list_in_stock)LinearLayout loaderview ;


    ////////////////////////Bucket attributes
    @Bind(R.id.cart_layout_tyreList_in_stock)View Bucket;
    @Bind(R.id.tyres_bucket)TextView tyre_Bucket;
    @Bind(R.id.tubes_bucket)TextView  tube_Bucket;
    @Bind(R.id.alloys_bucket)TextView  alloy_Bucket;
    @Bind(R.id.others_bucket)TextView  others_Bucket;
    @Bind(R.id.next)LinearLayout  next_Bucket;


    StockInMiniBucketManager  simbum  ;
    OthersManager  otm ;
    private EventBus bus = EventBus.getDefault();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_list_stock_in);
        ButterKnife.bind(this);
        simbum = new StockInMiniBucketManager(OthersListStockIn.this);
        otm = new OthersManager(OthersListStockIn.this);
        bus.register(this);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }



    public void onEvent(TotalItemsInMiniBucketsEvent event){
        tyre_Bucket.setText(event.getDatatyre());
        tube_Bucket.setText(event.getDatatube());
        others_Bucket.setText(event.getDataOthers());
        alloy_Bucket.setText(event.getDataAlloys());
    }


    @Override
    protected void onResume() {
        super.onResume();
        simbum.showDataOverBucket();
        loadActionBar();
        finishPreviousActivity();
        loadlist();
    }

    private void loadlist() {


        loaderview.setVisibility(View.VISIBLE);
        other_list.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loaderview.setVisibility(View.GONE);
                other_list.setVisibility(View.VISIBLE);
                Toaster.generatemessage(OthersListStockIn.this, "" + otm.getAllProductBySpecificcategory(OthersConstants.Categroy).size());
                other_list.setAdapter(new AdapterOtherList(OthersListStockIn.this, otm.getAllProductBySpecificcategory(OthersConstants.Categroy)));

            }
        }, 1500);
    }


    @Override
    protected void onDestroy() {
        // Unregister
        bus.unregister(this);
        super.onDestroy();
    }

    private void finishPreviousActivity() {
        CategoryActivityStockInModule.activity.finish();
    }

    private void loadActionBar() {
        category_name_Actionbar_txt.setText(""+OthersConstants.Categroy);
    }
}
