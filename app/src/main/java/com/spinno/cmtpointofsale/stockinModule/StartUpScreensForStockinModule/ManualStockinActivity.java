package com.spinno.cmtpointofsale.stockinModule.StartUpScreensForStockinModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.AlloysModule.AlloysBrandsActivity;
import com.spinno.cmtpointofsale.stockinModule.OthersModule.CategoryActivityStockInModule;
import com.spinno.cmtpointofsale.stockinModule.TubeFilter.VehicleActivityTubeStockin;
import com.spinno.cmtpointofsale.stockinModule.tyreFilter.VehicleTypeActivityStockinModule;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.StockInMiniBucketManager;

public class ManualStockinActivity extends Activity {


    @Bind(R.id.back_button_in_manual_stockin_screen)LinearLayout back_btn ;
    @Bind(R.id.add_tyre_button_in_manual_stockin) LinearLayout addtyre ;
    @Bind(R.id.add_tube) LinearLayout addTube ;
    @Bind(R.id.add_Alloy) LinearLayout addAlloy ;
    @Bind(R.id.add_others) LinearLayout addothers ;

    StockInMiniBucketManager simbummanager ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_stockin);
        ButterKnife.bind(this);

        simbummanager = new StockInMiniBucketManager(ManualStockinActivity.this );





        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        addtyre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ManualStockinActivity.this , VehicleTypeActivityStockinModule.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });


        addTube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ManualStockinActivity.this , VehicleActivityTubeStockin.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });


        addAlloy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ManualStockinActivity.this , AlloysBrandsActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        addothers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ManualStockinActivity.this , CategoryActivityStockInModule.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        ConstantClass.whichActivity = "ManualStockinActivity";
      //  StockInMiniBucketManager.removeFromAllMiniBuckets();
    }
}
