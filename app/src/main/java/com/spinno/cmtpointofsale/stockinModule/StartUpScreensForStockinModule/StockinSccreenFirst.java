package com.spinno.cmtpointofsale.stockinModule.StartUpScreensForStockinModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;

public class StockinSccreenFirst extends Activity {

    @Bind(R.id.back_button_in_stockin_screen_first)LinearLayout back_btn ;
    @Bind(R.id.againstpurchase)LinearLayout against_purchase_btn ;
    @Bind(R.id.direct_purchase)LinearLayout direct_purchase_btn ;


    //////////////array list for local databse ///////////////////
    public static  ArrayList<String> Tyreid_arr = new ArrayList<>();
    public static ArrayList<String>  TyreVehicleType_arr = new ArrayList<>();
    public static ArrayList<String> TyreBrand_arr = new ArrayList<>();
    public static ArrayList<String> TyreRimSize_arr = new ArrayList<>();
    public static ArrayList<String> TyreSize_arr = new ArrayList<>();
    public static ArrayList<String> TyrePattern_arr = new ArrayList<>();
    public static ArrayList<String> TyreSpeedRating_arr = new ArrayList<>();
    public static ArrayList<String> TyreType_arr = new ArrayList<>();
    public static ArrayList<String> TyreDealerPrice_arr = new ArrayList<>();






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stockin_sccreen_first);
        ButterKnife.bind(this);
        loadDatabseFromServer();


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        against_purchase_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toaster.generatemessage(StockinSccreenFirst.this, "Under Development");
            }
        });


        direct_purchase_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StockinSccreenFirst.this, ManualStockinActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });
    }

    private void loadDatabseFromServer() {

        /* new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
               /*i InputStream inputStream = getResources().openRawResource(R.raw.falken_data);
                CSVFile csvFile = new CSVFile(inputStream);
                List<String[]> scoreList = csvFile.read();
                String[] tyre ;

                for(int i = 0 ; i< scoreList.size() ; i++){
                    tyre = scoreList.get(i);
                    Tyreid_arr.add(tyre[0]);
                }
                Toaster.generatemessage(StockinSccreenFirst.this  , "Executed ");


            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Toaster.generatemessage(StockinSccreenFirst.this  , ""+s);
            }
        }.execute();    */


    }
}
