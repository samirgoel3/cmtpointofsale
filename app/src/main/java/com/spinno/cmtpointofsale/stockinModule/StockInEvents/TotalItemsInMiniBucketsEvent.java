package com.spinno.cmtpointofsale.stockinModule.StockInEvents;

/**
 * Created by spinnosolutions on 8/25/15.
 */
public class TotalItemsInMiniBucketsEvent {

    private String datatyre;
    private String datatube ;
    private String dataOthers ;



    private String dataAlloys ;


    public TotalItemsInMiniBucketsEvent(String datatyre, String datatube ,String dataOthersitems , String dataAlloys ) {
        this.datatyre = datatyre;
        this.datatube = datatube;
        this.dataOthers = dataOthersitems ;
        this.dataAlloys = dataAlloys ;

    }

    public String getDatatube() {
        return datatube;
    }

    public String getDatatyre() {
        return datatyre;
    }

    public String getDataOthers() {
        return dataOthers;
    }

    public String getDataAlloys() {
        return dataAlloys;
    }
}



