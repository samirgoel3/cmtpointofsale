package com.spinno.cmtpointofsale.stockinModule.StockinConstants;

/**
 * Created by spinnosolutions on 8/18/15.
 */
public class StockInConstants {

/////////// for tyres
    public static String VehicleType ="";
    public static String BrandName="";
    public static String RimSize="";
    public static String TyreSize="";
    public static String TyreType ="";
    public static String TyreNoOfUnits="";
    public static String TyrePRicePerUnit ="";


    public static String CAR_KEY = "Car";
    public static String TWO_WHEELER_KEY = "Two Wheeler";
    public static String LCV_KEY = "LCV";
    public static String HCV_KEY = "HCV";

    public static String Tyre_KEY = "Tyre";
    public static String Tube_KEY = "Tube";
    public static String Alloy_KEY = "Alloy";
    public static String Others_KEY = "Others";


////////////  for tubes
public static String TubeVehicle ="";
    public static String TubeBrandName="";
    public static String TubeRimSize="";
    public static String TubeTyreSize="";



   public static String ItemCategory = "";












    public static void clearAllMainConstants(){
         VehicleType ="";
         BrandName="";
         RimSize="";
         TyreSize="";
         TyreType ="";
         TyreNoOfUnits="";
         TyrePRicePerUnit ="";
    }
}
