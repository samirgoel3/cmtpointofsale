package com.spinno.cmtpointofsale.stockinModule.TubeFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.TubeManager;

public class BrandsNameActivityTubeInStock extends Activity {


    @Bind(R.id.gridview_in_company_names_activity)GridView grid_view;
    @Bind(R.id.back_laupit_in_company_name_activity_new)LinearLayout back_btn;
    @Bind(R.id.vehicle_type_image_in_companyname_activity)ImageView vehicletype;

    ///////////////// Table Explorer
    TubeManager tum ;
    ArrayList<String> brands_arr = new ArrayList<>();

    public static Activity activity ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brands_name_activity_tube_in_stock);
        ButterKnife.bind(this);
        activity = this ;
        tum = new TubeManager(BrandsNameActivityTubeInStock.this);



        setviewsaccordingtopreviouselection();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (ConstantClass.whichActivity.equals("TubeSizeListActivityStockInModule")) {
                    ///////////// fill in the temp variables
                    if(TubeSizeListActivityStockInModule.temp_vehicle.equals("")){
                        TubeSizeListActivityStockInModule.temp_vehicle = StockInConstants.TubeVehicle ;
                    }
                    TubeSizeListActivityStockInModule.temp_brand = brands_arr.get(i);
                    TubeSizeListActivityStockInModule.temp_rimsize = "";
                    finish();
                } else {
                    //////   fill in the Main variables
                    StockInConstants.TubeBrandName = brands_arr.get(i);
                    startActivity(new Intent(BrandsNameActivityTubeInStock.this, RimSizeActivityTubeStockiNModule.class));
                }
            }
        });

    }

    private void setviewsaccordingtopreviouselection() {
        loadGrid();
        if( StockInConstants.TubeVehicle.equals(StockInConstants.CAR_KEY)){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if ( StockInConstants.TubeVehicle.equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if ( StockInConstants.TubeVehicle.equals(StockInConstants.HCV_KEY)){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if ( StockInConstants.TubeVehicle.equals(StockInConstants.LCV_KEY)){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
    }

    private void loadGrid() {
        if(!TubeSizeListActivityStockInModule.temp_vehicle.equals("")){
            /// load grid according to temp constants
            brands_arr = tum.GetAllBrandsByVehicle(TubeSizeListActivityStockInModule.temp_vehicle);
            grid_view.setAdapter(new AdapterForGrid(BrandsNameActivityTubeInStock.this , brands_arr));
        }else {
            ////    load grid AT main constants
            brands_arr = tum.GetAllBrandsByVehicle(StockInConstants.TubeVehicle);
            grid_view.setAdapter(new AdapterForGrid(BrandsNameActivityTubeInStock.this , brands_arr));
        }

    }

}
