package com.spinno.cmtpointofsale.stockinModule.TubeFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.TubeManager;

public class RimSizeActivityTubeStockiNModule extends Activity {

    @Bind(R.id.gridview_in_rim_size_activity)GridView gdview  ;
    @Bind(R.id.vehicle_type_image_in_tyre_rim_activity)ImageView vehicletype;
    @Bind(R.id.company_name_tyre_rim_activity)TextView companyname_txt;

    public static Activity activity ;

    ///////////////// Table Explorer
    TubeManager tum ;
    ArrayList<String> rim_arr = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rim_size_activity_tube_stocki_nmodule);
        ButterKnife.bind(this);
        activity = this ;
        tum = new TubeManager(RimSizeActivityTubeStockiNModule.this);

        setviewsaccordingtopreviouselection();

        gdview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(ConstantClass.whichActivity.equals("TubeSizeListActivityStockInModule")){
                    /////  fill into temp variables
                    if(TubeSizeListActivityStockInModule.temp_vehicle.equals("")){
                        TubeSizeListActivityStockInModule.temp_vehicle = StockInConstants.TubeVehicle;
                    }if(TubeSizeListActivityStockInModule.temp_brand.equals("")){
                        TubeSizeListActivityStockInModule.temp_brand = StockInConstants.TubeBrandName ;
                    }
                    TubeSizeListActivityStockInModule.temp_rimsize = rim_arr.get(i);
                    finish();
                }else {
                    ///////   fill into main variables
                    StockInConstants.TubeRimSize = rim_arr.get(i);
                    startActivity(new Intent(RimSizeActivityTubeStockiNModule.this ,TubeSizeListActivityStockInModule.class));
                }
            }
        });
    }



    private void setviewsaccordingtopreviouselection() {
        loadGrid();
        if(StockInConstants.TubeVehicle.equals(StockInConstants.CAR_KEY)){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (StockInConstants.TubeVehicle.equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (StockInConstants.TubeVehicle.equals(StockInConstants.HCV_KEY)){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (StockInConstants.TubeVehicle.equals(StockInConstants.LCV_KEY)){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
        companyname_txt.setText(StockInConstants.TubeBrandName);
    }

    private void loadGrid() {
        if(!TubeSizeListActivityStockInModule.temp_brand.equals("") ){
            //////   load grid according to the temp variables
            rim_arr =  tum.getAllRimByVehicleBrand(TubeSizeListActivityStockInModule.temp_vehicle, TubeSizeListActivityStockInModule.temp_brand);
            gdview.setAdapter(new AdapterForGrid(RimSizeActivityTubeStockiNModule.this, rim_arr));
        }else {
            ////////   load grid according to the main  variables
            rim_arr =  TubeManager.getAllRimByVehicleBrand(StockInConstants.TubeVehicle , StockInConstants.TubeBrandName);
            gdview.setAdapter(new AdapterForGrid(RimSizeActivityTubeStockiNModule.this, rim_arr));
        }
    }
}
