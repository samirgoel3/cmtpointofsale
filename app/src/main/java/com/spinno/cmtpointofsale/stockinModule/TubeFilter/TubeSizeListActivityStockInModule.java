package com.spinno.cmtpointofsale.stockinModule.TubeFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.TubesTable;
import com.spinno.cmtpointofsale.stockinModule.StockInEvents.TotalItemsInMiniBucketsEvent;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForTubeList;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.ConstantClass;
import databases.manager.StockInMiniBucketManager;
import databases.manager.TubeManager;
import de.greenrobot.event.EventBus;

public class TubeSizeListActivityStockInModule extends Activity {


    /////////////////// action bar attribute
    @Bind(R.id.company_name_on_action_bar_of_tyre_list_in_stock)TextView company_name_on_actiopbar;
    @Bind(R.id.rim_size_on_action_bar_of_tyre_list_in_stock)TextView rim_size_on_action_bar;
    @Bind(R.id.tyre_size_on_action_bar_of_tyre_list_in_stock)TextView tyre_size_on_action_bar;
    @Bind(R.id.vehicle_image_on_action_bar_of_tyre_list_in_stock)ImageView vehicle_type_img_on_action_bar;
    @Bind(R.id.openfilter_in_tyreListInStock)LinearLayout openfilter_btn;


//////////////  ListView and loader
    @Bind(R.id.listview)ListView tube_list ;
    @Bind(R.id.loaderspinningwheeltyre_list_in_stock)LinearLayout loaderview ;




    ///////////////////////// filter

    @Bind(R.id.vehicle_type_layout)LinearLayout vehicleFilter ;
    @Bind(R.id.vehicle_type)TextView vehicletypeFilter_txt;
    @Bind(R.id.vehicle_image)ImageView vehicleImageFilter_img   ;
    @Bind(R.id.brand_linear_layout)LinearLayout brandFilter;
    @Bind(R.id.brand_name_in_filter)TextView brandnameFilter_txt;
    @Bind(R.id.rimsize_linear_layout)LinearLayout rim_SizeFilter;
    @Bind(R.id.rim_size_filter_txt)TextView rimsizeFilter_txt;
    @Bind(R.id.apply_filter)LinearLayout apply ;






    ////////////////////////Bucket attributes
    @Bind(R.id.cart_layout_tyreList_in_stock)View Bucket;
    @Bind(R.id.tyres_bucket)TextView  tyre_Bucket;
    @Bind(R.id.tubes_bucket)TextView  tube_Bucket;
    @Bind(R.id.alloys_bucket)TextView  alloy_Bucket;
    @Bind(R.id.others_bucket)TextView  others_Bucket;
    @Bind(R.id.next)LinearLayout  next_Bucket;



    ////////////////////   temporary variable
    public static String temp_vehicle = "";
    public static String temp_brand = "";
    public static String temp_rimsize = "";


    ////////databse variables
    TubeManager tum    ;




    private MenuDrawer mDrawer;

    private EventBus bus = EventBus.getDefault();

    StockInMiniBucketManager simbum ;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tube_size_list_activity_stock_in_module); mDrawer = MenuDrawer.attach(this , Position.RIGHT);
        mDrawer.setContentView(R.layout.activity_tyre_list_stock_in_module);
        mDrawer.setMenuView(R.layout.drawer_filter_for_tube);
        mDrawer.setDropShadowColor(R.color.client_colour_grey_dark);
        ButterKnife.bind(this);
        tum = new TubeManager(TubeSizeListActivityStockInModule.this);
        ConstantClass.whichActivity = "TubeSizeListActivityStockInModule";

        simbum = new StockInMiniBucketManager(TubeSizeListActivityStockInModule.this);

        // Register as a subscriber
        bus.register(this);

        VehicleActivityTubeStockin.activity.finish();
        BrandsNameActivityTubeInStock.activity.finish();
        RimSizeActivityTubeStockiNModule.activity.finish();

        loadtyreList();


        openfilter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openMenu();
            }
        });


        vehicleFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TubeSizeListActivityStockInModule.this, VehicleActivityTubeStockin.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });


        brandFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TubeSizeListActivityStockInModule.this, BrandsNameActivityTubeInStock.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });



        rim_SizeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (brandnameFilter_txt.getText().toString().equals("Click to select")) {
                    Toaster.generatemessage(TubeSizeListActivityStockInModule.this, "Please select Brand First");
                } else {
                    startActivity(new Intent(TubeSizeListActivityStockInModule.this, RimSizeActivityTubeStockiNModule.class));
                    overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
                }
            }
        });















        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //////  when ever click on apply button check all the data in tem variable  , and if available in all tem varible load it in list , replace all main variable by tem variables and empty tem variable
                mDrawer.closeMenu();

                if(!temp_vehicle.equals("")  && !temp_brand.equals("") && !temp_rimsize.equals("")) {
                    if (!TubeSizeListActivityStockInModule.temp_vehicle.equals("")) {
                        StockInConstants.TubeVehicle = TubeSizeListActivityStockInModule.temp_vehicle;
                    }
                    if (!TubeSizeListActivityStockInModule.temp_brand.equals("")) {
                        StockInConstants.TubeBrandName = TubeSizeListActivityStockInModule.temp_brand;
                    }
                    if (!TubeSizeListActivityStockInModule.temp_rimsize.equals("")) {
                        StockInConstants.TubeRimSize = TubeSizeListActivityStockInModule.temp_rimsize;
                    }

                    Toaster.generatemessage(TubeSizeListActivityStockInModule.this , "Main vehicle "+StockInConstants.VehicleType+" Main Brand  "+StockInConstants.BrandName + " Main Rim "+StockInConstants.RimSize+" Main TyreSize "+StockInConstants.TyreSize);
                    loadtyreList();
                    setViewsAccordingToPreciousSelection();
                }else {
                    Toaster.generatemessage(TubeSizeListActivityStockInModule.this, "Please create filter first");
                }
            }
        });




















        mDrawer.setOnDrawerStateChangeListener(new MenuDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (oldState == 0 && newState == 1) {
                    //////    when ever thje menu is open load data from main constants only
                    setViewsAccordingToPreciousSelection();
                    TubeSizeListActivityStockInModule.clearAllTempConstants();
                } else if (oldState == 8 && newState == 4) {
                    // runs when close
                    setViewsAccordingToPreciousSelection();
                }
            }
        });
    }

    private void loadtyreList() {
        //Bucket.setVisibility(View.GONE);
        loaderview.setVisibility(View.VISIBLE);
        tube_list.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loaderview.setVisibility(View.GONE);
                tube_list.setVisibility(View.VISIBLE);
                List<TubesTable> data = tum.getAllTubeSizesByVehicleCompanyRimSizeTyresize(StockInConstants.TubeVehicle, StockInConstants.TubeBrandName, StockInConstants.TubeRimSize);
                Toaster.generatemessage(TubeSizeListActivityStockInModule.this, "" + data.size());
                tube_list.setAdapter(new AdapterForTubeList(TubeSizeListActivityStockInModule.this, data));
            }
        }, 1500);










    }


    public void onEvent(TotalItemsInMiniBucketsEvent event){
        tyre_Bucket.setText( event.getDatatyre());
        tube_Bucket.setText(event.getDatatube());
        others_Bucket.setText(event.getDataOthers());
        alloy_Bucket.setText(event.getDataAlloys());
    }


    @Override
    protected void onResume() {
        super.onResume();
        StockInConstants.ItemCategory = StockInConstants.Tube_KEY ;
        ConstantClass.whichActivity = "TubeSizeListActivityStockInModule";
        simbum.showDataOverBucket();
        setViewsAccordingToPreciousSelection();
    }


    @Override
    protected void onDestroy() {
        // Unregister
        bus.unregister(this);
        super.onDestroy();
    }


    private void setViewsAccordingToPreciousSelection() {
        company_name_on_actiopbar.setText(StockInConstants.TubeBrandName);
        rim_size_on_action_bar.setText(StockInConstants.TubeRimSize + " inches");
        tyre_size_on_action_bar.setVisibility(View.GONE);
        setupDrawerLayoutViewsAccordingly();
        if(StockInConstants.TubeVehicle.equals(StockInConstants.CAR_KEY)){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.car_white_icon);
        }else  if (StockInConstants.TubeVehicle.equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (StockInConstants.TubeVehicle.equals(StockInConstants.HCV_KEY)){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (StockInConstants.TubeVehicle.equals(StockInConstants.LCV_KEY)){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.truck_icon_white);
        }
    }

    public void setupDrawerLayoutViewsAccordingly(){
        vehicletypeFilter_txt.setText("" + StockInConstants.TubeVehicle);
        brandnameFilter_txt.setText(StockInConstants.TubeBrandName);
        rimsizeFilter_txt.setText(StockInConstants.TubeRimSize);


        if(!TubeSizeListActivityStockInModule.temp_vehicle.equals("")){
            /// make downer rim and tyre size unclickable and set all downer text to click to select
            vehicletypeFilter_txt.setText(""+TubeSizeListActivityStockInModule.temp_vehicle);
            brandnameFilter_txt.setText("Click to select");
            rimsizeFilter_txt.setText("Click to select");
            if(!TubeSizeListActivityStockInModule.temp_brand.equals("")){
                brandnameFilter_txt.setText(TubeSizeListActivityStockInModule.temp_brand);
                //////check rim in temp is empty or not
                if(!TubeSizeListActivityStockInModule.temp_rimsize.equals("")){
                    rimsizeFilter_txt.setText(TubeSizeListActivityStockInModule.temp_rimsize);
                }else {
                    rimsizeFilter_txt.setText("Click to select");
                }
            }else {
                /////////   do nothing for brand
            }
        }else {
            ////// do nothing  for vehicle
        }
        setVehicleImageAccrodingly();
    }




    private void setVehicleImageAccrodingly() {
        if(vehicletypeFilter_txt.getText().toString().equals(StockInConstants.CAR_KEY)){
            vehicleImageFilter_img.setImageResource(R.drawable.car_icon_grey);
        }if(vehicletypeFilter_txt.getText().toString().equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicleImageFilter_img.setImageResource(R.drawable.scooter_icon_grey);
        }if(vehicletypeFilter_txt.getText().toString().equals(StockInConstants.LCV_KEY)){
            vehicleImageFilter_img.setImageResource(R.drawable.truck_icon_grey);
        }if(vehicletypeFilter_txt.getText().toString().equals(StockInConstants.HCV_KEY)){
            vehicleImageFilter_img.setImageResource(R.drawable.hcv_icon_grey);
        }
    }


    public static void clearAllTempConstants(){
        temp_vehicle = "";
        temp_brand = "";
        temp_rimsize = "";
    }


}