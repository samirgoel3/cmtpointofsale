package com.spinno.cmtpointofsale.stockinModule.TubeFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;
import com.spinno.cmtpointofsale.stockinModule.tyreFilter.TyreListStockInModule;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.ConstantClass;
import databases.manager.TubeManager;

public class VehicleActivityTubeStockin extends Activity {

    @Bind(R.id.back_button)LinearLayout back_btn ;
    @Bind(R.id.car_type_layout_in_tyre_type_activity) LinearLayout car ;
    @Bind(R.id.two_wheeler_type_layout_in_tyre_type_activity) LinearLayout twoweeler ;
    @Bind(R.id.lcv_type_layout_in_tyre_type_activity) LinearLayout lcv ;
    @Bind(R.id.hcv_type_layout_in_tyre_type_activity) LinearLayout hcv ;

    public static Activity activity ;

    ////////// databses
    TubeManager tum ;
    ArrayList<String> vehicle_arr = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_activity_tube_stockin);
        ButterKnife.bind(this);
        activity = this ;
        tum = new TubeManager(VehicleActivityTubeStockin.this );
        vehicle_arr = tum.getAllVehicleType();
        Toaster.generatemessage(VehicleActivityTubeStockin.this , ""+vehicle_arr);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivity(StockInConstants.CAR_KEY);
            }
        });


        twoweeler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivity(StockInConstants.TWO_WHEELER_KEY);
            }
        });




        lcv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivity(StockInConstants.LCV_KEY);
            }
        });


        hcv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivity(StockInConstants.HCV_KEY);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        TubeSizeListActivityStockInModule.clearAllTempConstants();
    }

    private void changeActivity(String Key) {
        if(ConstantClass.whichActivity.equals("TubeSizeListActivityStockInModule")){
            TubeSizeListActivityStockInModule.temp_vehicle = Key;
            TubeSizeListActivityStockInModule.temp_brand = "";
            TubeSizeListActivityStockInModule.temp_rimsize = "";
            finish();
        }else  {
            StockInConstants.clearAllMainConstants();
            TyreListStockInModule.clearAllTempConstants();
            startActivity(new Intent(VehicleActivityTubeStockin.this, BrandsNameActivityTubeInStock.class));
            StockInConstants.TubeVehicle = Key;
        }
    }


}
