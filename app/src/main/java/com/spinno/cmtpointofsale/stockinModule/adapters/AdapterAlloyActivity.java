package com.spinno.cmtpointofsale.stockinModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.AlloysTable;
import com.spinno.cmtpointofsale.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import databases.manager.StockInMiniBucketManager;
import de.greenrobot.event.EventBus;
import imageLoading.ImageLoader;
import logger.Logger;

/**
 * Created by spinnosolutions on 8/28/15.
 */
public class AdapterAlloyActivity  extends BaseAdapter {

    Context con ;
    List<AlloysTable> data ;
    LayoutInflater inflater ;
    StockInMiniBucketManager simbam ;

    ImageLoader imagerloader ;


    public AdapterAlloyActivity(Context con  , List<AlloysTable> data ){
       this.con = con ;
        this.data = data ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imagerloader = new ImageLoader(con.getApplicationContext());
        simbam = new StockInMiniBucketManager(con);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_alloy_stockin, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.brand_txt.setText(""+data.get(position).getAlloyBrand());
        holder.model_no_txt.setText(""+data.get(position).getAlloyModelNo());
        holder.color_txt.setText(""+data.get(position).getAlloyColor());
        holder.diameter_txt.setText(""+data.get(position).getAlloyDiameter());
        holder.rim_size_txt.setText(""+data.get(position).getAlloyWidth());
        holder.bolts_txt.setText(""+data.get(position).getAlloyBolts());
        holder.pcd_txt.setText(""+data.get(position).getAlloyPcd());
        holder.center_bore_txt.setText(""+data.get(position).getAlloyCenterBore());
        holder.negetiveoffset_txt.setText(""+data.get(position).getAlloyOffsetNegetive());
        holder.positive_offset_txt.setText(""+data.get(position).getAlloyOffsetPositive());
        holder.dealerPrice_txt.setText("Rs. "+data.get(position).getAlloyDealerPrice()+"/-");
        Logger.v("Image url "+data.get(position).getAlloyImage());
      //  imagerloader.DisplayImage(data.get(position).getAlloyImage() , holder.alloy_image);





////////////////////////    PLUS BUTTON
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value = Integer.parseInt(holder.noOfUnits.getText().toString());
                if(value >=  0 ){
                    int value_plus = value + 1 ;
                    holder.noOfUnits.setText(""+value_plus);

                    simbam.addToAlloyMiniBucket(data.get(position).AlloyId,data.get(position).AlloyBrand,data.get(position).getAlloyModelNo(),data.get(position).getAlloyColor(),data.get(position).getAlloyDiameter(),data.get(position).getAlloyWidth(),data.get(position).getAlloyBolts(),data.get(position).AlloyPcd,data.get(position).getAlloyCenterBore(),data.get(position).getAlloyOffsetNegetive(),data.get(position).getAlloyOffsetPositive(),data.get(position).getAlloyImage(),data.get(position).getAlloyDealerPrice(),""+value_plus , "0000");
                }
            }
        });








///////////////////////////////////////   MINUS BUTTON
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value = Integer.parseInt(holder.noOfUnits.getText().toString());
                if(value >1){
                    int value_minus = value - 1 ;
                    holder.noOfUnits.setText(""+value_minus);

                    simbam.addToAlloyMiniBucket(data.get(position).AlloyId,data.get(position).AlloyBrand,data.get(position).getAlloyModelNo(),data.get(position).getAlloyColor(),data.get(position).getAlloyDiameter(),data.get(position).getAlloyWidth(),data.get(position).getAlloyBolts(),data.get(position).AlloyPcd,data.get(position).getAlloyCenterBore(),data.get(position).getAlloyOffsetNegetive(),data.get(position).getAlloyOffsetPositive(),data.get(position).getAlloyImage(),data.get(position).getAlloyDealerPrice(),""+value_minus , "0000");

                }else if(value == 1){
                    /// dlete the item
                    int value_minus = value - 1 ;
                    holder.noOfUnits.setText(""+value_minus);
                    simbam.removeFromAlloyMiniBucket(data.get(position).getAlloyId());
                }else if(value <1){
                    ///  do nothing
                }
            }
        });

        return view;
    }

    static class ViewHolder {
        @Bind(R.id.brand_txt)TextView brand_txt;
        @Bind(R.id.model_no_txt)TextView model_no_txt;
        @Bind(R.id.color_txt)TextView color_txt;
        @Bind(R.id.diameter_txt)TextView diameter_txt;
        @Bind(R.id.rim_size_txt)TextView rim_size_txt;
        @Bind(R.id.bolts_txt)TextView bolts_txt;
        @Bind(R.id.pcd_txt)TextView pcd_txt;
        @Bind(R.id.center_bore_txt)TextView center_bore_txt;
        @Bind(R.id.negetiveoffset_txt)TextView negetiveoffset_txt;
        @Bind(R.id.positive_offset_txt)TextView positive_offset_txt;
        @Bind(R.id.dealerPrice_txt)TextView dealerPrice_txt;
        @Bind(R.id.alloy_image)ImageView alloy_image;


        @Bind(R.id.minus)ImageView minus;
        @Bind(R.id.plus)ImageView plus;
        @Bind(R.id.noOfUnits)TextView noOfUnits;




        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
