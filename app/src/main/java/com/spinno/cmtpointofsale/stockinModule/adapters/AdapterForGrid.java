package com.spinno.cmtpointofsale.stockinModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;

import java.util.ArrayList;

/**
 * Created by spinnosolutions on 8/18/15.
 */
public class AdapterForGrid extends BaseAdapter {

  Context con ;
    ArrayList<String> data ;
    LayoutInflater inflater ;

    public AdapterForGrid(Context context , ArrayList<String> data){
     this.con = context;
     this.data = data ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.item_grid_tyre_size_for_stockin, viewGroup, false);
        final TextView tyresize = (TextView) view.findViewById(R.id.company_name_item_of_grid_view);
        tyresize.setText(data.get(i));
           return  view ;
    }
}
