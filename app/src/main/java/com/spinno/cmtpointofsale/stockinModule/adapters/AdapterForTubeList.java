package com.spinno.cmtpointofsale.stockinModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.TubesTable;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import databases.manager.StockInMiniBucketManager;

/**
 * Created by spinnosolutions on 8/24/15.
 */
public class AdapterForTubeList  extends BaseAdapter {


    Context con  ;
    List<TubesTable> data ;
    LayoutInflater inflater ;
    StockInMiniBucketManager simbam ;

    public AdapterForTubeList(Context con  , List<TubesTable> data ){
        this.con = con  ;
        this.data = data ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        simbam = new StockInMiniBucketManager(con);
    }



    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_tube_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.tubesize_txt.setText(""+data.get(position).getTubeSize());
        holder.brand_txt.setText(""+data.get(position).getTubeBrand());
        holder.rimsize_txt.setText(""+data.get(position).getTubeRimSize());
        holder.vehicletype_txt.setText(""+data.get(position).getTubeVehicleType());
        holder.dealerprice_txt.setText("Rs. "+data.get(position).getTubePrice()+"/-");



//////////////////// minus button
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value_edt  =   Integer.parseInt(holder.noOfUnits.getText().toString());
                if(value_edt>1){
                    holder.noOfUnits.setText(""+(value_edt - 1));
                    if(StockInConstants.ItemCategory.equals(StockInConstants.Tube_KEY)){
                        simbam.addToTubeMiniBucket(data.get(position).TubeId
                                ,data.get(position).getTubeVehicleType()
                                ,data.get(position).getTubeBrand()
                                ,data.get(position).getTubeRimSize()
                                ,data.get(position).getTubeSize()
                                ,data.get(position).getTubePrice()
                                ,""+holder.noOfUnits.getText().toString()
                                ,""+0000);
                    }else {
                        Toaster.generatemessage(con , "Your not using tyre_Kew of StockinConstants");
                    }
                }else if(value_edt == 1 ){
                    /// delete value from databse
                    holder.noOfUnits.setText(""+(value_edt - 1));
                    simbam.removeFromTubeMiniBucket(data.get(position).getTubeId());
                }else if(value_edt<1){
                    //////   do nothing
                }
            }
        });


//////////////////// Plus button
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value_edt  = Integer.parseInt(holder.noOfUnits.getText().toString());
                if(value_edt>=0){
                    holder.noOfUnits.setText(""+(value_edt + 1));
                    if(StockInConstants.ItemCategory.equals(StockInConstants.Tube_KEY)){
                        simbam.addToTubeMiniBucket(data.get(position).TubeId
                                ,data.get(position).getTubeVehicleType()
                                ,data.get(position).getTubeBrand()
                                ,data.get(position).getTubeRimSize()
                                ,data.get(position).getTubeSize()
                                ,data.get(position).getTubePrice()
                                ,""+holder.noOfUnits.getText().toString()
                                ,""+0000);
                    }else {
                        Toaster.generatemessage(con , "Your not using tyre_Kew of StockinConstants");
                    }
                }/////  else button will no need to work
            }
        });


        return view;
    }

    static class ViewHolder {

        @Bind(R.id.minus_button_in_item_for_place_order_list) ImageView minus;
        @Bind(R.id.plus_button__in_item_for_place_order_list) ImageView plus;
        @Bind(R.id.no_of_unit_in_item_for_place_order_list)TextView noOfUnits;


        @Bind(R.id.tubesize)TextView tubesize_txt;
        @Bind(R.id.brand)TextView brand_txt;
        @Bind(R.id.rimsize)TextView rimsize_txt;
        @Bind(R.id.vehicletype)TextView vehicletype_txt;
        @Bind(R.id.dealerprice)TextView dealerprice_txt;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
