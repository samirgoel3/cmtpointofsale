package com.spinno.cmtpointofsale.stockinModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.SKUTable;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import databases.manager.StockInMiniBucketManager;

/**
 * Created by spinnosolutions on 8/21/15.
 */
public class AdapterForTyreList extends BaseAdapter {


    Context con ;
    LayoutInflater inflater ;
    List<SKUTable> data ;
    StockInMiniBucketManager simbam ;

    public AdapterForTyreList(Context con, List<SKUTable> data){
        this.con = con ;
        this.data = data ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        simbam = new StockInMiniBucketManager(con);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_stockin, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }



        holder.brand_txt.setText(""+data.get(position).TyreBrandName);
        holder.pattern_txt.setText(""+data.get(position).getTyrePattern());
        holder.type_txt.setText(""+data.get(position).getTyreType());
        holder.tyresizeWithRim_txt.setText(""+data.get(position).getTyreSize());
        holder.speedrating_txt.setText(""+data.get(position).getTyreSpeedRating());
        holder.dealerprice_txt.setText("Rs. "+data.get(position).getTyreDealerPrice()+"/-");

/////////////////minus button
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value_edt  = Integer.parseInt(holder.noOfUnits.getText().toString());
                if(value_edt>1){
                    holder.noOfUnits.setText(""+(value_edt - 1));
                    if(StockInConstants.ItemCategory.equals(StockInConstants.Tyre_KEY)){
                        simbam.addToSKUMiniBucket(data.get(position).getTyreId()
                                ,data.get(position).getTyreVehicleType()
                                ,data.get(position).getTyreBrandName()
                                ,data.get(position).getTyreRimSize()
                                ,data.get(position).getTyreSize()
                                ,data.get(position).getTyrePattern()
                                ,data.get(position).getTyreSpeedRating()
                                ,data.get(position).getTyreType()
                                ,data.get(position).getTyreDealerPrice(),holder.noOfUnits.getText().toString(), ""+0000);
                    }else {
                        Toaster.generatemessage(con , "Your not using tyre_Kew of StockinConstants");
                    }
                }else if(value_edt == 1 ){
                 /// delete value from databse
                    holder.noOfUnits.setText(""+(value_edt - 1));
                    simbam.removeFromSKUMiniBucket(data.get(position).getTyreId());
                }else if(value_edt<1){
                  //////   do nothing
                }
            }
        });



/////////////////plus button
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value_edt  = Integer.parseInt(holder.noOfUnits.getText().toString());
                if(value_edt>=0){
                    holder.noOfUnits.setText(""+(value_edt + 1));
                    if(StockInConstants.ItemCategory.equals(StockInConstants.Tyre_KEY)){
                        simbam.addToSKUMiniBucket(data.get(position).getTyreId()
                                ,data.get(position).getTyreVehicleType()
                                ,data.get(position).getTyreBrandName()
                                ,data.get(position).getTyreRimSize()
                                ,data.get(position).getTyreSize()
                                ,data.get(position).getTyrePattern()
                                ,data.get(position).getTyreSpeedRating()
                                ,data.get(position).getTyreType()
                                , data.get(position).getTyreDealerPrice(),""+holder.noOfUnits.getText().toString(), ""+0000);
                    }else {
                        Toaster.generatemessage(con , "Your not using tyre_Kew of StockinConstants");
                    }
                }/////  else button will no need to work
            }
        });

        return view;
    }

    static class ViewHolder {
        @Bind(R.id.minus_button_in_item_for_place_order_list) ImageView minus;
        @Bind(R.id.plus_button__in_item_for_place_order_list) ImageView plus;
        @Bind(R.id.no_of_unit_in_item_for_place_order_list)TextView noOfUnits;


        @Bind(R.id.brand)TextView brand_txt;
        @Bind(R.id.pattern)TextView pattern_txt;
        @Bind(R.id.type)TextView type_txt;
        @Bind(R.id.tyresizewithrimsize)TextView tyresizeWithRim_txt;
        @Bind(R.id.speedrating)TextView speedrating_txt;
        @Bind(R.id.dealerprice)TextView dealerprice_txt;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
