package com.spinno.cmtpointofsale.stockinModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.OthersTable;
import com.spinno.cmtpointofsale.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import databases.manager.StockInMiniBucketManager;

/**
 * Created by spinnosolutions on 8/27/15.
 */
public class AdapterOtherList extends BaseAdapter {

    Context con ;
    List<OthersTable>  data ;
    LayoutInflater inflater ;
    StockInMiniBucketManager simbam ;

    public AdapterOtherList(Context con, List<OthersTable> data){
         this.con =  con ;
         this.data = data ;
        simbam = new StockInMiniBucketManager(con);
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_for_other_stockin, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }


        holder.brand_txt.setText(""+data.get(position).getOtherBrand());
        holder.product_name_txt.setText(""+data.get(position).OtherProductName);
        holder.dealerprice_txt.setText("Rs."+data.get(position).getOtherDealerPrice()+" /-");
        holder.product_description_txt.setText(""+data.get(position).getOtherProductDescription());



///////////////////////////////// plus working
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value = Integer.parseInt(holder.no_of_unit.getText().toString());
                if(value >=  0 ){
                       int value_plus = value + 1 ;
                    holder.no_of_unit.setText(""+value_plus);
                    simbam.addToItemsMiniBucket(data.get(position).getOtherId()
                            , data.get(position).getOtherCategory()
                            , data.get(position).getOtherBrand()
                            , data.get(position).getOtherProductName()
                            , data.get(position).getOtherDealerPrice()
                            , data.get(position).OtherProductDescription
                            , data.get(position).OtherTaxation
                            , "" + value_plus
                            , "0000");
                }

            }
        });



        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value = Integer.parseInt(holder.no_of_unit.getText().toString());
                if(value >1){
                    int value_minus = value - 1 ;
                    holder.no_of_unit.setText(""+value_minus);

                    simbam.addToItemsMiniBucket(data.get(position).getOtherId()
                            , data.get(position).getOtherCategory()
                            , data.get(position).getOtherBrand()
                            , data.get(position).getOtherProductName()
                            , data.get(position).getOtherDealerPrice()
                            , data.get(position).OtherProductDescription
                            , data.get(position).OtherTaxation
                            , "" + value_minus
                            , "0000");

                }else if(value == 1){
                    /// dlete the item
                    int value_minus = value - 1 ;
                    holder.no_of_unit.setText(""+value_minus);
                    simbam.removeFromothersminiBucket(data.get(position).getOtherId());
                }else if(value <1){
                     ///  do nothing
                }
            }
        });

        return view;
    }

    static class ViewHolder {


        @Bind(R.id.brand) TextView brand_txt;
        @Bind(R.id.product_name_txt) TextView product_name_txt;
        @Bind(R.id.dealerprice) TextView dealerprice_txt;
        @Bind(R.id.product_description_txt) TextView product_description_txt;
        @Bind(R.id.minus_button_in_item_for_place_order_list) ImageView minus;
        @Bind(R.id.plus_button__in_item_for_place_order_list) ImageView plus;
        @Bind(R.id.no_of_unit_in_item_for_place_order_list) TextView no_of_unit;




        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
