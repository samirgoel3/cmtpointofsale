package com.spinno.cmtpointofsale.stockinModule.tyreFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.SKUManager;

public class BrandsNameActivityInStock extends Activity {


    @Bind(R.id.gridview_in_company_names_activity)GridView grid_view;
    @Bind(R.id.back_laupit_in_company_name_activity_new)LinearLayout back_btn;
    @Bind(R.id.vehicle_type_image_in_companyname_activity)ImageView vehicletype;



    ///////////////// Table Explorer
    SKUManager skumanager ;
    ArrayList<String> brands_arr = new ArrayList<>();





    public static Activity activity ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_names_activity_new);
        ButterKnife.bind(this);
        activity = this ;
        skumanager = new SKUManager(BrandsNameActivityInStock.this);



        setviewsaccordingtopreviouselection();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (ConstantClass.whichActivity.equals("TyreListStockInModule")) {
                    ///////////// fill in the temp variables
                    if(TyreListStockInModule.temp_vehicle.equals("")){
                        TyreListStockInModule.temp_vehicle = StockInConstants.VehicleType ;
                    }
                    TyreListStockInModule.temp_brand = brands_arr.get(i);
                    TyreListStockInModule.temp_rimsize = "";
                    TyreListStockInModule.tem_tyreSize = "";
                    finish();
                } else {
                    //////   fill in the Main variables
                    StockInConstants.BrandName = brands_arr.get(i);
                    startActivity(new Intent(BrandsNameActivityInStock.this, RimSizeActivityStockiNModule.class));
                }
            }
        });

    }

    private void setviewsaccordingtopreviouselection() {
        loadGrid();
        if( StockInConstants.VehicleType.equals(StockInConstants.CAR_KEY)){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if ( StockInConstants.VehicleType.equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if ( StockInConstants.VehicleType.equals(StockInConstants.HCV_KEY)){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if ( StockInConstants.VehicleType.equals(StockInConstants.LCV_KEY)){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
    }

    private void loadGrid() {
        if(!TyreListStockInModule.temp_vehicle.equals("")){
           /// load grid according to temp constants
            brands_arr = skumanager.GetAllBrandsByVehicle(TyreListStockInModule.temp_vehicle);
            grid_view.setAdapter(new AdapterForGrid(BrandsNameActivityInStock.this , brands_arr));
        }else {
            ////    load grid AT main constants
            brands_arr = skumanager.GetAllBrandsByVehicle(StockInConstants.VehicleType);
            grid_view.setAdapter(new AdapterForGrid(BrandsNameActivityInStock.this , brands_arr));
        }

    }

}
