package com.spinno.cmtpointofsale.stockinModule.tyreFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.SKUManager;

public class RimSizeActivityStockiNModule extends Activity {

    @Bind(R.id.gridview_in_rim_size_activity)GridView gdview  ;
    @Bind(R.id.vehicle_type_image_in_tyre_rim_activity)ImageView vehicletype;
    @Bind(R.id.company_name_tyre_rim_activity)TextView companyname_txt;

    public static Activity activity ;

    ///////////////// Table Explorer
    SKUManager skumanager ;
    ArrayList<String> rim_arr = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rim_size_activity_stocki_nmodule);
        ButterKnife.bind(this);
        activity = this ;
        skumanager = new SKUManager(RimSizeActivityStockiNModule.this);

        setviewsaccordingtopreviouselection();

        gdview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(ConstantClass.whichActivity.equals("TyreListStockInModule")){
                  /////  fill into temp variables
                    if(TyreListStockInModule.temp_vehicle.equals("")){
                        TyreListStockInModule.temp_vehicle = StockInConstants.VehicleType;
                    }if(TyreListStockInModule.temp_brand.equals("")){
                        TyreListStockInModule.temp_brand = StockInConstants.BrandName ;
                    }
                    TyreListStockInModule.temp_rimsize = rim_arr.get(i);
                    TyreListStockInModule.tem_tyreSize =  "";
                        finish();
                }else {
                    ///////   fill into main variables
                    StockInConstants.RimSize = rim_arr.get(i);
                    startActivity(new Intent(RimSizeActivityStockiNModule.this ,TyreSizeActivityStockInModule.class));
                }
            }
        });
    }



    private void setviewsaccordingtopreviouselection() {
        loadGrid();
        if(StockInConstants.VehicleType.equals(StockInConstants.CAR_KEY)){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (StockInConstants.VehicleType.equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (StockInConstants.VehicleType.equals(StockInConstants.HCV_KEY)){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (StockInConstants.VehicleType.equals(StockInConstants.LCV_KEY)){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
        companyname_txt.setText(StockInConstants.BrandName);
    }

    private void loadGrid() {
        if(!TyreListStockInModule.temp_brand.equals("") ){
             //////   load grid according to the temp variables
            rim_arr =  skumanager.getAllRimByVehicleBrand(TyreListStockInModule.temp_vehicle, TyreListStockInModule.temp_brand);
            gdview.setAdapter(new AdapterForGrid(RimSizeActivityStockiNModule.this, rim_arr));
        }else {
            ////////   load grid according to the main  variables
            rim_arr =  skumanager.getAllRimByVehicleBrand(StockInConstants.VehicleType , StockInConstants.BrandName);
            gdview.setAdapter(new AdapterForGrid(RimSizeActivityStockiNModule.this, rim_arr));
        }
    }
}
