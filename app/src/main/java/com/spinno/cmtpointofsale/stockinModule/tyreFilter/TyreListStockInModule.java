package com.spinno.cmtpointofsale.stockinModule.tyreFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.SKUTable;
import com.spinno.cmtpointofsale.stockinModule.Bucket.BucketActivity;
import com.spinno.cmtpointofsale.stockinModule.StockInEvents.TotalItemsInMiniBucketsEvent;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForTyreList;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import checkers.Toaster;
import contants.ConstantClass;
import databases.manager.SKUManager;
import databases.manager.StockInMiniBucketManager;
import de.greenrobot.event.EventBus;

public class TyreListStockInModule extends Activity {


/////////////////// action bar attribute
    @Bind(R.id.company_name_on_action_bar_of_tyre_list_in_stock)TextView company_name_on_actiopbar;
    @Bind(R.id.rim_size_on_action_bar_of_tyre_list_in_stock)TextView rim_size_on_action_bar;
    @Bind(R.id.tyre_size_on_action_bar_of_tyre_list_in_stock)TextView tyre_size_on_action_bar;
    @Bind(R.id.vehicle_image_on_action_bar_of_tyre_list_in_stock)ImageView vehicle_type_img_on_action_bar;
    @Bind(R.id.openfilter_in_tyreListInStock)LinearLayout openfilter_btn;



    @Bind(R.id.listview)ListView tyre_list ;
    @Bind(R.id.loaderspinningwheeltyre_list_in_stock)LinearLayout loaderview ;



///////////////////////// filter
    @Bind(R.id.brand_linear_layout)LinearLayout brandFilter;
    @Bind(R.id.brand_name_in_filter)TextView brandnameFilter_txt;
    @Bind(R.id.rimsize_linear_layout)LinearLayout rim_SizeFilter;
    @Bind(R.id.rim_size_filter_txt)TextView rimsizeFilter_txt;
    @Bind(R.id.tyre_size_linearLayout)LinearLayout tyreSizeFilter;
    @Bind(R.id.tyre_size_txt)TextView tyre_sizeFilter_txt;
    @Bind(R.id.apply_filter)LinearLayout apply ;
    @Bind(R.id.vehicle_type_layout)LinearLayout vehicleFilter ;
    @Bind(R.id.vehicle_type)TextView vehicletypeFilter_txt;
    @Bind(R.id.vehicle_image)ImageView vehicleImageFilter_img   ;



////////////////////////Bucket attributes
    @Bind(R.id.cart_layout_tyreList_in_stock)View  Bucket;
    @Bind(R.id.tyres_bucket)TextView  tyre_Bucket;
    @Bind(R.id.tubes_bucket)TextView  tube_Bucket;
    @Bind(R.id.alloys_bucket)TextView  alloy_Bucket;
    @Bind(R.id.others_bucket)TextView  others_Bucket;
    @Bind(R.id.next)LinearLayout  next_Bucket;
        ////////databse variables
         SKUManager  skum  ;


    ////////////////////   temporary variable
    public static String temp_vehicle = "";
    public static String temp_brand = "";
    public static String temp_rimsize = "";
    public static String tem_tyreSize = "";


    private MenuDrawer mDrawer;

    private EventBus bus = EventBus.getDefault();
    StockInMiniBucketManager simbum ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawer = MenuDrawer.attach(this , Position.RIGHT);
        mDrawer.setContentView(R.layout.activity_tyre_list_stock_in_module);
        mDrawer.setMenuView(R.layout.drawer_layout_tyreliststockinmodule);
        mDrawer.setDropShadowColor(R.color.client_colour_grey_dark);
        ButterKnife.bind(this);
        skum = new SKUManager(TyreListStockInModule.this);
        ConstantClass.whichActivity = "TyreListStockInModule";
        simbum = new StockInMiniBucketManager(TyreListStockInModule.this);


        // Register as a subscriber
        bus.register(this);


        VehicleTypeActivityStockinModule.activity.finish();
        BrandsNameActivityInStock.activity.finish();
        RimSizeActivityStockiNModule.activity.finish();
        TyreSizeActivityStockInModule.activity.finish();

        loadtyreList();


        openfilter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.openMenu();
            }
        });



        next_Bucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TyreListStockInModule.this ,BucketActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });


        vehicleFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TyreListStockInModule.this ,VehicleTypeActivityStockinModule.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });


        brandFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TyreListStockInModule.this ,BrandsNameActivityInStock.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });



        rim_SizeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brandnameFilter_txt.getText().toString().equals("Click to select")){
                    Toaster.generatemessage(TyreListStockInModule.this , "Please select Brand First");
                }else {
                    startActivity(new Intent(TyreListStockInModule.this ,RimSizeActivityStockiNModule.class));
                    overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
                }
            }
        });

        tyreSizeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rimsizeFilter_txt.getText().toString().equals("Click to select")){
                    Toaster.generatemessage(TyreListStockInModule.this , "Please Select Rim Size First");
                }else {
                    startActivity(new Intent(TyreListStockInModule.this ,TyreSizeActivityStockInModule.class));
                    overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
                }

            }
        });


        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //////  when ever click on apply button check all the data in tem variable  , and if available in all tem varible load it in list , replace all main variable by tem variables and empty tem variable
                mDrawer.closeMenu();

                if(!temp_vehicle.equals("")  && !temp_brand.equals("") && !temp_rimsize.equals("")  && !tem_tyreSize.equals("")) {
                    if (!TyreListStockInModule.temp_vehicle.equals("")) {
                        StockInConstants.VehicleType = TyreListStockInModule.temp_vehicle;
                    }
                    if (!TyreListStockInModule.temp_brand.equals("")) {
                        StockInConstants.BrandName = TyreListStockInModule.temp_brand;
                    }
                    if (!TyreListStockInModule.temp_rimsize.equals("")) {
                        StockInConstants.RimSize = TyreListStockInModule.temp_rimsize;
                    }
                    if (!TyreListStockInModule.tem_tyreSize.equals("")) {
                        StockInConstants.TyreSize = TyreListStockInModule.tem_tyreSize;
                    }
                    Toaster.generatemessage(TyreListStockInModule.this , "Main vehicle "+StockInConstants.VehicleType+" Main Brand  "+StockInConstants.BrandName + " Main Rim "+StockInConstants.RimSize+" Main TyreSize "+StockInConstants.TyreSize);
                    loadtyreList();
                    setViewsAccordingToPreciousSelection();
                }else {
                    Toaster.generatemessage(TyreListStockInModule.this , "Please create filter first");
                }
            }
        });




        mDrawer.setOnDrawerStateChangeListener(new MenuDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (oldState == 0 && newState == 1) {
                    //////    when ever thje menu is open load data from main constants only
                    setViewsAccordingToPreciousSelection();
                    TyreListStockInModule.clearAllTempConstants();
                } else if (oldState == 8 && newState == 4) {
                    // runs when close
                    setViewsAccordingToPreciousSelection();
                }
            }
        });
    }

    private void loadtyreList() {
        //Bucket.setVisibility(View.GONE);
        loaderview.setVisibility(View.VISIBLE);
        tyre_list.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loaderview.setVisibility(View.GONE);
                tyre_list.setVisibility(View.VISIBLE);
                List<SKUTable> data  = skum.getAllTyresByVehicleCompanyRimSizeTyresize(StockInConstants.VehicleType ,StockInConstants.BrandName , StockInConstants.RimSize , StockInConstants.TyreSize);
                Toaster.generatemessage(TyreListStockInModule.this , ""+data.size());
                tyre_list.setAdapter(new AdapterForTyreList(TyreListStockInModule.this , data));
            }
        }, 1500);
    }






    public void onEvent(TotalItemsInMiniBucketsEvent event){
        tyre_Bucket.setText(event.getDatatyre());
        tube_Bucket.setText(event.getDatatube());
        others_Bucket.setText(event.getDataOthers());
        alloy_Bucket.setText(event.getDataAlloys());
    }



    @Override
    protected void onResume() {
        super.onResume();
        StockInConstants.ItemCategory = StockInConstants.Tyre_KEY ;
        setViewsAccordingToPreciousSelection();
        simbum.showDataOverBucket();
    }


    @Override
    protected void onDestroy() {
        // Unregister
        bus.unregister(this);
        super.onDestroy();
    }



    private void setViewsAccordingToPreciousSelection() {
        company_name_on_actiopbar.setText(StockInConstants.BrandName);
        rim_size_on_action_bar.setText(StockInConstants.RimSize + " inches");
        tyre_size_on_action_bar.setText(StockInConstants.TyreSize);
        setupDrawerLayoutViewsAccordingly();
        if(StockInConstants.VehicleType.equals(StockInConstants.CAR_KEY)){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.car_white_icon);
        }else  if (StockInConstants.VehicleType.equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (StockInConstants.VehicleType.equals(StockInConstants.HCV_KEY)){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (StockInConstants.VehicleType.equals(StockInConstants.LCV_KEY)){
            vehicle_type_img_on_action_bar.setImageResource(R.drawable.truck_icon_white);
        }
    }



    public void setupDrawerLayoutViewsAccordingly(){
        vehicletypeFilter_txt.setText("" + StockInConstants.VehicleType);
        brandnameFilter_txt.setText(StockInConstants.BrandName);
        rimsizeFilter_txt.setText(StockInConstants.RimSize);
        tyre_sizeFilter_txt.setText(StockInConstants.TyreSize);


        if(!TyreListStockInModule.temp_vehicle.equals("")){
            /// make downer rim and tyre size unclickable and set all downer text to click to select
            vehicletypeFilter_txt.setText(""+TyreListStockInModule.temp_vehicle);
            brandnameFilter_txt.setText("Click to select");
            rimsizeFilter_txt.setText("Click to select");
            tyre_sizeFilter_txt.setText("Click to select");
            if(!TyreListStockInModule.temp_brand.equals("")){
                brandnameFilter_txt.setText(TyreListStockInModule.temp_brand);
                //////check rim in temp is empty or not
                if(!TyreListStockInModule.temp_rimsize.equals("")){
                    rimsizeFilter_txt.setText(TyreListStockInModule.temp_rimsize);
                    /////////check tyre size is empty or not
                    if(!TyreListStockInModule.tem_tyreSize.equals("")){
                        tyre_sizeFilter_txt.setText(TyreListStockInModule.tem_tyreSize);
                    }else {
                        tyre_sizeFilter_txt.setText("Click to select");
                    }
                }else {
                    rimsizeFilter_txt.setText("Click to select");
                    tyre_sizeFilter_txt.setText("Click to select");
                }
            }else {
                /////////   do nothing for brand
            }
        }else {
            ////// do nothing  for vehicle
        }
        setVehicleImageAccrodingly();
    }

    private void setVehicleImageAccrodingly() {
        if(vehicletypeFilter_txt.getText().toString().equals(StockInConstants.CAR_KEY)){
            vehicleImageFilter_img.setImageResource(R.drawable.car_icon_grey);
        }if(vehicletypeFilter_txt.getText().toString().equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicleImageFilter_img.setImageResource(R.drawable.scooter_icon_grey);
        }if(vehicletypeFilter_txt.getText().toString().equals(StockInConstants.LCV_KEY)){
            vehicleImageFilter_img.setImageResource(R.drawable.truck_icon_grey);
        }if(vehicletypeFilter_txt.getText().toString().equals(StockInConstants.HCV_KEY)){
            vehicleImageFilter_img.setImageResource(R.drawable.hcv_icon_grey);
        }
    }

    public static void clearAllTempConstants(){
        temp_vehicle = "";
        temp_brand = "";
        temp_rimsize = "";
        tem_tyreSize = "";
    }
}
