package com.spinno.cmtpointofsale.stockinModule.tyreFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.adapters.AdapterForGrid;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.SKUManager;

public class TyreSizeActivityStockInModule extends Activity {

    @Bind(R.id.gridview_in_tyre_size_activity)GridView gdview;
    @Bind(R.id.back_laupit_in_tyre_size_activity)LinearLayout back_btn;
    @Bind(R.id.vehicle_type_image_in_tyre_size_activity)ImageView vehicletype;
    @Bind(R.id.company_name_in_tyre_size_activity)TextView companyname;
    @Bind(R.id.rim_size_in_tyre_size_activity)TextView rimsize_txt;


    public static Activity activity ;

    ///////////////// Table Explorer
    SKUManager skumanager ;
    ArrayList<String> tyresize_arr = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_size_activity_stock_in_module);
        ButterKnife.bind(this);
        activity = this ;
        skumanager = new SKUManager(TyreSizeActivityStockInModule.this);
        setviewsaccordingtopreviouselection();
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




        gdview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(ConstantClass.whichActivity.equals("TyreListStockInModule")){
                    if(TyreListStockInModule.temp_vehicle.equals("")){
                        TyreListStockInModule.temp_vehicle = StockInConstants.VehicleType;
                    }if(TyreListStockInModule.temp_brand.equals("")){
                        TyreListStockInModule.temp_brand = StockInConstants.BrandName ;
                    }if(TyreListStockInModule.temp_rimsize.equals("")){
                        TyreListStockInModule.temp_rimsize = StockInConstants.RimSize ;
                    }
                    TyreListStockInModule.tem_tyreSize = tyresize_arr.get(i);
                        finish();
                }else {
                    StockInConstants.TyreSize = tyresize_arr.get(i);
                    startActivity(new Intent(TyreSizeActivityStockInModule.this , TyreListStockInModule.class));
                }
            }
        });
    }

    private void setviewsaccordingtopreviouselection() {
        companyname.setText(StockInConstants.BrandName);
        rimsize_txt.setText(StockInConstants.RimSize);
        loadGrid();
        if(StockInConstants.VehicleType.equals(StockInConstants.CAR_KEY)){
            vehicletype.setImageResource(R.drawable.car_white_icon);
        }else  if (StockInConstants.VehicleType.equals(StockInConstants.TWO_WHEELER_KEY)){
            vehicletype.setImageResource(R.drawable.motorcycle_white_icon);
        }else if (StockInConstants.VehicleType.equals(StockInConstants.HCV_KEY)){
            vehicletype.setImageResource(R.drawable.heavy_weight_vehicle_icon);
        }else if (StockInConstants.VehicleType.equals(StockInConstants.LCV_KEY)){
            vehicletype.setImageResource(R.drawable.truck_icon_white);
        }
    }

    private void loadGrid() {
        if(!TyreListStockInModule.temp_rimsize.equals("")){
            /// load grid according to the temp variables
            tyresize_arr = skumanager.getAllTyreSizeByVehicleBrandRim(TyreListStockInModule.temp_vehicle,TyreListStockInModule.temp_brand , TyreListStockInModule.temp_rimsize);
            gdview.setAdapter(new AdapterForGrid(TyreSizeActivityStockInModule.this, tyresize_arr));
        }else {
            /////   load grid according to main variables
            tyresize_arr = skumanager.getAllTyreSizeByVehicleBrandRim(StockInConstants.VehicleType ,StockInConstants.BrandName , StockInConstants.RimSize);
            gdview.setAdapter(new AdapterForGrid(TyreSizeActivityStockInModule.this, tyresize_arr));
        }

    }
}
