package com.spinno.cmtpointofsale.stockinModule.tyreFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.spinno.cmtpointofsale.R;
import com.spinno.cmtpointofsale.stockinModule.StockinConstants.StockInConstants;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import contants.ConstantClass;
import databases.manager.SKUManager;

public class VehicleTypeActivityStockinModule extends Activity {



    @Bind(R.id.back_button) LinearLayout back_btn ;
    @Bind(R.id.car_type_layout_in_tyre_type_activity) LinearLayout car ;
    @Bind(R.id.two_wheeler_type_layout_in_tyre_type_activity) LinearLayout twoweeler ;
    @Bind(R.id.lcv_type_layout_in_tyre_type_activity) LinearLayout lcv ;
    @Bind(R.id.hcv_type_layout_in_tyre_type_activity) LinearLayout hcv ;

    public static Activity activity ;



    ///////////////// Table Explorer
    SKUManager skumanager ;

    ArrayList<String> vehicle_arr = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_type_activity_stockin_module);
        ButterKnife.bind(this);
        activity = this ;
        skumanager = new SKUManager(VehicleTypeActivityStockinModule.this);
        vehicle_arr = skumanager.getAllVehicleType();
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivity(StockInConstants.CAR_KEY);
            }
        });


        twoweeler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivity(StockInConstants.TWO_WHEELER_KEY);
            }
        });




        lcv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivity(StockInConstants.LCV_KEY);
            }
        });


        hcv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivity(StockInConstants.HCV_KEY);
            }
        });
    }

    private void changeActivity(String Key) {
        if(ConstantClass.whichActivity.equals("TyreListStockInModule")){
            TyreListStockInModule.temp_vehicle = Key;
            TyreListStockInModule.temp_brand = "";
            TyreListStockInModule.temp_rimsize = "";
            TyreListStockInModule.tem_tyreSize = "";
            finish();
        }else  {
            StockInConstants.clearAllMainConstants();
            TyreListStockInModule.clearAllTempConstants();
            startActivity(new Intent(VehicleTypeActivityStockinModule.this, BrandsNameActivityInStock.class));
            StockInConstants.VehicleType = Key;
        }
    }
}
