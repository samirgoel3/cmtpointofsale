package databases.databseutils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import java.util.List;

import com.spinno.cmtpointofsale.CartTable;

/**
 * Created by samir on 04/07/15.
 */
public class CheckCart {

    Context con ;
    boolean checkid = false ;

    @SuppressLint("LongLogTag")
    public boolean idalreadyExsistinCart(String id){

        try {
            List<CartTable> templist = CartTable.find(CartTable.class, " Tyre_Id = ?", id);
            Log.d("Size of templist ", "" + templist.size());
            if (templist.size() == 1) {
                checkid = true;
            } else {
                checkid = false;
            }

        } catch (Exception e) {
            Log.d("Product Id is not saved in Databse ", "Id is not saved in Databse ");
        }

        return checkid;

    }

}
