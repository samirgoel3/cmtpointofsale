package databases.databseutils;

import android.content.Context;
import android.util.Log;

import com.spinno.cmtpointofsale.FavourateTable;

import java.util.List;

/**
 * Created by samir on 31/07/15.
 */
public class CheckFavourate {

    Context con ;
    boolean checkid = false ;

    public boolean idalreadyExsistinFavourate(String id){

        try {
            List<FavourateTable> templist = FavourateTable.find(FavourateTable.class, " Tyre_Id = ?", id);
            Log.d("Size of templist ", "" + templist.size());
            if (templist.size() == 1) {
                checkid = true;
            } else {
                checkid = false;
            }

        } catch (Exception e) {
            Log.d("Product Id is not saved in Databse ", "Id is not saved in Databse ");
        }

        return checkid;

    }
}
