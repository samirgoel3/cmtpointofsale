package databases.databseutils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.spinno.cmtpointofsale.OthersTable;

import java.util.List;

/**
 * Created by spinnosolutions on 8/26/15.
 */
public class CheckOther {


    Context con ;
    boolean checkid = false ;

    @SuppressLint("LongLogTag")
    public boolean siIDAlreadyExsistInOtherTable(String id){

        try {
            List<OthersTable> templist = OthersTable.find(OthersTable.class, " Other_Id = ?", id);
            Log.d("Size of templist ", "" + templist.size());
            if (templist.size() == 1) {
                checkid = true;
            } else {
                checkid = false;
            }

        } catch (Exception e) {
            Log.d("Product Id is not saved in SKU Table ", "Id is not saved in SKU ");
        }

        return checkid;

    }


}
