package databases.databseutils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.spinno.cmtpointofsale.SKUTable;

import java.util.List;

/**
 * Created by spinnosolutions on 8/22/15.
 */
public class CheckSKU {

    Context con ;
    boolean checkid = false ;

    @SuppressLint("LongLogTag")
    public boolean siIDAlreadyExsistInSKUTable(String id){

        try {
            List<SKUTable> templist = SKUTable.find(SKUTable.class, " Tyre_Id = ?", id);
            Log.d("Size of templist ", "" + templist.size());
            if (templist.size() == 1) {
                checkid = true;
            } else {
                checkid = false;
            }

        } catch (Exception e) {
            Log.d("Product Id is not saved in SKU Table ", "Id is not saved in SKU ");
        }

        return checkid;

    }
}
