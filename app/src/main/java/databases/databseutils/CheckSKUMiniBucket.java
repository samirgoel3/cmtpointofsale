package databases.databseutils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.spinno.cmtpointofsale.SKUMiniBucket;

import java.util.List;

/**
 * Created by spinnosolutions on 8/25/15.
 */
public class CheckSKUMiniBucket {
    Context con ;
    boolean checkid = false ;

    @SuppressLint("LongLogTag")
    public boolean isTyreExsistInSKUMiniBucket(String id){

        try {
            List<SKUMiniBucket> templist = SKUMiniBucket.find(SKUMiniBucket.class, " Tyre_Id = ?", id);
            Log.d("Size of templist ", "" + templist.size());
            if (templist.size() == 1) {
                checkid = true;
            } else {
                checkid = false;
            }

        } catch (Exception e) {
            Log.d("Product Id is not saved in Stock Table ", "Id is not saved in Databse ");
        }

        return checkid;

    }
}
