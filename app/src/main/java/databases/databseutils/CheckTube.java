package databases.databseutils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.spinno.cmtpointofsale.TubesTable;

import java.util.List;

/**
 * Created by spinnosolutions on 8/24/15.
 */
public class CheckTube {

    Context con ;
    boolean checkid = false ;

    @SuppressLint("LongLogTag")
    public boolean siIDAlreadyExsistInTubeTable(String id){

        try {
            List<TubesTable> templist = TubesTable.find(TubesTable.class, " Tube_Id = ?", id);
            Log.d("Size of templist ", "" + templist.size());
            if (templist.size() == 1) {
                checkid = true;
            } else {
                checkid = false;
            }

        } catch (Exception e) {
            Log.d("Product Id is not saved in SKU Table ", "Id is not saved in SKU ");
        }

        return checkid;

    }

}
