package databases.databseutils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.spinno.cmtpointofsale.TubeMiniBucket;

import java.util.List;

/**
 * Created by spinnosolutions on 8/25/15.
 */
public class CheckTubeMiniBucket {
    Context con ;
    boolean checkid = false ;

    @SuppressLint("LongLogTag")
    public boolean isTubeExsistInTubeMiniBucket(String id){

        try {
            List<TubeMiniBucket> templist = TubeMiniBucket.find(TubeMiniBucket.class, " Tube_Id = ?", id);
            Log.d("Size of templist ", "" + templist.size());
            if (templist.size() == 1) {
                checkid = true;
            } else {
                checkid = false;
            }

        } catch (Exception e) {
            Log.d("Product Id is not saved in Stock Table ", "Id is not saved in Databse ");
        }

        return checkid;

    }
}
