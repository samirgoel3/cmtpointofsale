package databases.manager;

import android.content.Context;

import com.spinno.cmtpointofsale.AlloysTable;

import java.util.ArrayList;
import java.util.List;

import databases.databseutils.CheckAlloy;

/**
 * Created by spinnosolutions on 8/28/15.
 */
public class AlloyManager {


    public static AlloysTable alt ;
    public static Context con ;

    public AlloyManager(Context con ){
        this.con = con ;
    }




    public static  void addToAlloysTable(String AlloyId ,String AlloyBrand,String AlloyModelNo ,String AlloyColor ,String AlloyDiameter ,String AlloyWidth ,String AlloyBolts, String AlloyPcd ,String AlloyCenterBore ,String AlloyOffsetNegetive ,String AlloyOffsetPositive ,String AlloyImage ,String AlloyDealerPrice){

        if(new CheckAlloy().isAlloyAlresdyExsisteInAlloyTable(AlloyId)){
            ////change already saved details in database
            changeExsistingRowInAlloyTable(  AlloyId , AlloyBrand, AlloyModelNo , AlloyColor , AlloyDiameter , AlloyWidth , AlloyBolts,  AlloyPcd , AlloyCenterBore , AlloyOffsetNegetive , AlloyOffsetPositive , AlloyImage , AlloyDealerPrice);
        }else {
            /////  create new row in database
            createNewRowInAlloyTable(AlloyId, AlloyBrand, AlloyModelNo, AlloyColor, AlloyDiameter, AlloyWidth, AlloyBolts, AlloyPcd, AlloyCenterBore, AlloyOffsetNegetive , AlloyOffsetPositive , AlloyImage , AlloyDealerPrice);
        }

    }

    private static void createNewRowInAlloyTable(String AlloyId ,String AlloyBrand,String AlloyModelNo ,String AlloyColor ,String AlloyDiameter ,String AlloyWidth ,String AlloyBolts, String AlloyPcd ,String AlloyCenterBore ,String AlloyOffsetNegetive ,String AlloyOffsetPositive ,String AlloyImage ,String AlloyDealerPrice ) {
        new AlloysTable( AlloyId , AlloyBrand, AlloyModelNo , AlloyColor , AlloyDiameter , AlloyWidth , AlloyBolts,  AlloyPcd , AlloyCenterBore , AlloyOffsetNegetive , AlloyOffsetPositive , AlloyImage , AlloyDealerPrice).save();
    }


    public static void changeExsistingRowInAlloyTable(String AlloyId ,String AlloyBrand,String AlloyModelNo ,String AlloyColor ,String AlloyDiameter ,String AlloyWidth ,String AlloyBolts, String AlloyPcd ,String AlloyCenterBore ,String AlloyOffsetNegetive ,String AlloyOffsetPositive ,String AlloyImage ,String AlloyDealerPrice) {
        List<AlloysTable> temp =  AlloysTable.find(AlloysTable.class, "Alloy_Id = ?", AlloyId);
        long id_of_row_intable = temp.get(0).getId();
        alt = AlloysTable.findById(AlloysTable.class, id_of_row_intable);

        alt.AlloyBrand = AlloyBrand;
        alt.AlloyModelNo = AlloyModelNo ;
        alt.AlloyColor = AlloyColor ;
        alt.AlloyDiameter = AlloyDiameter ;
        alt.AlloyWidth = AlloyWidth ;
        alt.AlloyBolts = AlloyBolts;
        alt.AlloyPcd = AlloyPcd ;
        alt.AlloyCenterBore = AlloyCenterBore ;
        alt.AlloyOffsetNegetive = AlloyOffsetNegetive ;
        alt.AlloyOffsetPositive  = AlloyOffsetPositive;
        alt.AlloyImage = AlloyImage ;
        alt.AlloyDealerPrice = AlloyDealerPrice ;
        alt.save();
    }



    public static ArrayList<String> getAllBrand(){
        ArrayList<String> vehicles = new ArrayList<>();
        List<AlloysTable> templist = AlloysTable.listAll(AlloysTable.class);
        for (int i = 0 ; i <templist.size() ;i++){
            if(vehicles.contains(templist.get(i).getAlloyBrand())){
                // ignore this value
            }else {
                vehicles.add(templist.get(i).getAlloyBrand());
            }
        }
        return  vehicles;
    }


    public static ArrayList<String>getAllDiameterByBrand(String brand){
        ArrayList<String> diameters_Arr = new ArrayList<>();
        List<AlloysTable> data_in_list = AlloysTable.find(AlloysTable.class, "Alloy_Brand = ?", brand);
        for (int i = 0 ; i <data_in_list.size() ;i++){
            if(diameters_Arr.contains(data_in_list.get(i).getAlloyDiameter())){
                // ignore this value
            }else {
                diameters_Arr.add(data_in_list.get(i).getAlloyDiameter());
            }
        }
        return diameters_Arr ;
    }


    public static ArrayList<String> getAllWidthByBrandsDiameter(String Brand , String Diameter){
        ArrayList<String> alloywidth = new ArrayList<>();
        List<AlloysTable> data_in_list = AlloysTable.find(AlloysTable.class, "Alloy_Brand = ? and Alloy_Diameter = ?", Brand, Diameter);
        for(int i = 0 ;i<data_in_list.size() ; i++){
            if(alloywidth.contains(data_in_list.get(i).getAlloyWidth())){
                // do nothing
            }else {
                alloywidth.add(data_in_list.get(i).getAlloyWidth());
            }
        }
        return alloywidth;
    }



    public static ArrayList<String> getAllBoltsByBrandDiameterWidth(String Brand , String Diameter , String width){
        ArrayList<String> tyresize = new ArrayList<>();
        List<AlloysTable> data_in_list = AlloysTable.find(AlloysTable.class, "Alloy_Brand = ? and Alloy_Diameter = ? and Alloy_Width = ?", Brand, Diameter, width);
        for(int i = 0 ;i<data_in_list.size() ; i++){
            if(tyresize.contains(data_in_list.get(i).getAlloyBolts())){
                /// do nothing
            }else {
                tyresize.add(data_in_list.get(i).getAlloyBolts());
            }
        }
        return tyresize;
    }


    public static List<AlloysTable> getAllAloyByBrandDiameterWidthBolts(String Brand , String Diameter , String width , String  Bolts){

        List<AlloysTable> data_in_list = AlloysTable.find(AlloysTable.class, "Alloy_Brand = ? and Alloy_Diameter = ? and Alloy_Width = ? and Alloy_Bolts = ?", Brand, Diameter, width, Bolts);

        return data_in_list;
    }















}
