package databases.manager;

import android.content.Context;

import com.orm.SugarRecord;
import com.spinno.cmtpointofsale.CartTable;
import com.spinno.cmtpointofsale.placeordermodule.CartActivity;
import com.spinno.cmtpointofsale.placeordermodule.tyre.PlaceOrderListActivity;

import java.util.List;

import contants.ConstantClass;
import databases.databseutils.CheckCart;

/**
 * Created by samir on 30/07/15.
 */
public class CartManager {

    public static CartTable ct ;
    public static Context con ;

       public CartManager(Context con ){
        this.con = con ;
    }

    public static  void addtocart(String tyreid  , String tyrename , String tyrespecs , String tyrePrice , String tyrenoOfUnits){

        if(new CheckCart().idalreadyExsistinCart(tyreid)){
            ////change already saved details in database
            changeExsistingRowintable(tyreid  , tyrenoOfUnits);
        }else {
            /////  create new row in database
            createNewRowintable(tyreid, tyrename, tyrespecs, tyrePrice, tyrenoOfUnits);
        }

    }

    private static void createNewRowintable(String tyreid  , String tyrename , String tyrespecs , String tyrePrice , String tyrenoOfUnits) {
        new CartTable(tyreid,
                tyrename,
                tyrespecs,
                tyrePrice ,
                tyrenoOfUnits
        ).save();
       showbadgeAnimation();
    }

    private static void changeExsistingRowintable(String reportid , String noOfunitreport) {
        List<CartTable> temp =  CartTable.find(CartTable.class, "Tyre_Id = ?", reportid);
        long id_of_row_intable = temp.get(0).getId();
        ct = CartTable.findById(CartTable.class, id_of_row_intable);
        ct.TyreNoOfunits = noOfunitreport ;
        ct.save();
        showbadgeAnimation();
    }

    public static void removeFromCart(String tyreid){

        List<CartTable>  data_in_list;
        data_in_list = CartTable.find(CartTable.class, "Tyre_Id = ?", tyreid);
        long id_of_row = data_in_list.get(0).getId();
        ct = CartTable.findById(CartTable.class, id_of_row);
        ct.delete();
        showbadgeAnimation();
    }

    public static  int countTotalTypesOfTyre(){
        List<CartTable> templist = CartTable.listAll(CartTable.class);
           return templist.size();
    }

    public static  int countTotalNumbersOfTyre(){
        int numbers = 0 ;

        List<CartTable> templist = CartTable.listAll(CartTable.class);
            for(int i = 0 ; i<templist.size() ;i++){
                numbers = numbers +  (Integer.parseInt(templist.get(i).getTyreNoOfunits()));
            }
          return numbers ;

    }

    public static double grossTotalofFullCart(){
        double dtemp1  , multiplier, grosstotal  = 0.0;
        int temp1;

        List<CartTable> templist = CartTable.listAll(CartTable.class);
        for (int i = 0 ; i<templist.size() ; i++){
            dtemp1 = Double.parseDouble(templist.get(i).getTyrePrice());
            temp1 = Integer.parseInt(templist.get(i).getTyreNoOfunits());
            multiplier = temp1 * dtemp1 ;
            grosstotal = grosstotal + multiplier ;
        }
         return  grosstotal ;
    }

    public static double grossPriceOfParticularTyre(String tyreid){
        List<CartTable>  data_in_list;
        data_in_list = CartTable.find(CartTable.class, "Tyre_Id = ?", tyreid);
        long id_of_row = data_in_list.get(0).getId();
        ct = CartTable.findById(CartTable.class, id_of_row);
        return Integer.parseInt(ct.getTyreNoOfunits()) *Double.parseDouble(ct.getTyrePrice());
    }

    public static void clearCartTable(){
        SugarRecord.deleteAll(CartTable.class);
    }

    public  static List<CartTable> getalldata(){
        return  CartTable.listAll(CartTable.class);

    }

    public static void showbadgeAnimation(){
        if(ConstantClass.whichActivity.equals(PlaceOrderListActivity.classname)){
        }if(ConstantClass.whichActivity.equals(CartActivity.classname)){
            CartActivity.showTextonActionBar();
        }
    }

}
