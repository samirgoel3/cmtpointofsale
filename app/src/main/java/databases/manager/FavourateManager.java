package databases.manager;

import android.content.Context;

import com.spinno.cmtpointofsale.FavourateTable;

import java.util.List;

import checkers.Toaster;
import databases.databseutils.CheckFavourate;

/**
 * Created by samir on 31/07/15.
 */
public class FavourateManager {

    public static FavourateTable ft ;
    public static Context con ;

    public FavourateManager(Context con ){
        this.con = con ;
    }



    public static boolean addtofavourate(String tyreid  , String tyrename , String tyrespecs , String tyrePrice , String tyrenoOfUnits){
          boolean check ;
        if(!new CheckFavourate().idalreadyExsistinFavourate(tyreid)){
            addinFavourate(tyreid ,tyrename ,tyrespecs , tyrePrice , tyrenoOfUnits);
              check = true;
        }else {
            removeFromFavourate(tyreid);
            check = false ;
        }
        return  check ;
    }

    private static void addinFavourate(String tyreid  , String tyrename , String tyrespecs , String tyrePrice , String tyrenoOfUnits) {
        new FavourateTable(tyreid,
                tyrename,
                tyrespecs,
                tyrePrice ,
                tyrenoOfUnits
        ).save();
        Toaster.generatemessage(con  , "Tyre Added To the favourate");
    }




    public static void removeFromFavourate(String tyreid){

        List<FavourateTable> data_in_list;
        data_in_list = FavourateTable.find(FavourateTable.class, "Tyre_Id = ?", tyreid);
        long id_of_row = data_in_list.get(0).getId();
        ft = FavourateTable.findById(FavourateTable.class, id_of_row);
        ft.delete();
        Toaster.generatemessage(con, "Tyre removed To the favourate");

    }

    public static  int countTotalTypesOfTyre(){
        List<FavourateTable> templist = FavourateTable.listAll(FavourateTable.class);
        return templist.size();
    }

    public  static List<FavourateTable> getalldata(){
        return  FavourateTable.listAll(FavourateTable.class);

    }

}
