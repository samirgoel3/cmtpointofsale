package databases.manager;

import android.content.Context;

import com.spinno.cmtpointofsale.InvoiceBucketTable;

import java.util.List;

import databases.databseutils.CheckBucket;

/**
 * Created by spinnosolutions on 8/13/15.
 */
public class InvoiceBucketManager {

    public static InvoiceBucketTable ibt ;
    public static Context con ;

    public InvoiceBucketManager(Context con ){
        this.con = con ;
    }

    public static  void addToBucket(String TyreId, String VehicleType, String BrandName , String TyreRimSize , String TyreName , String TyreSpecs , String TyreType , String TyreFavourite , String TyreNoOfUnit , String TyreOffer , String TyreOfferId , String TyreOfferCompany , String TyreOfferName , String TyreOfferDescription , String TyreOfferStart , String TyreOfferEnd , String TyreOfferUnit , String TyrePrice ){

        if(new CheckBucket().isTyreExsistInBucket(TyreId)){
            ////change already saved details in database
            changeExsistingRowInInvoiceBucketTable(TyreId, TyreNoOfUnit);
        }else {
            /////  create new row in database
            createNewRowInInvoiceBucketTable(TyreId, VehicleType, BrandName, TyreRimSize, TyreName, TyreSpecs, TyreType, TyreFavourite, TyreNoOfUnit, TyreOffer, TyreOfferId, TyreOfferCompany, TyreOfferName, TyreOfferDescription, TyreOfferStart, TyreOfferEnd, TyreOfferUnit, TyrePrice);
        }
    }

    public static void createNewRowInInvoiceBucketTable(String TyreId, String VehicleType, String BrandName , String TyreRimSize , String TyreName , String TyreSpecs , String TyreType , String TyreFavourite , String TyreInStock , String TyreOffer , String TyreOfferId , String TyreOfferCompany , String TyreOfferName , String TyreOfferDescription , String TyreOfferStart , String TyreOfferEnd , String TyreOfferUnit , String TyrePrice ) {
        new InvoiceBucketTable(TyreId, VehicleType, BrandName, TyreRimSize, TyreName, TyreSpecs, TyreType, TyreFavourite, TyreInStock, TyreOffer, TyreOfferId, TyreOfferCompany, TyreOfferName, TyreOfferDescription, TyreOfferStart, TyreOfferEnd, TyreOfferUnit , TyrePrice).save();
    }


    public static void changeExsistingRowInInvoiceBucketTable(String reportid , String noOfUnitInStock) {
        List<InvoiceBucketTable> temp =  InvoiceBucketTable.find(InvoiceBucketTable.class, "Tyre_Id = ?", reportid);
        long id_of_row_intable = temp.get(0).getId();
        ibt = InvoiceBucketTable.findById(InvoiceBucketTable.class, id_of_row_intable);
        ibt.TyreNoOfunits = noOfUnitInStock ;
        ibt.save();
    }


    public static void removeFromBucket(String tyreid){

        List<InvoiceBucketTable>  data_in_list;
        data_in_list = InvoiceBucketTable.find(InvoiceBucketTable.class, "Tyre_Id = ?", tyreid);
        long id_of_row = data_in_list.get(0).getId();
        ibt = InvoiceBucketTable.findById(InvoiceBucketTable.class, id_of_row);
        ibt.delete();
    }




    public static int getTotalnooftyreInBucket(){
        int numbers = 0 ;
        List<InvoiceBucketTable> templist = InvoiceBucketTable.listAll(InvoiceBucketTable.class);
        for(int i = 0 ; i<templist.size() ;i++){
            numbers = numbers +  (Integer.parseInt(templist.get(i).getTyreNoOfunits()));
        }
        return numbers ;
    }



    public static int getTotalTYpeOftyreInBucket(){
        List<InvoiceBucketTable> templist = InvoiceBucketTable.listAll(InvoiceBucketTable.class);
        return templist.size();
    }


    public  static List<InvoiceBucketTable> getalldata(){
        return  InvoiceBucketTable.listAll(InvoiceBucketTable.class);

    }





}
