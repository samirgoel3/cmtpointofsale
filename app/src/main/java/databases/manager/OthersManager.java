package databases.manager;

import android.content.Context;

import com.spinno.cmtpointofsale.OthersTable;
import com.spinno.cmtpointofsale.TubesTable;

import java.util.List;

import databases.databseutils.CheckOther;
import logger.Logger;

/**
 * Created by spinnosolutions on 8/26/15.
 */
public class OthersManager {

    public static OthersTable ott ;
    public static Context con ;

    public OthersManager(Context con ){
        this.con = con ;
    }




    public static  void addToOtherTable(  String Otherid ,String OtherCategory ,String OtherBrand ,String OtherProductName ,String OtherDealerPrice ,String OtherProductDescription ,String OtherTaxation){

        if(new CheckOther().siIDAlreadyExsistInOtherTable(Otherid)){
            ////change already saved details in database
            changeExsistingRowInOtherTable(    Otherid , OtherCategory , OtherBrand , OtherProductName , OtherDealerPrice , OtherProductDescription , OtherTaxation);
        }else {
            /////  create new row in database
            createNewRowInOtherTable(    Otherid , OtherCategory , OtherBrand , OtherProductName , OtherDealerPrice , OtherProductDescription , OtherTaxation);
        }

    }

    private static void createNewRowInOtherTable(String Otherid ,String OtherCategory ,String OtherBrand ,String OtherProductName ,String OtherDealerPrice ,String OtherProductDescription ,String OtherTaxation) {
        Logger.d("-----------------    createNewRowInOtherTable() --------------------   with other id  "+Otherid);
       new OthersTable( Otherid , OtherCategory , OtherBrand , OtherProductName , OtherDealerPrice , OtherProductDescription , OtherTaxation).save();

    }


    public static void changeExsistingRowInOtherTable( String Otherid ,String OtherCategory ,String OtherBrand ,String OtherProductName ,String OtherDealerPrice ,String OtherProductDescription ,String OtherTaxation) {
        List<OthersTable> temp = OthersTable.find(OthersTable.class, "Other_Id = ?", Otherid);
        long id_of_row_intable = temp.get(0).getId();
        ott = TubesTable.findById(OthersTable.class, id_of_row_intable);


        ott.OtherCategory = OtherCategory ;
        ott.OtherBrand = OtherBrand ;
        ott.OtherProductName = OtherProductName ;
        ott.OtherDealerPrice = OtherDealerPrice ;
        ott.OtherProductDescription = OtherProductDescription ;
        ott.OtherTaxation = OtherTaxation ;
        ott.save();
    }



    public static  int countTotalTypesOfOtherProduct(){
        List<OthersTable> templist = OthersTable.listAll(OthersTable.class);
        return templist.size();
    }

    public static String generateIdofProduct(){
        int ids = countTotalTypesOfOtherProduct();
        return "OTH"+(ids+1);
    }




    public static  List<OthersTable> getAllProductBySpecificcategory (String Category){
       //  ArrayList<String> items = new ArrayList<>();
        List<OthersTable> data_in_list = OthersTable.find(OthersTable.class, "Other_Category = ?", Category);
        return  data_in_list ;
    }


    public static List<OthersTable>  getFullTable(){
         return OthersTable.listAll(OthersTable.class);
    }

}
