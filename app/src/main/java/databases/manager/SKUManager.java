package databases.manager;

import android.content.Context;
import android.util.Log;

import com.spinno.cmtpointofsale.SKUTable;

import java.util.ArrayList;
import java.util.List;

import databases.databseutils.CheckSKU;

/**
 * Created by spinnosolutions on 8/22/15.
 */
public class SKUManager {

    public static SKUTable skut ;
    public static Context con ;

    public SKUManager(Context con ){
        this.con = con ;
    }

    public static  void addToSKUTable(String TyreId ,String TyreVehicleType ,String TyreBrandName  ,String TyreRimSize ,String TyreSize , String TyrePattern ,String TyreSpeedRating ,String TyreType,String TyreDealerPrice  ){

        if(new CheckSKU().siIDAlreadyExsistInSKUTable(TyreId)){
            ////change already saved details in database
            changeExsistingRowInSKUTable( TyreId , TyreVehicleType , TyreBrandName  , TyreRimSize , TyreSize ,  TyrePattern , TyreSpeedRating , TyreType, TyreDealerPrice  );
        }else {
            /////  create new row in database
            createNewRowInSKUTable(TyreId, TyreVehicleType, TyreBrandName, TyreRimSize, TyreSize, TyrePattern, TyreSpeedRating, TyreType, TyreDealerPrice);
        }

    }

    private static void createNewRowInSKUTable(String TyreId ,String TyreVehicleType ,String TyreBrandName  ,String TyreRimSize ,String TyreSize , String TyrePattern ,String TyreSpeedRating ,String TyreType,String TyreDealerPrice ) {
        new SKUTable(TyreId , TyreVehicleType , TyreBrandName  , TyreRimSize , TyreSize ,  TyrePattern , TyreSpeedRating , TyreType, TyreDealerPrice ).save();
    }


    public static void changeExsistingRowInSKUTable(String TyreId ,String TyreVehicleType ,String TyreBrandName  ,String TyreRimSize ,String TyreSize , String TyrePattern ,String TyreSpeedRating ,String TyreType,String TyreDealerPrice ) {
        List<SKUTable> temp =  SKUTable.find(SKUTable.class, "Tyre_Id = ?", TyreId);
        long id_of_row_intable = temp.get(0).getId();
        skut = SKUTable.findById(SKUTable.class, id_of_row_intable);

        skut.TyreVehicleType = TyreVehicleType ;
        skut.TyreBrandName = TyreBrandName ;
        skut.TyreRimSize = TyreRimSize ;
        skut.TyreSize = TyreSize ;
        skut.TyrePattern = TyrePattern ;
        skut.TyreSpeedRating = TyreSpeedRating ;
        skut.TyreType = TyreType ;
        skut.TyreDealerPrice = TyreDealerPrice ;
        skut.save();
    }



    public static ArrayList<String> getAllVehicleType(){
        ArrayList<String> vehicles = new ArrayList<>();
        List<SKUTable> templist = SKUTable.listAll(SKUTable.class);
        for (int i = 0 ; i <templist.size() ;i++){
            if(vehicles.contains(templist.get(i).getTyreVehicleType())){
                // ignore this value
            }else {
                vehicles.add(templist.get(i).getTyreVehicleType());
            }
        }
        return  vehicles;
    }




    /////// this will give all companies of specific vehicle type
    public static ArrayList<String>GetAllBrandsByVehicle(String Vehicletype){
        ArrayList<String> companies = new ArrayList<>();
        List<SKUTable> data_in_list = SKUTable.find(SKUTable.class, "Tyre_Vehicle_Type = ?", Vehicletype);
        for (int i = 0 ; i <data_in_list.size() ;i++){
            if(companies.contains(data_in_list.get(i).getTyreBrandName())){
                // ignore this value
            }else {
                companies.add(data_in_list.get(i).getTyreBrandName());
            }
        }
        return companies ;
    }



    //////this  will return all rim size of specific companies in stock
    public static ArrayList<String> getAllRimByVehicleBrand(String Vehicle , String brand){
        ArrayList<String> rimsize = new ArrayList<>();
        List<SKUTable> data_in_list = SKUTable.find(SKUTable.class, "Tyre_Vehicle_Type = ? and Tyre_Brand_Name = ?", Vehicle, brand);
        for(int i = 0 ;i<data_in_list.size() ; i++){
            if(rimsize.contains(data_in_list.get(i).getTyreRimSize())){
                // do nothing
            }else {
                rimsize.add(data_in_list.get(i).getTyreRimSize());
            }
        }
        return rimsize;
    }






    //////this  will return all tyre size of specific vehicle , company , rim size  in stock
    public static ArrayList<String> getAllTyreSizeByVehicleBrandRim(String Vehicle , String brand , String rimsize){
        ArrayList<String> tyresize = new ArrayList<>();
        List<SKUTable> data_in_list = SKUTable.find(SKUTable.class, "Tyre_Vehicle_Type = ? and Tyre_Brand_Name = ? and Tyre_Rim_Size = ?", Vehicle, brand, rimsize);
        for(int i = 0 ;i<data_in_list.size() ; i++){
            if(tyresize.contains(data_in_list.get(i).getTyreSize())){
                /// do nothing
            }else {
                tyresize.add(data_in_list.get(i).getTyreSize());
            }
        }
        return tyresize;
    }







    //////////////      this will return all tyre by vehicle brand , rim size and tyre size filteration
    public static List<SKUTable> getAllTyresByVehicleCompanyRimSizeTyresize(String Vehicle , String brand , String rimsize ,String tyresize){
        Log.e(" for all tyre ATF" , ""+Vehicle + " "+brand+ " "+ " "+rimsize+"  "+tyresize);

        List<SKUTable> data_in_list = SKUTable.find(SKUTable.class, "Tyre_Vehicle_Type = ? and Tyre_Brand_Name = ? and Tyre_Rim_Size = ? and Tyre_Size = ?", Vehicle, brand, rimsize, tyresize);

        return data_in_list;
    }









}
