package databases.manager;

import android.content.Context;

import com.spinno.cmtpointofsale.StockTable;

import java.util.ArrayList;
import java.util.List;

import databases.databseutils.CheckStock;

/**
 * Created by spinnosolutions on 8/11/15.
 */
public class StockInManager {

    public static StockTable st ;
    public static Context con ;

    public StockInManager(Context con ){
        this.con = con ;
    }

    public static  void addToStock(String TyreId, String VehicleType, String BrandName , String TyreRimSize , String TyreName , String TyreSpecs , String TyreType , String TyreFavourite , String TyreInStock , String TyreOffer , String TyreOfferId , String TyreOfferCompany , String TyreOfferName , String TyreOfferDescription , String TyreOfferStart , String TyreOfferEnd , String TyreOfferUnit , String TyrePrice ){

        if(new CheckStock().isTyreExsistInStock(TyreId)){
            ////change already saved details in database
            changeExsistingRowInStockTable(TyreId, TyreInStock);
        }else {
            /////  create new row in database
            createNewRowInStockTable(TyreId, VehicleType, BrandName, TyreRimSize, TyreName, TyreSpecs, TyreType, TyreFavourite, TyreInStock, TyreOffer, TyreOfferId, TyreOfferCompany, TyreOfferName, TyreOfferDescription, TyreOfferStart, TyreOfferEnd, TyreOfferUnit, TyrePrice);
        }

    }

    private static void createNewRowInStockTable(String TyreId, String VehicleType, String BrandName , String TyreRimSize , String TyreName , String TyreSpecs , String TyreType , String TyreFavourite , String TyreInStock , String TyreOffer , String TyreOfferId , String TyreOfferCompany , String TyreOfferName , String TyreOfferDescription , String TyreOfferStart , String TyreOfferEnd , String TyreOfferUnit , String TyrePrice ) {
        new StockTable(TyreId, VehicleType, BrandName, TyreRimSize, TyreName, TyreSpecs, TyreType, TyreFavourite, TyreInStock, TyreOffer, TyreOfferId, TyreOfferCompany, TyreOfferName, TyreOfferDescription, TyreOfferStart, TyreOfferEnd, TyreOfferUnit , TyrePrice).save();
    }


    public static void changeExsistingRowInStockTable(String reportid , String noOfUnitInStock) {
        List<StockTable> temp =  StockTable.find(StockTable.class, "Tyre_Id = ?", reportid);
        long id_of_row_intable = temp.get(0).getId();
        st = StockTable.findById(StockTable.class, id_of_row_intable);
        st.TyreInStock = noOfUnitInStock ;
        st.save();
    }


    ///////this will return total type of vehicle that distributor will avail with
    public static ArrayList<String> getAllTypeOfVehicleRelatedToTyre(){
        ArrayList<String> vehicles = new ArrayList<>();

        List<StockTable> templist = StockTable.listAll(StockTable.class);
        for (int i = 0 ; i <templist.size() ;i++){
                if(vehicles.contains(templist.get(i).getVehicleType())){
                   // ignore this value
                }else {
                    vehicles.add(templist.get(i).getVehicleType());
                }
        }
        return  vehicles;

    }

    /////// this will give all companies of specific vehicle type
    public static ArrayList<String>getAllTypeOfCompaniesRelatedToVehicle(String Vehicletype){
        ArrayList<String> companies = new ArrayList<>();
        List<StockTable> data_in_list = StockTable.find(StockTable.class, "Vehicle_Type = ?", Vehicletype);
        for (int i = 0 ; i <data_in_list.size() ;i++){
            if(companies.contains(data_in_list.get(i).getBrandName())){
                // ignore this value
            }else {
                companies.add(data_in_list.get(i).getBrandName());
            }
        }
        return companies ;
    }


    //////this  will return all rim size of specific companies in stock
    public static ArrayList<String> getAllRimSizeByVehicleAndCompany(String Vehicle , String brand){
        ArrayList<String> rimsize = new ArrayList<>();
        List<StockTable> data_in_list = StockTable.find(StockTable.class, "Vehicle_Type = ? and Brand_Name = ?", Vehicle , brand);
              for(int i = 0 ;i<data_in_list.size() ; i++){
                  if(rimsize.contains(data_in_list.get(i).getTyreRimSize())){
                    // do nothing
                  }else {
                      rimsize.add(data_in_list.get(i).getTyreRimSize());
                  }
              }
         return rimsize;
    }

    //////this  will return all tyre size of specific companies in stock
    public static ArrayList<String> getAllTyreSizeByVehicleAndCompanyAndRimSize(String Vehicle , String brand , String rimsize){
        ArrayList<String> tyresize = new ArrayList<>();
        List<StockTable> data_in_list = StockTable.find(StockTable.class, "Vehicle_Type = ? and Brand_Name = ? and Tyre_Rim_Size = ?", Vehicle , brand,rimsize);
        for(int i = 0 ;i<data_in_list.size() ; i++){
            if(tyresize.contains(data_in_list.get(i).getTyreSpecs())){
          /// do nothing
            }else {
                tyresize.add(data_in_list.get(i).getTyreSpecs());
            }
        }
        return tyresize;
    }


    public static List<StockTable> getAllTyresByVehicleCompanyRimSizeTyresize(String Vehicle , String brand , String rimsize ,String tyresize){
        List<StockTable> data_in_list = StockTable.find(StockTable.class, "Vehicle_Type = ? and Brand_Name = ? and Tyre_Rim_Size = ? and Tyre_Specs = ?", Vehicle , brand,rimsize , tyresize);

        return data_in_list;
    }
    ///////this will give the list all tyres of specific vehicle in stock
    public static  List<StockTable> getAllTyreOfSpecificVehicle(String vehicletype){
        List<StockTable> data_in_list = StockTable.find(StockTable.class, "Vehicle_Type = ?", vehicletype);
        return  data_in_list ;
    }


    public static int getTotalNoOftyreOfByTyreid(String tyreId){
        List<StockTable> temp =  StockTable.find(StockTable.class, "Tyre_Id = ?", tyreId);
        long id_of_row_intable = temp.get(0).getId();
        st = StockTable.findById(StockTable.class, id_of_row_intable);
        return Integer.parseInt(st.getTyreInStock());
    }

}
