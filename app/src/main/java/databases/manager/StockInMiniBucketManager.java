package databases.manager;

import android.content.Context;
import android.util.Log;

import com.orm.SugarRecord;
import com.spinno.cmtpointofsale.AlloyMiniBucket;
import com.spinno.cmtpointofsale.OthersMiniBucket;
import com.spinno.cmtpointofsale.SKUMiniBucket;
import com.spinno.cmtpointofsale.TubeMiniBucket;
import com.spinno.cmtpointofsale.stockinModule.StockInEvents.TotalItemsInMiniBucketsEvent;

import java.util.List;

import databases.databseutils.CheckAlloyMiniBucket;
import databases.databseutils.CheckOthersMiniBucket;
import databases.databseutils.CheckSKUMiniBucket;
import databases.databseutils.CheckTubeMiniBucket;
import de.greenrobot.event.EventBus;

/**
 * Created by spinnosolutions on 8/25/15.
 */
public class StockInMiniBucketManager {

    Context con ;

    public static EventBus bus = EventBus.getDefault();

    ///////////////Bucket Tables
    public static SKUMiniBucket skuminbuckt  ;
    public static TubeMiniBucket tubeminibucket ;
    public static OthersMiniBucket  otherminibucket ;
    public static AlloyMiniBucket alloymniniBucket ;



    public StockInMiniBucketManager(Context con ){
       this.con = con ;
    }






/////////////////////////////////////////////////////////////////////////////  TYRE  or SKU
    public static  void addToSKUMiniBucket(String TyreId, String TyreVehicleType, String TyreBrandName, String TyreRimSize, String TyreSize, String TyrePattern, String TyreSpeedRating, String TyreType, String TyreDealerPrice ,String NoOfUnits , String SellingPrice){
        Log.v("Stcking Bucket Manager ", "---------------------------------- Using addToSkuMiniBucket()  ---------------------------");
    if(new CheckSKUMiniBucket().isTyreExsistInSKUMiniBucket(TyreId)){
        ////change already saved details in database
        changeExsistingRowInSKUMiniBucket(TyreId, NoOfUnits);
    }else {
        /////  create new row in database
        createNewRowInSKUMiniBucket(TyreId, TyreVehicleType, TyreBrandName, TyreRimSize, TyreSize, TyrePattern, TyreSpeedRating, TyreType, TyreDealerPrice, NoOfUnits, SellingPrice);
    }

}

    private static void createNewRowInSKUMiniBucket(String TyreId, String TyreVehicleType, String TyreBrandName, String TyreRimSize, String TyreSize, String TyrePattern, String TyreSpeedRating, String TyreType, String TyreDealerPrice ,String NoOfUnits , String SellingPrice) {
        Log.v("Stcking Bucket Manager " , "---------------------------------- Using createNewRowInSKUMiniBucket()  ---------------------------");

        new SKUMiniBucket( TyreId,  TyreVehicleType,  TyreBrandName,  TyreRimSize,  TyreSize,  TyrePattern,  TyreSpeedRating,  TyreType,  TyreDealerPrice , NoOfUnits ,  SellingPrice).save();
        showDataOverBucket();
    }

    public static void changeExsistingRowInSKUMiniBucket(String reportid , String noOfUnitInStock) {
        Log.v("Stcking Bucket Manager " , "---------------------------------- Using changeExsistingRowInSKUMiniBucket()  ---------------------------");

        List<SKUMiniBucket> temp =  SKUMiniBucket.find(SKUMiniBucket.class, "Tyre_Id = ?", reportid);
        long id_of_row_intable = temp.get(0).getId();
        skuminbuckt = SKUMiniBucket.findById(SKUMiniBucket.class, id_of_row_intable);
        skuminbuckt.NoOfUnits = noOfUnitInStock ;
        skuminbuckt.save();
        showDataOverBucket();
    }

    public static void removeFromSKUMiniBucket(String tyreid){
        Log.v("Stcking Bucket Manager " , "---------------------------------- Using removeFromSKUMiniBucket()  ---------------------------");

        List<SKUMiniBucket>  data_in_list;
        data_in_list = SKUMiniBucket.find(SKUMiniBucket.class, "Tyre_Id = ?", tyreid);
        long id_of_row = data_in_list.get(0).getId();
        skuminbuckt = SKUMiniBucket.findById(SKUMiniBucket.class, id_of_row);
        skuminbuckt.delete();
        showDataOverBucket();
    }

    public static  int countTotalTypesOfTyre(){
        List<SKUMiniBucket> templist = SKUMiniBucket.listAll(SKUMiniBucket.class);
        return templist.size();
    }

    public static  int countTotalNumbersOfTyres(){
        Log.v("StockingBucketManager " , "---------------------------------- Using countTotalNumbersOfTubes()  ---------------------------");

        int numbers = 0 ;

        List<SKUMiniBucket> templisttttt = SKUMiniBucket.listAll(SKUMiniBucket.class);

        for(int i = 0 ; i<templisttttt.size() ;i++){
            numbers = numbers +  (Integer.parseInt(templisttttt.get(i).NoOfUnits));
        }

        return numbers ;

    }

    public static List<SKUMiniBucket> getAllTyreFromMiniBucket(){
        return  SKUMiniBucket.listAll(SKUMiniBucket.class);
    }

























////////////////////////////////////////////////////////////////////// Tubes

        public static  void addToTubeMiniBucket(String TubeId ,String TubeVehicleType ,String TubeBrand  , String TubeRimSize ,String TubeSize  , String TubePrice  , String NoofUnits , String SellingPrice){
            Log.v("Stcking Bucket Manager ", "---------------------------------- Using addToTubeMiniBucket()  ---------------------------");

        if(new CheckTubeMiniBucket().isTubeExsistInTubeMiniBucket(TubeId)){
            ////change already saved details in database
            changeExsistingRowInTubeMiniBucket(TubeId, NoofUnits);
        }else {
            /////  create new row in database
            createNewRowInTubeMiniBucket(TubeId, TubeVehicleType, TubeBrand, TubeRimSize, TubeSize, TubePrice, NoofUnits, SellingPrice);
        }

    }

        private static void createNewRowInTubeMiniBucket(String TubeId ,String TubeVehicleType ,String TubeBrand  , String TubeRimSize ,String TubeSize  , String TubePrice  , String NoofUnits , String SellingPrice) {
            Log.v("Stcking Bucket Manager " , "---------------------------------- Using createNewRowInTubeMiniBucket()  ---------------------------");
            new TubeMiniBucket( TubeId , TubeVehicleType , TubeBrand  ,  TubeRimSize , TubeSize  ,  TubePrice  ,  NoofUnits,  SellingPrice).save();
            showDataOverBucket();
    }

        public static void changeExsistingRowInTubeMiniBucket(String reportid , String noOfUnitInStock) {

            Log.v("Stcking Bucket Manager " , "---------------------------------- Using changeExsistingRowInTubeMiniBucket()  ---------------------------");


            List<TubeMiniBucket> temp =  TubeMiniBucket.find(TubeMiniBucket.class, "Tube_Id = ?", reportid);
        long id_of_row_intable = temp.get(0).getId();
        tubeminibucket = TubeMiniBucket.findById(TubeMiniBucket.class, id_of_row_intable);
        tubeminibucket.NoofUnits = noOfUnitInStock ;
        tubeminibucket.save();
            showDataOverBucket();


    }

        public static void removeFromTubeMiniBucket(String tubeid){
        Log.v("Stcking Bucket Manager " , "---------------------------------- Using removeFromTubeMiniBucket()  ---------------------------");

        List<TubeMiniBucket>  data_in_list;
        data_in_list = TubeMiniBucket.find(TubeMiniBucket.class, "Tube_Id = ?", tubeid);
        long id_of_row = data_in_list.get(0).getId();
        tubeminibucket = TubeMiniBucket.findById(TubeMiniBucket.class, id_of_row);
        tubeminibucket.delete();

            showDataOverBucket();
    }

        public static  int countTotalTypesOfTubes(){
        List<TubeMiniBucket> templist = TubeMiniBucket.listAll(TubeMiniBucket.class);
        return templist.size();
    }

        public static  int countTotalNumbersOfTubes(){
        Log.v("StockingBucketManager " , "---------------------------------- Using countTotalNumbersOfTubes()  ---------------------------");

        int numbers = 0 ;

        List<TubeMiniBucket> templisttttt = TubeMiniBucket.listAll(TubeMiniBucket.class);

        for(int i = 0 ; i<templisttttt.size() ;i++){
            numbers = numbers +  (Integer.parseInt(templisttttt.get(i).NoofUnits));
        }

        return numbers ;

    }


    public static List<TubeMiniBucket> getAllTubeFromMiniBucket(){
        return  TubeMiniBucket.listAll(TubeMiniBucket.class);
    }


















//////////////////////////////////////////////////////////////  OTHERS
    public static  void addToItemsMiniBucket( String OtherId ,String OtherCategory ,String OtherBrand ,String OtherProductName ,String OtherDealerPrice ,String OtherProductDescription ,String OtherTaxation ,String NoOfUnits  ,String OthersSellingPrice ){
    Log.v("Stcking Bucket Manager ", "---------------------------------- Using addToItemsMiniBucket()  ---------------------------");

    if(new CheckOthersMiniBucket().isItemExsistInOthersMiniBucket(OtherId)){
        ////change already saved details in database
        changeExsistingRowInOthersMiniBucket(OtherId, NoOfUnits);
    }else {
        /////  create new row in database
        createNewRowInOthersMiniBucket(OtherId, OtherCategory, OtherBrand, OtherProductName, OtherDealerPrice, OtherProductDescription, OtherTaxation, NoOfUnits, OthersSellingPrice);
    }

}

    private static void createNewRowInOthersMiniBucket(String OtherId ,String OtherCategory ,String OtherBrand ,String OtherProductName ,String OtherDealerPrice ,String OtherProductDescription ,String OtherTaxation ,String NoOfUnits  ,String OthersSellingPrice ) {
        Log.v("Stcking Bucket Manager " , "---------------------------------- Using createNewRowInOthersMiniBucket()  ---------------------------");
        new OthersMiniBucket(  OtherId , OtherCategory , OtherBrand , OtherProductName , OtherDealerPrice , OtherProductDescription , OtherTaxation , NoOfUnits  , OthersSellingPrice ).save();
        showDataOverBucket();
    }

    public static void changeExsistingRowInOthersMiniBucket(String reportid , String noOfUnitInStock) {

        Log.v("Stcking Bucket Manager " , "---------------------------------- Using changeExsistingRowInOthersMiniBucket()  ---------------------------");


        List<OthersMiniBucket> temp =  OthersMiniBucket.find(OthersMiniBucket.class, "Other_Id = ?", reportid);
        long id_of_row_intable = temp.get(0).getId();
        otherminibucket = OthersMiniBucket.findById(OthersMiniBucket.class, id_of_row_intable);
        otherminibucket.NoOfUnits = noOfUnitInStock ;
        otherminibucket.save();
        showDataOverBucket();


    }

    public static void  removeFromothersminiBucket(String item_Id){
        Log.v("Stcking Bucket Manager " , "---------------------------------- Using removeItemFromMiniBucket()  ---------------------------");

        List<OthersMiniBucket>  data_in_list;
        data_in_list = OthersMiniBucket.find(OthersMiniBucket.class, "Other_Id = ?", item_Id);
        long id_of_row = data_in_list.get(0).getId();
        otherminibucket = OthersMiniBucket.findById(OthersMiniBucket.class, id_of_row);
        otherminibucket.delete();
        showDataOverBucket();
    }


    public static  int countTotalTypesOfOthersItems(){
        List<OthersMiniBucket> templist = OthersMiniBucket.listAll(OthersMiniBucket.class);
        return templist.size();
    }

    public static  int countTotalNumbersOfOthersItems(){
        Log.v("StockingBucketManager " , "---------------------------------- Using countTotalNumbersOfTubes()  ---------------------------");

        int numbers = 0 ;

        List<OthersMiniBucket> templisttttt = OthersMiniBucket.listAll(OthersMiniBucket.class);

        for(int i = 0 ; i<templisttttt.size() ;i++){
            numbers = numbers +  (Integer.parseInt(templisttttt.get(i).NoOfUnits));
        }

        return numbers ;

    }

    public static List<OthersMiniBucket> getAllTubeOthersMiniBucket(){
        return  OthersMiniBucket.listAll(OthersMiniBucket.class);
    }























//////////////////////////////////////////////////ALLOYS
public static  void addToAlloyMiniBucket(String AlloyId, String AlloyBrand, String AlloyModelNo, String AlloyColor, String AlloyDiameter, String AlloyWidth, String AlloyBolts, String AlloyPcd, String AlloyCenterBore, String AlloyOffsetNegetive, String AlloyOffsetPositive, String AlloyImage, String AlloyDealerPrice, String NoOfUnits, String AlloySellingPrice ){
    Log.v("Stcking Bucket Manager ", "---------------------------------- Using addToAlloyMiniBucket()  ---------------------------");
    if(new CheckAlloyMiniBucket().isAlloyExsistinMiniBucket(AlloyId)){
        ////change already saved details in database
        changeExsistingRowInAlloyminiBucket(AlloyId, NoOfUnits);
    }else {
        /////  create new row in database
        createNewRowInAlloyMiniBucket( AlloyId,  AlloyBrand,  AlloyModelNo,  AlloyColor,  AlloyDiameter,  AlloyWidth,  AlloyBolts,  AlloyPcd,  AlloyCenterBore,  AlloyOffsetNegetive,  AlloyOffsetPositive,  AlloyImage,  AlloyDealerPrice,  NoOfUnits,  AlloySellingPrice );
    }

}

    private static void createNewRowInAlloyMiniBucket(String AlloyId, String AlloyBrand, String AlloyModelNo, String AlloyColor, String AlloyDiameter, String AlloyWidth, String AlloyBolts, String AlloyPcd, String AlloyCenterBore, String AlloyOffsetNegetive, String AlloyOffsetPositive, String AlloyImage, String AlloyDealerPrice, String NoOfUnits, String AlloySellingPrice ) {
        Log.v("St0cking Bucket Manager ", "---------------------------------- Using createNewRowInAlloyMiniBucket()  ---------------------------");

        new AlloyMiniBucket( AlloyId,  AlloyBrand,  AlloyModelNo,  AlloyColor,  AlloyDiameter,  AlloyWidth,  AlloyBolts,  AlloyPcd,  AlloyCenterBore,  AlloyOffsetNegetive,  AlloyOffsetPositive,  AlloyImage,  AlloyDealerPrice,  NoOfUnits,  AlloySellingPrice ).save();
        showDataOverBucket();
    }

    public static void changeExsistingRowInAlloyminiBucket(String reportid , String noOfUnitInStock) {
        Log.v("Stcking Bucket Manager " , "---------------------------------- Using changeExsistingRowInAlloyminiBucket()  ---------------------------");

        List<AlloyMiniBucket> temp =  AlloyMiniBucket.find(AlloyMiniBucket.class, "Alloy_Id = ?", reportid);
        long id_of_row_intable = temp.get(0).getId();
        alloymniniBucket = AlloyMiniBucket.findById(AlloyMiniBucket.class, id_of_row_intable);
        alloymniniBucket.NoOfUnits = noOfUnitInStock ;
        alloymniniBucket.save();
        showDataOverBucket();
    }

    public static void removeFromAlloyMiniBucket(String alloyid){
        Log.v("Stocking Bucket Manager " , "---------------------------------- Using removeFromAlloyMiniBucket()  ---------------------------");

        List<AlloyMiniBucket>  data_in_list;
        data_in_list = AlloyMiniBucket.find(AlloyMiniBucket.class, "Alloy_Id = ?", alloyid);
        long id_of_row = data_in_list.get(0).getId();
        alloymniniBucket = AlloyMiniBucket.findById(AlloyMiniBucket.class, id_of_row);
        alloymniniBucket.delete();
        showDataOverBucket();
    }

    public static  int countTotalTypesOfAlloy(){
        List<AlloyMiniBucket> templist = AlloyMiniBucket.listAll(AlloyMiniBucket.class);
        return templist.size();
    }

    public static  int countTotalNumbersOfAlloys(){
        Log.v("StockingBucketManager " , "---------------------------------- Using countTotalNumbersOfAlloys()  ---------------------------");

        int numbers = 0 ;
        List<AlloyMiniBucket> templisttttt = AlloyMiniBucket.listAll(AlloyMiniBucket.class);
        for(int i = 0 ; i<templisttttt.size() ;i++){
            numbers = numbers +  (Integer.parseInt(templisttttt.get(i).NoOfUnits));
        }
        return numbers ;
    }


    public static List<AlloyMiniBucket> getAllAlloyMiniBucket(){
        return  AlloyMiniBucket.listAll(AlloyMiniBucket.class);
    }






































































////////////////////////////////////////////////////  central logic

    public static void removeFromAllMiniBuckets(){
        SugarRecord.deleteAll(SKUMiniBucket.class);
        SugarRecord.deleteAll(TubeMiniBucket.class);
        SugarRecord.deleteAll(OthersMiniBucket.class);
        SugarRecord.deleteAll(AlloyMiniBucket.class);
        Log.v("Stocking Bucket Manager ", "---------------------------------- Using removeFromAllMiniBuckets()  ---------------------------");
    }

    public static void showDataOverBucket(){

        TotalItemsInMiniBucketsEvent event = null;


        event = new TotalItemsInMiniBucketsEvent(""+countTotalNumbersOfTyres(),""+countTotalNumbersOfTubes() ,""+countTotalNumbersOfOthersItems() , ""+countTotalNumbersOfAlloys());

        bus.post(event);

    }


}












