package databases.manager;

import android.content.Context;
import android.util.Log;

import com.spinno.cmtpointofsale.TubesTable;

import java.util.ArrayList;
import java.util.List;

import databases.databseutils.CheckTube;

/**
 * Created by spinnosolutions on 8/24/15.
 */
public class TubeManager {


    public static TubesTable tut ;
    public static Context con ;

    public TubeManager(Context con ){
        this.con = con ;
    }




    public static  void addToTubeTable(String TubeId ,String TubeVehicleType ,String TubeBrand , String TubeRimSize,String TubeSize ,String TubePrice){

        if(new CheckTube().siIDAlreadyExsistInTubeTable(TubeId)){
            ////change already saved details in database
            changeExsistingRowInTubeTable( TubeId , TubeVehicleType , TubeBrand,  TubeRimSize , TubeSize , TubePrice);
        }else {
            /////  create new row in database
            createNewRowInTubeTable( TubeId , TubeVehicleType , TubeBrand ,  TubeRimSize, TubeSize , TubePrice);
        }

    }

    private static void createNewRowInTubeTable(String TubeId ,String TubeVehicleType ,String TubeBrand , String TubeRimSize,String TubeSize  ,String TubePrice) {
        Log.d("Saving data to Tune table" , "tube id " + TubeId);
        new TubesTable(TubeId , TubeVehicleType , TubeBrand,  TubeRimSize , TubeSize , TubePrice ).save();
    }


    public static void changeExsistingRowInTubeTable( String TubeId ,String TubeVehicleType ,String TubeBrand , String TubeRimSize,String TubeSize , String TubePrice) {
        List<TubesTable> temp =  TubesTable.find(TubesTable.class, "Tube_Id = ?", TubeId);
        long id_of_row_intable = temp.get(0).getId();
        tut = TubesTable.findById(TubesTable.class, id_of_row_intable);

        tut.TubeId = TubeId ;
        tut.TubeVehicleType = TubeVehicleType ;
        tut.TubeBrand = TubeBrand ;
        tut.TubeRimSize = TubeRimSize ;
        tut.TubeSize = TubeSize ;
        tut.TubePrice = TubePrice ;
        tut.save();
    }




    public static ArrayList<String> getAllVehicleType(){
        ArrayList<String> vehicles = new ArrayList<>();
        List<TubesTable> templist = TubesTable.listAll(TubesTable.class);
        for (int i = 0 ; i <templist.size() ;i++){
            if(vehicles.contains(templist.get(i).getTubeVehicleType())){
                // ignore this value
            }else {
                vehicles.add(templist.get(i).getTubeVehicleType());
            }
        }
        return  vehicles;
    }




    /////// this will give all companies of specific vehicle type
    public static ArrayList<String>GetAllBrandsByVehicle(String Vehicletype){
        ArrayList<String> companies = new ArrayList<>();
        List<TubesTable> data_in_list = TubesTable.find(TubesTable.class, "Tube_Vehicle_Type = ?", Vehicletype);
        for (int i = 0 ; i <data_in_list.size() ;i++){
            if(companies.contains(data_in_list.get(i).getTubeBrand())){
                // ignore this value
            }else {
                companies.add(data_in_list.get(i).getTubeBrand());
            }
        }
        return companies ;
    }



    //////this  will return all tube size of specific companies and vehicle
    public static ArrayList<String> getAllRimByVehicleBrand(String Vehicle , String brand){
        ArrayList<String> rimsize = new ArrayList<>();
        List<TubesTable> data_in_list = TubesTable.find(TubesTable.class, "Tube_Vehicle_Type = ? and Tube_Brand = ?", Vehicle, brand);
        for(int i = 0 ;i<data_in_list.size() ; i++){
            if(rimsize.contains(data_in_list.get(i).getTubeRimSize())){
                // do nothing
            }else {
                rimsize.add(data_in_list.get(i).getTubeRimSize());
            }
        }
        return rimsize;
    }



    //////this  will return all tube size of specific vehicle , company , rim size  in stock

    public static List<TubesTable> getAllTubeSizesByVehicleCompanyRimSizeTyresize(String Vehicle , String brand , String rimsize ){

        List<TubesTable> data_in_list = TubesTable.find(TubesTable.class, "Tube_Vehicle_Type = ? and Tube_Brand = ? and Tube_Rim_Size = ?", Vehicle, brand, rimsize);

        return data_in_list;
    }






}
