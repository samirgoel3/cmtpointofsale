package spinno.com.draggablegrisview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Created by samir on 03/08/15.
 */
public class Adaprtergrid   extends BaseAdapter {

    Context con ;
    LayoutInflater inflater;


    public Adaprtergrid (Context con ){
        this.con = con ;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view ;

        view = inflater.inflate(R.layout.item_grid, parent, false);
        return view;
    }
}
