package spinno.com.draggablegrisview;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;


public class MainActivity extends Activity {


    static Random random = new Random();
    String [] colorsarr = {"e74c3c" ,"2ecc71" , "e67e22" ,"9b59b6" , "f1c40f" ,"3498db" , "f39c12" , "34495e" , "e74c3c" ,"2ecc71" , "e67e22" ,"9b59b6" , "f1c40f" ,"3498db" , "f39c12" , "34495e" , "e74c3c" ,"2ecc71" , "e67e22" ,"9b59b6" , "f1c40f" ,"3498db" , "f39c12" , "34495e"};
    String [] companyarr = {"Apollo" ,"bridgestone" ,"CEAT" , "Continental" , "Cooper" , "Falken" , "GoodYear" , "Hankook" , "Ironman" , "JK Tyre" , "kenda" , "kumho" ,"Leao" , "Maxxis" , "Metro" , "Michelin" , "MRF" , "Nexan" ,"Pirelli" , "Yokohama"};

    DraggableGridView dgv;
    ArrayList<String> poem = new ArrayList<String>();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dgv = ((DraggableGridView)findViewById(R.id.vgv));

        setListeners();
    }
    private void setListeners()
    {


        for(int i = 0 ; i<companyarr.length ; i++){
            String word = ""+companyarr[i];
            ImageView view = new ImageView(MainActivity.this);
            view.setImageBitmap(getThumb(word , i));
            dgv.addView(view);
            poem.add(word);
        }


    }

    private Bitmap getThumb(String s , int i)
    {
        Bitmap bmp = Bitmap.createBitmap(150, 150, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bmp);
        Paint paint = new Paint();

        //paint.setColor(Color.rgb(random.nextInt(128), random.nextInt(128), random.nextInt(128)));

        paint.setColor(Color.parseColor("#"+colorsarr[i]));
        paint.setTextSize(20);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        canvas.drawRect(new Rect(0, 0, 150, 150), paint);
        paint.setColor(Color.WHITE);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(s, 75, 75, paint);

        return bmp;
    }
}