package spinno.com.draggablegrisview;

public interface OnRearrangeListener {
	
	public abstract void onRearrange(int oldIndex, int newIndex);
}
